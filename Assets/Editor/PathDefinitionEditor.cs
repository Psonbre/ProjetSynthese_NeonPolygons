using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(MovingPlatform))]
public class PathDefinitionEditor : Editor
{
    public override void OnInspectorGUI()
    {
        MovingPlatform pathDefinition = (MovingPlatform)target;

        EditorGUI.BeginChangeCheck();

        EditorGUILayout.PropertyField(serializedObject.FindProperty("speed"));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("shouldUseCirclePath"));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("goClockwise"));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("distanceBeforeSwitchPoint"));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("groundCheckDistance"));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("groundLayer"));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("maxTimeBeforeMoveAgain"));

        if (pathDefinition.ShouldUseCirclePath)
        {
            EditorGUILayout.PropertyField(serializedObject.FindProperty("radius"), true);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("circlePoint"), true);
        }
        else
        {
            EditorGUILayout.PropertyField(serializedObject.FindProperty("path"), true);
        }

        if (EditorGUI.EndChangeCheck())
        {
            serializedObject.ApplyModifiedProperties();
        }
    }

    void OnSceneGUI()
    {
        MovingPlatform pathDefinition = (MovingPlatform)target;
        Event currentEvent = Event.current;
        Vector2 mousePosition = HandleUtility.GUIPointToWorldRay(currentEvent.mousePosition).origin;

        if (currentEvent.type == EventType.MouseDown && currentEvent.button == 0 && Event.current.control)
        {
            Undo.RecordObject(pathDefinition, "Add Path Point");
            if (!pathDefinition.ShouldUseCirclePath)
                pathDefinition.path.Add(mousePosition);
            else
                pathDefinition.circlePoint = mousePosition;
            SceneView.RepaintAll();
            currentEvent.Use();
        }

        if (pathDefinition) {
            if (pathDefinition.ShouldUseCirclePath)
            {
                // Draw circle path
                Handles.color = Color.green;
                Handles.DrawWireDisc(pathDefinition.circlePoint, Vector3.back, pathDefinition.Radius);
            }
            else if (pathDefinition.path != null && pathDefinition.path.Count > 0)
            {
                // Draw lines between points to visualize the path
                Handles.color = Color.green;
                for (int i = 0; i < pathDefinition.path.Count - 1; i++)
                {
                    Handles.DrawLine(pathDefinition.path[i], pathDefinition.path[i + 1]);
                }
                if (pathDefinition.path.Count > 1)
                {
                    Handles.DrawLine(pathDefinition.path[0], pathDefinition.path[pathDefinition.path.Count - 1]);
                }
            }
        }
    }
}