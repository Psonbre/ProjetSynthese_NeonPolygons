using UnityEngine;
using UnityEditor;

public class ScreenshotTaker : EditorWindow
{
    private Camera targetCamera;
    private string path = "Assets/Screenshots";
    private string filename = "screenshot.png";

    [MenuItem("Tools/Screenshot Taker")]
    public static void ShowWindow()
    {
        GetWindow<ScreenshotTaker>("Screenshot Taker");
    }

    private void OnGUI()
    {
        GUILayout.Label("Screenshot Settings", EditorStyles.boldLabel);
        targetCamera = EditorGUILayout.ObjectField("Camera", targetCamera, typeof(Camera), true) as Camera;
        path = EditorGUILayout.TextField("Path", path);
        filename = EditorGUILayout.TextField("Filename", filename);

        if (GUILayout.Button("Take Screenshot"))
        {
            if (targetCamera != null)
            {
                TakeScreenshot();
            }
            else
            {
                Debug.LogError("Screenshot Taker: No camera selected!");
            }
        }
    }

    private void TakeScreenshot()
    {
        RenderTexture rt = new RenderTexture(targetCamera.pixelWidth, targetCamera.pixelHeight, 24);
        targetCamera.targetTexture = rt;
        Texture2D screenShot = new Texture2D(targetCamera.pixelWidth, targetCamera.pixelHeight, TextureFormat.RGB24, false);
        targetCamera.Render();
        RenderTexture.active = rt;
        screenShot.ReadPixels(new Rect(0, 0, targetCamera.pixelWidth, targetCamera.pixelHeight), 0, 0);
        targetCamera.targetTexture = null;
        RenderTexture.active = null; // JC: added to avoid errors
        DestroyImmediate(rt);
        byte[] bytes = screenShot.EncodeToPNG();
        string fullPath = System.IO.Path.Combine(path, filename);
        System.IO.File.WriteAllBytes(fullPath, bytes);
        Debug.Log($"Screenshot saved to {fullPath}");
    }
}
