using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pixel : MonoBehaviour
{
    SpriteRenderer spriteRenderer;
    Rigidbody2D rb;
    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        spriteRenderer = GetComponent<SpriteRenderer>();
    }
    public void Initialize(Vector2 position, float scale, Color color, Vector2 dir)
    {
        rb.velocity = -dir * 100f * Random.value;
        transform.position = position;
        spriteRenderer.color = color;
        transform.localScale = new(scale, scale, scale);
    }
    void Update()
    {
        if (transform.position.y < -300f) gameObject.SetActive(false);
    }
}
