using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class IntroTriangle : MonoBehaviour
{
    [SerializeField] private float rotatingSpeed = 100f;
    [SerializeField] private float trailRate = 1f;
    [SerializeField] private float circleAroundSpeed = 1f;
    [SerializeField] private SpriteRenderer blackFade;
    [SerializeField] private ParticleSystem stars;
    [SerializeField] private ParticleSystem explosion;
    [SerializeField] private Canvas canvas;
    [SerializeField] private AudioClip explosionSound;

    private bool trail = true;
    private float circularAngle = 0f;
    private float circularPathRadius = 0f;
    private float timeUntilNextTrail;
    private bool circling = true;
    void Update()
    {
        transform.eulerAngles = new(0,0, transform.eulerAngles.z + rotatingSpeed * Time.deltaTime);

        timeUntilNextTrail -= Time.deltaTime;

        if (timeUntilNextTrail < 0 && trail)
        {
            timeUntilNextTrail = 1 / trailRate;

            IntroTriangleTrail trail = PoolManager.Instance.Get(Harmony.PoolTags.trail).GetComponent<IntroTriangleTrail>();
            trail.gameObject.SetActive(true);
            trail.transform.eulerAngles = transform.eulerAngles;
            trail.transform.position = transform.position;
            trail.transform.localScale = transform.localScale;
            trail.Initialize();
        }

        if (circling)
        {
			circularAngle += circleAroundSpeed * Time.deltaTime;
			transform.position = new(Mathf.Cos(circularAngle) * circularPathRadius, Mathf.Sin(circularAngle) * circularPathRadius * 1.5f);
		}
	}
	private void Start()
	{
        StartCoroutine(AnimationCoroutine());
	}
	private IEnumerator AnimationCoroutine()
    {

        float timeLeft = 1.5f;
        circularAngle = (Mathf.PI * 3f) / 2f;

        while (timeLeft > 0)
        {
            blackFade.color = new(blackFade.color.r, blackFade.color.g, blackFade.color.b, Mathf.Clamp(timeLeft,0,1));
			timeLeft -= Time.deltaTime;
            circularPathRadius += 75f * Time.deltaTime;
			yield return null;
        }
        
        yield return new WaitForSeconds(1);

        transform.localScale = Vector2.one * 8f;

        yield return new WaitForSeconds(1.5f);

        circling = false;

        transform.position = new(0, -260);
        canvas.transform.position = new(0, -260);

        while (stars.playbackSpeed < 9)
        {
            stars.playbackSpeed += 8f * Time.deltaTime;
            yield return null;
        }

        yield return new WaitForSeconds(1);
        trailRate = 2;
		while (stars.playbackSpeed > 1)
		{
			stars.playbackSpeed -= 4f * Time.deltaTime;
            transform.position = new(0, transform.position.y + 130f * Time.deltaTime);
			canvas.transform.position = new(0, transform.position.y + 130f * Time.deltaTime);
			yield return null;
		}

        yield return new WaitForSeconds(2f);

        explosion.transform.position = transform.position;
        explosion.Play();
        SoundManager.Instance.PlaySoundEffect(explosionSound);
        trail = false;

        GetComponent<SpriteRenderer>().color = new(0, 0, 0, 0);

        while (blackFade.color.a < 1)
        {
            blackFade.color = new(blackFade.color.r, blackFade.color.g, blackFade.color.b, blackFade.color.a + 0.5f * Time.deltaTime);
            yield return null;
        }

        MusicManager.Instance.PlayMainMusic();
        SceneManager.LoadScene(Harmony.Scenes.MainMenuScene);
	}
}
