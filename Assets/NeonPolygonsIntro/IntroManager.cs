using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Controls;
using UnityEngine.SceneManagement;

public class IntroManager : MonoBehaviour

{
    [SerializeField] private AudioClip ambientSound;
    [SerializeField] private AudioClip wooshSound;
    [SerializeField] private AudioClip jingleSound;
    [SerializeField] private AudioClip fallingSound;

    private float timeElapsed = 0;

    private float jingleTime = 8f;
    private bool jinglePlayed = false;

    private float firstWooshTime = 0.2f;
    private bool firstWooshPlayed = false;

    private float secondWooshTime = 1.8f;
    private bool secondWooshPlayed = false;

    private float fallingTime = 2.4f;
    private bool fallingPlayed = false;

    private void Start()
    {
        MusicManager.Instance.PlayIntroMusic();
    }

    void Update()
    {
        timeElapsed += Time.deltaTime;
        CheckForSoundEffectToPlay();
        if (Keyboard.current.anyKey.IsPressed() || IsGamepadPressed())
        {
            MusicManager.Instance.PlayMainMusic();
            SceneManager.LoadScene(Harmony.Scenes.MainMenuScene);
        }
    }

    private void CheckForSoundEffectToPlay()
    {
        if (!firstWooshPlayed && timeElapsed >= firstWooshTime)
        {
            SoundManager.Instance.PlaySoundEffect(wooshSound);
            firstWooshPlayed = true;
        }

        if (!secondWooshPlayed && timeElapsed >= secondWooshTime)
        {
            SoundManager.Instance.PlaySoundEffect(wooshSound);
            secondWooshPlayed = true;
        }

        if (!fallingPlayed && timeElapsed >= fallingTime)
        {
            SoundManager.Instance.PlaySoundEffect(fallingSound);
            fallingPlayed = true;
        }

        if (!jinglePlayed && timeElapsed >= jingleTime)
        {
            SoundManager.Instance.PlaySoundEffect(jingleSound);
            jinglePlayed = true;
        }
    }

    private bool IsGamepadPressed()
    {
        Gamepad gamepad = Gamepad.current;
        if (gamepad != null) 
        {
            ButtonControl[] controls = new ButtonControl[] {
                gamepad.buttonEast, 
                gamepad.buttonSouth, 
                gamepad.buttonNorth, 
                gamepad.buttonWest, 
                gamepad.startButton, 
                gamepad.selectButton
            };

            foreach (ButtonControl control in controls)
            {
                if (control.isPressed) return true;
            }
        }
        return false;
    }
}
