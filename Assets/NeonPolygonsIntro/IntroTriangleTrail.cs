using UnityEngine;

public class IntroTriangleTrail : MonoBehaviour
{
	[SerializeField] private string poolTag = "trail";
	[SerializeField] private float fadeTime = 1.0f;
	[SerializeField] private float startingOpacity = 1.0f;
	[SerializeField] private float verticalSpeed;

	private float actualVerticalSpeed;
	private ParticleSystem stars;
	private float fadeSpeed;
	private SpriteRenderer spriteRenderer;

	private void Awake()
	{
		spriteRenderer = GetComponent<SpriteRenderer>();
	}

	public void Initialize()
	{
		if (!stars) stars = FindFirstObjectByType<ParticleSystem>();
		spriteRenderer.color = new(startingOpacity, startingOpacity, startingOpacity, startingOpacity);
		fadeSpeed = startingOpacity / fadeTime;
		
	}

	private void Update()
	{
		actualVerticalSpeed = verticalSpeed * stars.playbackSpeed;
		transform.position = new(transform.position.x, transform.position.y + actualVerticalSpeed * Time.deltaTime);
		float newAlpha = spriteRenderer.color.a - fadeSpeed * Time.deltaTime;
		spriteRenderer.color = new(newAlpha, newAlpha, newAlpha, newAlpha);

		if (spriteRenderer.color.a <= 0 )
		{
			PoolManager.Instance.ReturnToPool(poolTag, gameObject);
		}
	}
}
