using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

[System.Serializable]
public class SaveData
{
    public List<SaveAchievement> achievements;
    public List<SaveStatistic> statistics;
}

[System.Serializable]
public class SaveAchievement
{
    public string slug;
    public string dateUnlocked;

    public SaveAchievement(string slug)
    {
        this.slug = slug;
        dateUnlocked = DateTime.Now.ToString();
    }

    public DateTime GetDateUnlocked()
    {
        return DateTime.Parse(dateUnlocked);
    }
}

[System.Serializable]
public class SaveStatistic
{
    public string slug;
    public string value;
    public SaveStatistic(string surname, object value)
    {
        this.slug = surname;
        this.value = value.ToString();
    }

    public void SetValue(object value)
    {
        this.value = value.ToString();
    }

    public long GetValue() 
    {
        return long.Parse(value);
    }
}

public class SaveSystem : MonoBehaviour
{
    const string savePath = "/save.json";
    string saveFile;

    private static SaveSystem _instance;
    public static SaveSystem Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindAnyObjectByType<SaveSystem>();
                if (_instance == null)
                {
                    GameObject singletonObject = new GameObject(typeof(SaveSystem).Name);
                    _instance = singletonObject.AddComponent<SaveSystem>();
                    DontDestroyOnLoad(singletonObject);
                }
                DontDestroyOnLoad(_instance);
            }
            return _instance;
        }
    }

    void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
            DontDestroyOnLoad(_instance);
        }
        else if (_instance != this)
            Destroy(gameObject);

        saveFile = Application.persistentDataPath + savePath;
        CreateSaveDataIfNull();
        InitializeAllStatistics();
        Statistics.LoadFromSave();
        Achievements.LoadFromSave();
    }

    public void SaveStats()
    {
        CreateSaveDataIfNull();
        SaveData data = GetSaveDataObject();
        
        foreach (KeyValuePair<string, Statistic> stat in Statistics.statistics)
        {
            SaveStatistic savedStat = data.statistics.Find(s => s.slug == stat.Key);

            //Add stat to save if doesn't exist
            if (savedStat == null) { data.statistics.Add(new SaveStatistic(stat.Key, stat.Value.GetValue())); }

            //Update stat if it already exists
            else savedStat.SetValue(stat.Value.GetValue());

        }

        string updatedJson = JsonUtility.ToJson(data);
        File.WriteAllText(saveFile, updatedJson);
    }

    public SaveStatistic[] GetAllSavedStatistics() 
    {
        List<SaveStatistic> statisticsList = new List<SaveStatistic>();

        CreateSaveDataIfNull();
        SaveData data = GetSaveDataObject();

        foreach (SaveStatistic stat in data.statistics)
        {
            statisticsList.Add(stat);
        }
        return statisticsList.ToArray();
    }

    public void SaveUnlockedAchievement(string achievementSlug)
    {
        CreateSaveDataIfNull();
        SaveData data = GetSaveDataObject();

        //If achievement doesn't exist in save data, create it
        if (data.achievements.Find(a => a.slug == achievementSlug) == null)
        {
            data.achievements.Add(new SaveAchievement(achievementSlug));
            string updatedJson = JsonUtility.ToJson(data);
            File.WriteAllText(saveFile, updatedJson);
        }
    }

    public SaveAchievement[] GetAllSavedAchievements()
    {
        List<SaveAchievement> achievementsList = new List<SaveAchievement>();

        CreateSaveDataIfNull();
        SaveData data = GetSaveDataObject();
        
        foreach (SaveAchievement achievement in data.achievements)
        {
            achievementsList.Add(achievement);
        }

        return achievementsList.ToArray();
    }

    private void CreateSaveDataIfNull()
    {
        if (!File.Exists(saveFile))
        {
            using (StreamWriter writer = File.CreateText(saveFile))
            {
                SaveData emptySaveData = new SaveData
                {
                    achievements = new List<SaveAchievement>(),
                    statistics = new List<SaveStatistic>()
                };

                string json = JsonUtility.ToJson(emptySaveData);
                writer.Write(json);
            }
        }
    }

    private SaveData GetSaveDataObject()
    {
        string json = File.ReadAllText(saveFile);
        return JsonUtility.FromJson<SaveData>(json);
    }

    private void InitializeAllStatistics()
    {
        CreateSaveDataIfNull();
        foreach (KeyValuePair <string, Statistic> stat in Statistics.statistics)
        {
            AddStatToSaveFile(stat.Key, stat.Value);
        }
    }

    private void AddStatToSaveFile(string slug, Statistic stat)
    {
        SaveData data = GetSaveDataObject();
        SaveStatistic foundStat = data.statistics.Find(s => s.slug == slug);

        //Not existing
        if (foundStat == null) { data.statistics.Add(new SaveStatistic(slug, stat.GetValue())); }
        string updatedJson = JsonUtility.ToJson(data);
        File.WriteAllText(saveFile, updatedJson);
    }
}
