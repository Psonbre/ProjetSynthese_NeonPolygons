using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

public class SensitivitySlider : MonoBehaviour
{
    private enum VolumeType { Master, Music, SFX};

    [SerializeField] private VolumeType volumeType;
    [SerializeField] private AudioClip testSound; 
    [SerializeField] private float testVolume;

    private Slider slider;
    private TextMeshProUGUI sensitivityText;

    // Start is called before the first frame update
    void Start()
    {
        sensitivityText = GetComponentInChildren<TextMeshProUGUI>();
        slider = GetComponent<Slider>();
        slider.value = GetVolumeInSettings();
        SetText();
        slider.onValueChanged.AddListener(delegate { SetVolume(); });
    }

    private void SetVolume()
    {
        SetVolumeInSettings();
        SetText();
        PlayExampleSound();
    }

    private void PlayExampleSound() {
        SoundManager.Instance.PlaySound(testSound, slider.value * testVolume);
    }

    private void SetText()
    {
        sensitivityText.text = Mathf.RoundToInt(slider.value * 100f).ToString() + " %";
    }

    private void SetVolumeInSettings()
    {
        switch (volumeType) 
        {
            case VolumeType.Master: SettingsManager.SetMasterVolume(slider.value); break;
            case VolumeType.Music: SettingsManager.SetMusicVolume(slider.value); break;
            case VolumeType.SFX: SettingsManager.SetSFXVolume(slider.value); break;
        }
    }

    private float GetVolumeInSettings()
    {
        switch (volumeType)
        {
            case VolumeType.Master: return SettingsManager.GetMasterVolume();
            case VolumeType.Music: return SettingsManager.GetMusicVolume();
            case VolumeType.SFX: return SettingsManager.GetSFXVolume();
            default: return 1;
        }
    }
}