using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class URLShortcut : MonoBehaviour
{
    [SerializeField] private string url;

    private void Start()
    {
        Button button = GetComponent<Button>();
        button.onClick.AddListener(OpenUrl);
    }

    public void OpenUrl()
    {
        Application.OpenURL(url);
    }
}
