using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using TMPro;
using UnityEngine;
using UnityEngine.SocialPlatforms;
using UnityEngine.SocialPlatforms.Impl;

public class AchievementsPanelManager : MonoBehaviour
{
    [SerializeField] private GameObject statisticsSection;
    [SerializeField] private GameObject achievementsSection;
    [SerializeField] private GameObject achievementPrefab;
    [SerializeField] private GameObject statisticsPrefab;

    const int achievementNbColumns = 3;
    const int achievementHeightRow = 135;
    const int achievementXOffset = 80;
    const int achievementYOffset = 50;
    const int achievementInitialX = -40;
    const int achievementInitialY = 30;

    const int statHeightRow = 55;
    const int statInitialX = 120;
    const int statYOffset = 30;
    const int statInitialY = 50;

    private void Start()
    {
        UpdatePanel();
    }

    public void UpdatePanel()
    {
        UpdateStatistics();
        UpdateAchievements();
    }

    public GameObject GetAchievementSection()
    {
        return achievementsSection;
    }

    private void UpdateAchievements()
    {
        int heightRows = CalculateAchievementHeightContent(Achievements.achievements.Count);
        SetHeight(achievementsSection, heightRows);

        //Move achievement
        int achievementsCount = 1;
        int yPosition = achievementInitialY;
        foreach (KeyValuePair<string, Achievement> entry in Achievements.achievements)
        {
            GameObject achievementGameObject = CreateUIItemAchievement(entry.Key);
            achievementGameObject.transform.SetParent(achievementsSection.transform, false);

            //Left column
            if (achievementsCount == 1 || (achievementsCount - 1) % achievementNbColumns == 0)
            {
                achievementGameObject.transform.position = new Vector2(achievementInitialX - achievementXOffset, yPosition);
            }
            //Right Column
            else if (achievementsCount == 3 || (achievementsCount - 3) % achievementNbColumns == 0)
            {
                achievementGameObject.transform.position = new Vector2(achievementInitialX + achievementXOffset, yPosition);
                yPosition -= achievementYOffset;
            }
            //Middle Column
            else
            {
                achievementGameObject.transform.position = new Vector2(achievementInitialX, yPosition);
            }
            achievementsCount++;
        }
        achievementsSection.transform.parent.parent.localScale = Vector2.one * 0.97f;
    }

    private void UpdateStatistics()
    {
        int heightRows = CalculateStatisticsHeightContent(Statistics.statistics.Count);
        SetHeight(statisticsSection, heightRows);

        //Move Statistics
        int yPosition = statInitialY;
        foreach (KeyValuePair<string, Statistic> stat in Statistics.statistics)
        {
            if(!stat.Value.GetHidden())
            {
                GameObject statGameObject = CreateUIItemStat(stat.Value);
                statGameObject.transform.SetParent(statisticsSection.transform, false);
                statGameObject.transform.position = new Vector2(statInitialX, yPosition);
                yPosition -= statYOffset;
            }
        }
    }

    private int CalculateAchievementHeightContent(int achievementsCount)
    {
        int rowsNeeded = Mathf.CeilToInt(achievementsCount / (float)achievementNbColumns);
        return rowsNeeded * achievementHeightRow + 60;
    }

    private int CalculateStatisticsHeightContent(int statsCount)
    {
        return statsCount * statHeightRow;
    }

    private void SetHeight(GameObject contentSection,int height)
    {
        Vector2 rectTransform = contentSection.GetComponent<RectTransform>().sizeDelta;
        rectTransform.y = height;
        contentSection.GetComponent<RectTransform>().sizeDelta = rectTransform;
    }

    private GameObject CreateUIItemAchievement(string slug)
    {
        GameObject newAchievement = Instantiate(achievementPrefab);
        newAchievement.GetComponent<UIItemAchievement>().Instanciate(slug);
        return newAchievement;
    }

    private GameObject CreateUIItemStat(Statistic statistic)
    {
        GameObject newStat = Instantiate(statisticsPrefab);
        newStat.GetComponent<UIItemStatistic>().Instanciate(statistic);
        return newStat;
    }
}
