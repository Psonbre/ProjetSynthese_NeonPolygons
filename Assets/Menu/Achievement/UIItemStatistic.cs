using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UIItemStatistic : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, ISelectHandler, IDeselectHandler
{
    const float UNSELECTED_SCALE = 0.9f;
    const float SELECTED_SCALE = 1f;
    const float SCREEN_CENTER = 0;
    const float SCROLL_SPEED = 1f;

    [SerializeField] private TextMeshProUGUI title;
    [SerializeField] private TextMeshProUGUI value;

    private ScrollRect scrollRect;
    private bool selected = false;

    private void Start()
    {
        scrollRect = transform.parent.parent.parent.GetComponent<ScrollRect>();
    }

    private void Update()
    {
        if (selected)
        {
            if (transform.position.y > SCREEN_CENTER + 10f) scrollRect.verticalNormalizedPosition = Mathf.MoveTowards(scrollRect.verticalNormalizedPosition, 1, SCROLL_SPEED * Time.deltaTime);
            else if (transform.position.y < SCREEN_CENTER - 10f) scrollRect.verticalNormalizedPosition = Mathf.MoveTowards(scrollRect.verticalNormalizedPosition, 0, SCROLL_SPEED * Time.deltaTime);
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        transform.localScale = Vector2.one * SELECTED_SCALE;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        transform.localScale = Vector2.one * UNSELECTED_SCALE;
    }

    public void OnSelect(BaseEventData eventData)
    {
        transform.localScale = Vector2.one * SELECTED_SCALE;
        selected = true;
    }

    public void OnDeselect(BaseEventData eventData)
    {
        transform.localScale = Vector2.one * UNSELECTED_SCALE;
        selected = false;
    }
    public void Instanciate(Statistic statistic)
    {
        title.SetText(statistic.GetTitle());
        if (statistic.GetTitle() == "Time played")
        {
            int totalSeconds = int.Parse(statistic.GetValue().ToString());
            int hours = totalSeconds / 3600;
            int minutes = (totalSeconds % 3600) / 60;
            string timeString = string.Format("{0:00}h{1:00}", hours, minutes);
            value.SetText(timeString);
        }
        else if(statistic.GetTitle() == "Shortest round" || statistic.GetTitle() == "Longest round")
        {
            int totalSeconds = int.Parse(statistic.GetValue().ToString());  
            int minutes = Mathf.FloorToInt(totalSeconds / 60f);
            int seconds = Mathf.FloorToInt(totalSeconds % 60f);
            string timeString = string.Format("{0:00}:{1:00}", minutes, seconds);
            value.SetText(timeString);
        }
        else
        {
            value.SetText(statistic.GetValue().ToString());
        }
    }
}
