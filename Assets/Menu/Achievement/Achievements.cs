using System;
using System.Collections.Generic;
using System.Linq;

[System.Serializable]
public static class Achievements
{
    public static void LoadFromSave()
    {
        SaveSystem.Instance.GetAllSavedAchievements().ToList().ForEach(savedAchievement => Unlock(savedAchievement.slug));
    }

    //Ins�rez ici la liste des achievements
    public static Dictionary<string, Achievement> achievements = new Dictionary<string, Achievement>()
    {
        {Harmony.Achievements.jumpMan,          new("Jump Man", "Jump 10000 times", 0, 10000, Harmony.Stats.jumps)},
        {Harmony.Achievements.dash,             new("Dash sho impreshive", "Dash 200 times", 0, 200, Harmony.Stats.dash)},
        {Harmony.Achievements.groundPound,      new("Enemies? Dead. Ground? Pounded.", "Ground Pound 200 times", 0, 200, Harmony.Stats.groundPound)},
        {Harmony.Achievements.hacker,           new("L + ratio + skill iss-...", "Win a game without dying a single time", 0, 1)},
        {Harmony.Achievements.levelDestroyed,   new( "Annihilation", "Destroy every pixel in a level", 0, 1)},
        {Harmony.Achievements.builder,          new("Bob the constructor", "Build 200 platforms", 0, 200, Harmony.Stats.build)},
        {Harmony.Achievements._8Players,        new("Congrats, you have friends!", "Start a game with 8 players", 0, 1)},
        {Harmony.Achievements.endGame,          new("GGWP", "Complete a game", 0, 1)},
        {Harmony.Achievements.chief,            new("Coffee ?", "T-bag 1000 times", 0, 1000, Harmony.Stats.tbag)},
        {Harmony.Achievements.fastGame,         new("I am speed", "Finish a round in less than 10 seconds", 0, 10)},
        {Harmony.Achievements.betrayal,         new("Betrayal", "Eliminate one of your teammate", 0, 1)},
        {Harmony.Achievements.training,         new("Becoming the very best", "Enter the training mode", 0, 1)},
        {Harmony.Achievements.wallbang,         new("Wallbang", "Shoot a player through a wall", 0,1)},
        {Harmony.Achievements.box,              new("Think outside the box", "Find the training mode secret", 0,1)},
        {Harmony.Achievements._100Rounds,       new("Bare minimum", "Play 100 rounds", 0, 100, Harmony.Stats.rounds)},
        {Harmony.Achievements._1000Rounds,      new("Touch grass", "Play 1000 rounds", 0, 1000, Harmony.Stats.rounds)},
        {Harmony.Achievements._240p,            new("Caught in 240p", "Destroy 102,240 pixels", 0, 102240, Harmony.Stats.pixels)},
        {Harmony.Achievements._720p,            new("Caught in 720p", "Destroy 921,600 pixels", 0, 921600, Harmony.Stats.pixels)},
        {Harmony.Achievements._1080p,           new("Caught in 1080p", "Destroy 2,073,600 pixels", 0, 2073600, Harmony.Stats.pixels)},
        {Harmony.Achievements._4K,              new("Caught in 4K", "Destroy 8,294,400 pixels", 0, 8294400, Harmony.Stats.pixels)},
        {Harmony.Achievements.all,              new("Sounds fishy...", "Get every achievement", 0, 1)},
    };

    public static void Unlock(string slug)
    {
        Achievement achievement = achievements[slug];
        if (achievement != null && achievement.GetTimeUnlocked() == null)
        {
            //Unlock locally
            achievement.SetTimeUnlocked(DateTime.Now);
            //Unlock visually
            if (AchievementPopUpManager.Instance) AchievementPopUpManager.Instance.ShowAchievement(slug);

            //Save achievements and stats while we're at it
            SaveSystem.Instance.SaveUnlockedAchievement(slug);
            SaveSystem.Instance.SaveStats();
        }

    }

    public static void UpdateAchievementProgression(string statSlug, long statValue)
    {
        foreach (KeyValuePair<string, Achievement> entry in achievements)
        {
            Achievement achievement = entry.Value;
            if (achievement.GetStatSlug() == statSlug)
            {
                achievement.SetCurrentProgress(statValue);
                if (achievement.IsReadyToBeUnlocked()) Unlock(entry.Key);
            }
        }
    }
}

[System.Serializable]
public class Achievement
{
    private string title;
    private string description;
    private DateTime? timeUnlocked;
    private int currentProgress;
    private long totalProgress;
    private string statSlug;

    public Achievement(string title, string description, int currentProgress, long totalProgress, string linkStat = null)
    {
        this.title = title;
        this.description = description;
        this.currentProgress = currentProgress;
        this.totalProgress = totalProgress;
        this.statSlug = linkStat;
    }
    public string GetTitle() { return title; }
    public string GetDescription() { return description; }
    public DateTime? GetTimeUnlocked() { return timeUnlocked; }
    public long GetCurrentProgress() { return currentProgress; }
    public long GetTotalProgress() {  return totalProgress; }
    public string GetStatSlug() {  return statSlug; }
    public void SetTimeUnlocked(DateTime dateTime)
    {
        this.timeUnlocked = dateTime;
    }
    public void SetCurrentProgress(long progress)
    {
        this.currentProgress = (int)progress;
    }
    public bool IsReadyToBeUnlocked()
    {
        return currentProgress >= totalProgress && timeUnlocked == null;
    }
}
