using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.VisualScripting;
using UnityEngine;

[System.Serializable]
public static class Statistics
{

    public static Dictionary<string, Statistic> statistics = new Dictionary<string, Statistic>
    {
        { Harmony.Stats.time ,new Statistic("Time played", false, 0) },
        { Harmony.Stats.eliminations ,new Statistic("Eliminations", false, 0) },
        { Harmony.Stats.pixels ,new Statistic("Pixels destroyed", false, 0) },
        { Harmony.Stats.games ,new Statistic("Games played", false, 0) },
        { Harmony.Stats.rounds ,new Statistic("Rounds played", false, 0) },
        { Harmony.Stats.shortestRound ,new Statistic("Shortest round", false, 0) },
        { Harmony.Stats.longestRound ,new Statistic("Longest round", false, 0) },
        { Harmony.Stats.jumps ,new Statistic("Jumps", false, 0) },
        { Harmony.Stats.tbag ,new Statistic("T-bags", false, 0) },
        { Harmony.Stats.dash ,new Statistic("Number of dash used", true, 0) },
        { Harmony.Stats.build ,new Statistic("Number of platform built", true, 0) },
        { Harmony.Stats.groundPound ,new Statistic("Number of grouds pounded", true, 0) },
    };

    public static void LoadFromSave()
    {
        //Load all saved stats to the static class
        foreach (SaveStatistic savedStat in SaveSystem.Instance.GetAllSavedStatistics().ToList())
        {
            statistics[savedStat.slug].SetValue(savedStat.GetValue());

            //Update related achievements' progression
            Achievements.UpdateAchievementProgression(savedStat.slug, savedStat.GetValue());
        }
    }

    public static void IncrementStat(string slug, int value)
    {
        Statistic stat = statistics[slug];
        stat.SetValue(stat.GetValue() + value);

        //Unlock related achievements
        Achievements.UpdateAchievementProgression(slug, stat.GetValue());
    }

    public static void SetStat(string slug, int value)
    {
        Statistic stat = statistics[slug];
        stat.SetValue(value);

        //Unlock related achievements
        Achievements.UpdateAchievementProgression(slug, stat.GetValue());
    }

    public static long GetValue(string slug)
    {
        return statistics[slug].GetValue();
    }
}


[System.Serializable]
public class Statistic
{
    public string title;
    public bool hidden;
    public long value;

    public Statistic(string title, bool hidden, long value)
    {
        this.title = title;
        this.hidden = hidden;
        this.value = value;
    }

    public string GetTitle()
    {
        return title;
    }

    public bool GetHidden()
    {
        return hidden;
    }

    public void SetValue(long value)
    {
        this.value = value;
    }

    public long GetValue()
    {
        return value;
    }
}


