using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEditor;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UIItemAchievement : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, ISelectHandler, IDeselectHandler
{
    const string ICONS_DIRECTORY = "UnlockableSkins/";
    const string DATE_UNLOCKED = "Unlocked on ";
    const float UNSELECTED_SCALE = 0.7f;
    const float SELECTED_SCALE = 0.80f;
    const float SCREEN_CENTER = -26;
    const float SCROLL_SPEED = 1f;
    [SerializeField] private TextMeshProUGUI title;
    [SerializeField] private TextMeshProUGUI description;
    [SerializeField] private Image icon;
    [SerializeField] private Slider progressBar;
    [SerializeField] private TextMeshProUGUI progressBarLabel;
    [SerializeField] private TextMeshProUGUI timeUnlocked;
    [SerializeField] private GameObject lockedOverlay;
    [SerializeField] private GameObject lockedImage;

    private ScrollRect scrollRect;

    private bool selected = false;

    private void Start()
    {
        scrollRect = transform.parent.parent.parent.GetComponent<ScrollRect>();
    }

    private void Update()
    {
        if (selected)
        {
            if (transform.position.y > SCREEN_CENTER + 10f) scrollRect.verticalNormalizedPosition = Mathf.MoveTowards(scrollRect.verticalNormalizedPosition, 1, SCROLL_SPEED * Time.deltaTime);
            else if (transform.position.y < SCREEN_CENTER - 10f) scrollRect.verticalNormalizedPosition = Mathf.MoveTowards(scrollRect.verticalNormalizedPosition, 0, SCROLL_SPEED * Time.deltaTime);
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        transform.localScale = Vector2.one * SELECTED_SCALE;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        transform.localScale = Vector2.one * UNSELECTED_SCALE;
    }

    public void OnSelect(BaseEventData eventData)
    {
        transform.localScale = Vector2.one * SELECTED_SCALE;
        selected = true;
    }

    public void OnDeselect(BaseEventData eventData)
    {
        transform.localScale = Vector2.one * UNSELECTED_SCALE;
        selected = false;
    }


    public void Instanciate(string slug)
    {
        Achievement achievement = Achievements.achievements[slug];

        title.SetText(achievement.GetTitle());
        description.SetText(achievement.GetDescription());
        //Check if there is a correct path
        string imageIcon = ICONS_DIRECTORY + slug;
        if (imageIcon != null ) 
        { 
            icon.sprite = Resources.Load<Sprite>(imageIcon); 
        }
        DateTime? dateTime = achievement.GetTimeUnlocked();
        //No date == not done
        if (dateTime == null)
        {
            long maxProgress = achievement.GetTotalProgress();
            long currentProgress = achievement.GetCurrentProgress();
            progressBar.maxValue = maxProgress;
            progressBar.value = currentProgress;
            progressBarLabel.SetText(currentProgress + "/" + maxProgress);
		}

        // Date == done
        if (dateTime != null)
        {
            lockedOverlay.SetActive(false);
            lockedImage.SetActive(false);
            progressBar.gameObject.SetActive(false);
            timeUnlocked.gameObject.SetActive(true);
            timeUnlocked.SetText(DATE_UNLOCKED + ((DateTime)dateTime).ToString("MM/dd/yyyy"));
        }
    }
}
