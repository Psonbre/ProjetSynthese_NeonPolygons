using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.UI;
using UnityEngine.UI;

public class BackButton : MonoBehaviour
{
    private InputSystemUIInputModule inputModule;
    private Button button;

    void Start()
    {
        inputModule = FindAnyObjectByType<InputSystemUIInputModule>();
        button = GetComponent<Button>();

        // Subscribe to the cancel action's performed event
        inputModule.cancel.action.performed += OnCancelPressed;
    }

    void OnCancelPressed(InputAction.CallbackContext context)
    {
        button.onClick.Invoke();
    }

    void OnDestroy()
    {
        inputModule.cancel.action.performed -= OnCancelPressed;
    }
}
