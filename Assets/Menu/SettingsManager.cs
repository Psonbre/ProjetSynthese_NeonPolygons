using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class SettingsManager
{
    public static string MasterVolume = "MasterVolume";
    public static string MusicVolume = "MusicVolume";
    public static string SFXVolume = "SFXVolume";

    public static void SetMasterVolume(float newSensitivity)
    {
        SetVolume(newSensitivity, MasterVolume);
        MusicManager.Instance.UpdateMusicVolume();
    }

    public static void SetMusicVolume(float newSensitivity)
    {
        SetVolume(newSensitivity, MusicVolume);
        MusicManager.Instance.UpdateMusicVolume();
    }

    public static void SetSFXVolume(float newSensitivity)
    {
        SetVolume(newSensitivity, SFXVolume);
    }

    public static float GetMasterVolume()
    {
        return GetVolume(MasterVolume);
    }

    public static float GetMusicVolume()
    {
        return GetVolume(MusicVolume);
    }

    public static float GetSFXVolume()
    {
        return GetVolume(SFXVolume);
    }

    private static void SetVolume(float newVolume, string volumeType) 
    {
        PlayerPrefs.SetFloat(volumeType, newVolume);
    }

    private static float GetVolume(string volumeType) 
    {
        return PlayerPrefs.GetFloat(volumeType, 1);
    } 
}