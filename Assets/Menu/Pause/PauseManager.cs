using Harmony;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.UI;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PauseManager : MonoBehaviour
{
    private const float PAUSE_DELAY = 0.1f;

    private float currentPauseDelay = 0;
    private bool isPausing = false;
    private EventSystem eventSystem;
    private InputSystemUIInputModule inputSystemUIInputModule;
    private GameObject currentPanel;
    private bool isUsingMouse = true;
    private bool pauseInputModeMouse = false;
    [SerializeField] private GameObject pausePanel;
    [SerializeField] private GameObject mainPanel;
    [SerializeField] private GameObject settingsPanel;

    private void Start()
    {
        eventSystem = EventSystem.current;
        inputSystemUIInputModule = eventSystem.transform.GetComponent<InputSystemUIInputModule>();
    }

    private void Update()
    {
        currentPauseDelay = Mathf.Max(0, currentPauseDelay - Time.unscaledDeltaTime);
        if (isPausing)
        {
            if (pauseInputModeMouse)
            {
                CheckMouseInput();
                CheckKeyboardInput();
            }
            else CheckGamepadInput();
        }
    }

    public void TogglePause(PlayerInput playerInput)
    {
        if (currentPauseDelay == 0)
        {
            if (isPausing)
            {
                if(playerInput.actions == inputSystemUIInputModule.actionsAsset)
                {
                    Unpause();
                    ResetPauseDelay();
                }
            }
            else
            {
                SetInputMode(playerInput);
                Pause();
                ResetPauseDelay();
            }
        }
    }

    private void SetInputMode(PlayerInput playerInput)
    {
        inputSystemUIInputModule.actionsAsset = playerInput.actions;
        if (playerInput.currentControlScheme == PlayerControls.KEYBOARD_CONTROL_SCHEME) pauseInputModeMouse = true;
        else pauseInputModeMouse = false;
    }

    private void ResetPauseDelay()
    {
        currentPauseDelay = PAUSE_DELAY;
    }

    private void TogglePanel()
    {
        pausePanel.SetActive(isPausing);
        if (isPausing ) Time.timeScale = 0;
        else Time.timeScale = 1;
    }

    public void OnResumeClicked()
    {
        Unpause();
    }

    public void OnSettingsClicked()
    {
        ChangePanel(settingsPanel);
    }

    public void onBackClicked()
    {
        ChangePanel(mainPanel);
    }

    public void OnQuitClicked()
    {
        MusicManager.Instance.PlayMainMusic();
        if (MultiplayerManager.Instance != null) MultiplayerManager.Instance.TryResetAllVibrations();
        if (MultiplayerManager.Instance != null) MultiplayerManager.Instance.EmptyDontDestroyOnLoad();
        if (TrainingModeManager.Instance != null) Destroy(TrainingModeManager.Instance.gameObject);
        if (SaveSystem.Instance != null) SaveSystem.Instance.SaveStats();

        if (SceneManager.GetActiveScene().name == Scenes.GameScene)
        {
            SceneManager.LoadSceneAsync(Scenes.CharacterSelectScene);
        }
        else
        {
            SceneManager.LoadSceneAsync(Scenes.MainMenuScene);
        }
        Unpause();
    }

    /*private IEnumerator Countdown()
    {
        pausePanel.gameObject.SetActive(false);
        countdownPanel.gameObject.SetActive(true);
        int timeLeft = 3;

        while (timeLeft > 0)
        {
            countdownPanel.GetComponentInChildren<TextMeshProUGUI>().text = timeLeft.ToString();
            yield return new WaitForSecondsRealtime(1);
            timeLeft--;
        }
        Time.timeScale = 1;
        countdownPanel.gameObject.SetActive(false);
        Unpause();
    }*/

    private void SetPlayerActivation()
    {
        Player[] players = FindObjectsByType<Player>(FindObjectsSortMode.None);
        foreach (var p in players)
        {
            if (isPausing) p.SetPreventAllInputs(true);
            else p.SetPreventAllInputs(false);
        }
    }

    private void Unpause()
    {
        ChangePanel(mainPanel);
        isPausing = false;
        SetPlayerActivation();
        TogglePanel();
    }

    private void Pause()
    {
        ChangePanel(mainPanel);
        isPausing = true;
        SetPlayerActivation();
        TogglePanel();
    }

    private void CheckMouseInput()
    {
        Vector2 mouseDelta = Mouse.current.delta.ReadValue();

        if (mouseDelta.x != 0 || mouseDelta.y != 0)
        {
            isUsingMouse = true;
            SetSelectedItem(null);
        }
    }

    private void CheckKeyboardInput()
    {
        if (Keyboard.current.anyKey.isPressed)
        {
            OnAnyKeyPressed();
        }
    }

    private void CheckGamepadInput()
    {
        Gamepad gamepad = Gamepad.current;

        if (gamepad != null)
        {
            foreach (InputControl control in gamepad.allControls)
            {
                if (control.IsPressed())
                {
                    OnAnyKeyPressed();
                }
            }
        }
    }

    private void OnAnyKeyPressed()
    {
        if (isUsingMouse)
        {
            isUsingMouse = false;
            SetSelectedItem(GetFirstItemOnPanel());
        }
    }

    private GameObject GetFirstItemOnPanel()
    {
        for (int i = 0; i < currentPanel.transform.childCount; i++)
        {
            Transform child = currentPanel.transform.GetChild(i);
            Button button = child.GetComponent<Button>();

            // Check if the child has a Button component
            if (button != null)
            {
                return child.gameObject;
            }
        }
        return null;
    }

    private void SetSelectedItem(GameObject gameObject)
    {
        eventSystem.SetSelectedGameObject(gameObject);
    }

    private void ChangePanel(GameObject panel)
    {
        if (currentPanel) currentPanel.SetActive(false);
        currentPanel = panel;
        currentPanel.SetActive(true);
        if (!isUsingMouse)
        {
            SetSelectedItem(GetFirstItemOnPanel());
        }
    }
}