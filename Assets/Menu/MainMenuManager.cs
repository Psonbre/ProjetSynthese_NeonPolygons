using Harmony;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuManager : MonoBehaviour
{
    [SerializeField] private GameObject mainPanel;
    [SerializeField] private GameObject settingsPanel;
    [SerializeField] private GameObject creditsPanel;
    [SerializeField] private GameObject achievementsPanel;

    [SerializeField] private List<AudioClip> buttonPressSounds;
    [SerializeField] private List<AudioClip> buttonOverSounds;
    [SerializeField] private float buttonPressSoundVolume;
    [SerializeField] private float buttonOverSoundVolume;

    private Banner banner;
    private List<GameObject> panels;
    private EventSystem eventSystem;
    private GameObject currentPanel;
    private GameObject mainPanelButtonClicked;
    private GameObject previousSelectedGameobject;

    private bool changedPanel = false;
    private bool isUsingMouse = true;
    private bool isPlaying = false;


    private void Start()
    {
        currentPanel = mainPanel;
        panels = GetSerializedGameObjects();
        eventSystem = EventSystem.current; 
    }

    private void Update()
    {
        if (eventSystem.currentSelectedGameObject != previousSelectedGameobject && !isUsingMouse && !changedPanel)
            SoundManager.Instance.PlayRandomSoundEffect(buttonOverSounds, buttonOverSoundVolume);

        CheckMouseInput();
        CheckKeyboardInput();
        CheckGamepadInput();

        previousSelectedGameobject = eventSystem.currentSelectedGameObject;
        changedPanel = false;
    }

    public void OnPlayClicked()
    {
        if (!isPlaying)
        {
            SoundManager.Instance.PlayRandomSoundEffect(buttonPressSounds, buttonPressSoundVolume);
            StartCoroutine(ShowPlayPanel());
        }
    }

    private Banner GetBanner()
    {
        if (!banner)
            return GameObject.FindAnyObjectByType<Banner>();
        return banner;
    }

    public void OnSettingsClicked()
    {
        if (!isPlaying)
        {
            SoundManager.Instance.PlayRandomSoundEffect(buttonPressSounds, buttonPressSoundVolume);
            ShowPanel(settingsPanel);
        }
    }

    public void OnCreditsClicked()
    {
        if (!isPlaying)
        {
            SoundManager.Instance.PlayRandomSoundEffect(buttonPressSounds, buttonPressSoundVolume);
            ShowPanel(creditsPanel);
        }
    }

    public void OnQuitClicked()
    {
        if (!isPlaying)
        {
            if (MultiplayerManager.Instance != null) MultiplayerManager.Instance.TryResetAllVibrations();
            SoundManager.Instance.PlayRandomSoundEffect(buttonPressSounds, buttonPressSoundVolume);
            Application.Quit();
        }
    }

    public void OnTrainingClicked()
    {
        if (!isPlaying)
        {
            SoundManager.Instance.PlayRandomSoundEffect(buttonPressSounds, buttonPressSoundVolume);
            StartCoroutine(ShowTrainingPanel());
        }
    }

    public void OnAchievementsClicked()
    {
        if (!isPlaying)
        {
            SoundManager.Instance.PlayRandomSoundEffect(buttonPressSounds, buttonPressSoundVolume);
            ShowPanel(achievementsPanel);
        }
    }

    public void OnReturnClicked()
    {
        SoundManager.Instance.PlayRandomSoundEffect(buttonPressSounds, buttonPressSoundVolume);
        ShowPanel(mainPanel);
        mainPanelButtonClicked = null;
    }

    private IEnumerator ShowPlayPanel()
    {
        isPlaying = true;
        GetBanner().CloseFully(.5f);
        yield return new WaitForSeconds(1f);
        SceneManager.LoadSceneAsync("Scenes/CharacterSelectScene");
    }

    private IEnumerator ShowTrainingPanel()
    {
        GetBanner().CloseFully(.5f);
        yield return new WaitForSeconds(1f);
        SceneManager.LoadSceneAsync("Scenes/TrainingMode");
    }

    private void ShowPanel(GameObject showPanel)
    {
        foreach (GameObject panel in panels) 
        {
            if (panel == showPanel)
            {
                panel.SetActive(true);
            }
            else
            {
                panel.SetActive(false);
            }
        }
        currentPanel = showPanel;
        if (!isUsingMouse)
        {
            if (mainPanelButtonClicked != null)
            {
                SetSelectedItem(mainPanelButtonClicked);
                mainPanelButtonClicked = null;
            }
            else
            {
                mainPanelButtonClicked = eventSystem.currentSelectedGameObject;
                SetSelectedItem(GetFirstSelectableGrandchild(currentPanel));
            }
        }
        changedPanel = true;
    }

    private void SetSelectedItem(GameObject gameObject)
    {
        eventSystem.SetSelectedGameObject(gameObject);
    }

    private GameObject GetFirstSelectableGrandchild(GameObject parent)
    {
        if (parent.GetComponent<Selectable>()) return parent;

        foreach (Transform child in parent.transform)
        {
            GameObject selectedGrandchild = GetFirstSelectableGrandchild(child.gameObject);
            if (selectedGrandchild != null) return selectedGrandchild;
        }

        return null;
    }

    private List<GameObject> GetSerializedGameObjects()
    {
        List<GameObject> serializedGameObjects = new List<GameObject>();

        Type type = GetType();
        FieldInfo[] fields = type.GetFields(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);

        foreach (var field in fields)
        {
            if (Attribute.IsDefined(field, typeof(SerializeField)) && field.FieldType == typeof(GameObject))
            {
                GameObject serializedGameObject = field.GetValue(this) as GameObject;
                if (serializedGameObject != null)
                {
                    serializedGameObjects.Add(serializedGameObject);
                }
            }
        }

        return serializedGameObjects;
    }

    private void CheckMouseInput()
    {
        Vector2 mouseDelta = Mouse.current.delta.ReadValue();
        Vector2 scrollDelta = Mouse.current.scroll.ReadValue();
        if (mouseDelta != Vector2.zero || scrollDelta != Vector2.zero)
        {
            isUsingMouse = true;
            RemoveButtonSelected();
        }
    }

    private void CheckKeyboardInput()
    {
        if (Keyboard.current.anyKey.isPressed)
        {
            OnAnyKeyPressed();
        }
    }

    private void CheckGamepadInput()
    {
        Gamepad gamepad = Gamepad.current;

        if (gamepad != null)
        {
            foreach (InputControl control in gamepad.allControls)
            {
                if (control.IsPressed())
                {
                    OnAnyKeyPressed();
                }
            }
        }
    }

    private void OnAnyKeyPressed()
    {
        if (isUsingMouse) 
        {
            isUsingMouse = false;
            SetSelectedItem(GetFirstSelectableGrandchild(currentPanel));
        }
    }

    private void RemoveButtonSelected()
    {
        SetSelectedItem(null);
    }

}
