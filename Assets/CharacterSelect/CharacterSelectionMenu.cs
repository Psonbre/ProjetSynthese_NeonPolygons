using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEditor;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.UI;
using UnityEngine.UI;

public class CharacterSelectionMenu : MonoBehaviour
{
    [SerializeField] private SpriteRenderer playerSprite;
    [SerializeField] private Player player;

    [SerializeField] private List<AudioClip> leaveSound;
    [SerializeField] private List<AudioClip> readySound;
    [SerializeField] private List<AudioClip> unreadySound;
    [SerializeField] private List<AudioClip> switchLeftRightSound;
    [SerializeField] private List<AudioClip> switchUpDownSound;

    [SerializeField] private float leaveSoundVolume = 1f;
    [SerializeField] private float readySoundVolume = 1f;
    [SerializeField] private float unreadySoundVolume = 1f;
    [SerializeField] private float switchLeftRightSoundVolume = 1f;
    [SerializeField] private float switchUpDownSoundVolume = 1f;

    [SerializeField] private float cursorDampSpeed = 50;


    private bool ready = false;

    private LinkedList<Color> teams = new LinkedList<Color>();

    PlayerInput playerInput;
    private int selectedSkinIndex = -1;
    private MultiplayerManager multiplayerManager;
    private CharacterSelectionManager characterSelectionManager;
    private GameObject parent;
    private AbilitySelection currentAbility;
    private float defaultAbilityScale;
    private LinkedList<AbilitySelection> abilities = new();
    private Vector2 defaultPosition;
    private Color currentTeam;
    private RawImage background;
    private GameObject colorButton;
    private GameObject colorLeft;
    private GameObject colorRight;
    private Image abilityImage;
    private TextMeshProUGUI abilityDesc;
    private GameObject abilityLeft;
    private GameObject abilityRight;
    private GameObject teamButton;
    private GameObject teamLeft;
    private GameObject teamRight;
    private GameObject abilityBackground;
    private GameObject abilityDescBackground;
    private SpriteRenderer readyForeground;
    private TextMeshProUGUI readyText;
    private bool acceptInputs = false;

    private GameObject cursorLeft;
    private GameObject cursorRight;
    private float cursorLeftX;
    private float cursorRightX;

    private MultiplayerEventSystem eventSystem;
    private Vector2 defaultPlayerPos;

    private GameObject previousSelectedButton;

    private void Awake()
    {
        multiplayerManager = MultiplayerManager.Instance;
        characterSelectionManager = CharacterSelectionManager.Instance;
        playerInput = GetComponentInParent<PlayerInput>();
        parent = transform.parent.gameObject;
        eventSystem = GetComponent<MultiplayerEventSystem>();
        background = transform.Find(Harmony.GameObjects.Background).GetComponent<RawImage>();
        background.material = new(background.material);
		colorButton = transform.Find(Harmony.GameObjects.Color).gameObject;
		colorLeft = transform.Find(Harmony.GameObjects.ColorLeft).gameObject;
        colorRight = transform.Find(Harmony.GameObjects.ColorRight).gameObject;
        abilityBackground = transform.Find(Harmony.GameObjects.AbilityBackground).gameObject;
        abilityDescBackground = transform.Find(Harmony.GameObjects.AbilityDescBackground).gameObject;
		abilityImage = abilityBackground.GetComponentInChildren<Image>();
        abilityDesc = abilityDescBackground.GetComponentInChildren<TextMeshProUGUI>();
        abilityLeft = transform.Find(Harmony.GameObjects.AbilityLeft).gameObject;
        abilityRight = transform.Find(Harmony.GameObjects.AbilityRight).gameObject;
        teamButton = transform.Find(Harmony.GameObjects.Team).gameObject;
        teamLeft = transform.Find(Harmony.GameObjects.TeamLeft).gameObject;
        teamRight = transform.Find(Harmony.GameObjects.TeamRight).gameObject;
        cursorLeft = transform.Find(Harmony.GameObjects.CursorLeft).gameObject;
        cursorRight = transform.Find(Harmony.GameObjects.CursorRight).gameObject;
        cursorRightX = cursorRight.transform.localPosition.x;
        cursorLeftX = cursorLeft.transform.localPosition.x;
        readyForeground = transform.Find(Harmony.GameObjects.ReadyForeground).GetComponent<SpriteRenderer>();
        readyText = readyForeground.transform.Find(Harmony.GameObjects.Canvas).GetComponentInChildren<TextMeshProUGUI>();
    }
    private void Start()
    {
        defaultPlayerPos = player.gameObject.transform.position;
        characterSelectionManager.GetAllTeams().ForEach(color => { teams.AddFirst(color); });
        player.SetTeam(characterSelectionManager.GetTeam(0));
        SetTeam(characterSelectionManager.GetTeam(playerInput.playerIndex));
        defaultPosition = transform.position;
        characterSelectionManager.GetAbilities().ForEach(ability => abilities.AddFirst(ability));
        defaultAbilityScale = abilityBackground.transform.localScale.x;
        readyForeground.gameObject.SetActive(false);
		NextAbility();
        NextSkin();
		Invoke("ActivateAcceptInputs", 0.3f);
        characterSelectionManager.CheckAllPlayersReady();
    }

    private void Update()
    {
        if (previousSelectedButton != eventSystem.currentSelectedGameObject && !ready)
        {
            if (!eventSystem.currentSelectedGameObject.name.Contains("Left") && !eventSystem.currentSelectedGameObject.name.Contains("Right"))
            {
                cursorLeft.transform.position = new(cursorLeft.transform.position.x, eventSystem.currentSelectedGameObject.transform.position.y);
                cursorRight.transform.position = new(cursorRight.transform.position.x, eventSystem.currentSelectedGameObject.transform.position.y);
                if (previousSelectedButton && previousSelectedButton != eventSystem.currentSelectedGameObject) {
                    SoundManager.Instance.PlayRandomSoundEffect(switchUpDownSound, switchUpDownSoundVolume);
                }
                previousSelectedButton = eventSystem.currentSelectedGameObject;
                StopAllCoroutines();
                StartCoroutine(MoveRightCursor());
                StartCoroutine(MoveLeftCursor());
            }
            else
            {
                if (eventSystem.currentSelectedGameObject == colorRight) NextSkin();
                else if (eventSystem.currentSelectedGameObject == colorLeft) PreviousSkin();
                else if (eventSystem.currentSelectedGameObject == teamLeft) PreviousTeam();
                else if (eventSystem.currentSelectedGameObject == teamRight) NextTeam();
                else if (eventSystem.currentSelectedGameObject == abilityLeft) PreviousAbility();
                else if (eventSystem.currentSelectedGameObject == abilityRight) NextAbility();
                SoundManager.Instance.PlayRandomSoundEffect(switchLeftRightSound, switchLeftRightSoundVolume);
            }

        }
        Vector2 targetPosition = new(defaultPosition.x + Mathf.Cos(Time.time + characterSelectionManager.GetTeamNumber(currentTeam) - 4) * 2, defaultPosition.y + Mathf.Sin(Time.time + characterSelectionManager.GetTeamNumber(currentTeam)) * 2);
        transform.position = new(Mathf.MoveTowards(transform.position.x, targetPosition.x, Time.deltaTime), Mathf.MoveTowards(transform.position.y, targetPosition.y, Time.deltaTime));
        abilityBackground.transform.localScale = Vector2.one * Mathf.MoveTowards(abilityBackground.transform.localScale.x, defaultAbilityScale, Time.deltaTime * 2);
        if (readyText.isActiveAndEnabled) readyText.fontSize = Mathf.MoveTowards(readyText.fontSize, 1.5f, Time.deltaTime * 10);

        if (eventSystem.currentSelectedGameObject == abilityImage.gameObject && !ready)
            abilityDescBackground.gameObject.SetActive(true);
        else
            abilityDescBackground.gameObject.SetActive(false);
    }
    public void NextSkin()
    {
        do
        {
            selectedSkinIndex++;
            playerSprite.sprite = characterSelectionManager.GetClosestAvailableSkin(selectedSkinIndex);
        } while (playerSprite.sprite == null);

        player.SetSkin(playerSprite.sprite);

        eventSystem.SetSelectedGameObject(colorButton);
        BouncePlayer();
        StopAllCoroutines();
        StartCoroutine(MoveRightCursor());
    }

	public void PreviousSkin()
	{
		do
		{
            selectedSkinIndex--;
			playerSprite.sprite = characterSelectionManager.GetClosestAvailableSkin(selectedSkinIndex);
		} while (playerSprite.sprite == null);

		player.SetSkin(playerSprite.sprite);

		eventSystem.SetSelectedGameObject(colorButton);
        BouncePlayer();
        StopAllCoroutines();
        StartCoroutine(MoveLeftCursor());
    }
    public void SetTeam(Color team)
    {
        PreviousTeam();
        while (currentTeam != team)
            NextTeam();
    }
    public void NextTeam()
    {
		eventSystem.SetSelectedGameObject(teamButton);
		Color team = teams.Last.Value;
		teams.RemoveLast();
		if (!teams.Contains(player.GetTeam())) teams.AddFirst(player.GetTeam());
		teamButton.GetComponentInChildren<TextMeshProUGUI>().text = "Team " + characterSelectionManager.GetTeamNumber(team);
        player.SetTeam(team);
		background.material.SetColor("_Color", team);
        currentTeam = team;
		StopAllCoroutines();
		StartCoroutine(MoveRightCursor());
	}

    public void PreviousTeam()
    {
		eventSystem.SetSelectedGameObject(teamButton);
		Color team = teams.First.Value;
		teams.RemoveFirst();
		if (!teams.Contains(player.GetTeam())) teams.AddLast(player.GetTeam());
		teamButton.GetComponentInChildren<TextMeshProUGUI>().text = "Team " + characterSelectionManager.GetTeamNumber(team);
		player.SetTeam(team);
		background.material.SetColor("_Color", team);
        currentTeam = team;
		StopAllCoroutines();
		StartCoroutine(MoveLeftCursor());
	}

    public void NextAbility()
    {
        if (currentAbility != null) abilities.AddLast(currentAbility);
		currentAbility = abilities.First.Value;
		abilities.RemoveFirst();
        player.ActivateAbilityByType(System.Type.GetType(currentAbility.abilityName));
        eventSystem.SetSelectedGameObject(abilityImage.gameObject);
		abilityImage.sprite = currentAbility.abilitySprite;
        abilityDesc.text = "<u>" + currentAbility.abilityTitle + "</u>\n\n" + currentAbility.abilityDesc;
        abilityBackground.transform.localScale = Vector2.one * defaultAbilityScale * 1.3f;
		StopAllCoroutines();
		StartCoroutine(MoveRightCursor());
	}

    public void PreviousAbility()
    {
		if (currentAbility != null) abilities.AddFirst(currentAbility);
		currentAbility = abilities.Last.Value;
		abilities.RemoveLast();
        player.ActivateAbilityByType(System.Type.GetType(currentAbility.abilityName));
        eventSystem.SetSelectedGameObject(abilityImage.gameObject);
        abilityImage.sprite = currentAbility.abilitySprite;
        abilityDesc.text = "<u>" + currentAbility.abilityTitle + "</u>\n\n" + currentAbility.abilityDesc;
        abilityBackground.transform.localScale = Vector2.one * defaultAbilityScale * 1.3f;
		StopAllCoroutines();
		StartCoroutine(MoveLeftCursor());
	}
    private void ActivateAcceptInputs()
    {
        acceptInputs = true;
	}

    public void ConfirmCharacterSelection()
    {
        if (!ready && acceptInputs)
        {
            ready = true;
            SoundManager.Instance.PlayRandomSoundEffect(readySound, readySoundVolume);
            readyForeground.gameObject.SetActive(true);
            readyText.fontSize = 3;
            eventSystem.enabled = false;
            characterSelectionManager.CheckAllPlayersReady();
        }
    }

    public void CancelCharacterSelection()
    {
        if (ready)
        {
            ready = false;
            SoundManager.Instance.PlayRandomSoundEffect(unreadySound, unreadySoundVolume);
            readyForeground.gameObject.SetActive(false);
            eventSystem.enabled = true;
            characterSelectionManager.CheckAllPlayersReady();
        }
    }

    public void OnPlayerLost()
    {
        characterSelectionManager.RemoveCustomizationMenu(parent);
        multiplayerManager.RemovePlayer(player);
        ready = false;
        SoundManager.Instance.PlayRandomSoundEffect(leaveSound, leaveSoundVolume);
        readyForeground.gameObject.SetActive(false);
        eventSystem.enabled = true;
        characterSelectionManager.CheckAllPlayersReady();
        Destroy(parent);
    }
    public Color GetCurrentTeam()
    {
        return currentTeam;
    }
    public bool IsReady { get { return ready; } }
    

    private void BouncePlayer()
    {
        player.transform.position = defaultPlayerPos;
        player.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
    }

    private IEnumerator MoveLeftCursor()
    {
        cursorLeft.transform.localPosition = new(cursorLeftX - 5, cursorLeft.transform.localPosition.y);

        while (cursorLeft.transform.localPosition.x != cursorLeftX)
        {
            cursorLeft.transform.localPosition = new(Mathf.MoveTowards(cursorLeft.transform.localPosition.x, cursorLeftX, cursorDampSpeed * Time.deltaTime), cursorLeft.transform.localPosition.y);
            yield return null;
        }
    }
    private IEnumerator MoveRightCursor()
    {
        cursorRight.transform.localPosition = new(cursorRightX + 5, cursorRight.transform.localPosition.y);

        while (cursorRight.transform.localPosition.x != cursorRightX)
        {
            cursorRight.transform.localPosition = new(Mathf.MoveTowards(cursorRight.transform.localPosition.x, cursorRightX, cursorDampSpeed * Time.deltaTime), cursorRight.transform.localPosition.y);
            yield return null;
        }
    }
}
