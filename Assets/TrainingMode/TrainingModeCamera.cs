using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrainingModeCamera : MonoBehaviour
{
	[SerializeField] private float moveSpeed = 1000f;
	private Vector2 targetPosition;

	public void SetTargetPosition(Vector2 newTarget)
	{
		targetPosition = newTarget;	
	}

	public Vector2 GetTargetPosition()
	{
		return targetPosition;
	}

	private void Update()
	{
		if (targetPosition != (Vector2)transform.position) transform.position = new(Mathf.MoveTowards(transform.position.x, targetPosition.x, moveSpeed * Time.deltaTime), Mathf.MoveTowards(transform.position.y, targetPosition.y, moveSpeed * Time.deltaTime), -10);
	}
}
