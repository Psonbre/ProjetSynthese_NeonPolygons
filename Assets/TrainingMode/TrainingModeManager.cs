using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrainingModeManager : MonoBehaviour
{
	public static TrainingModeManager Instance { get; set; }

	private readonly Vector2 DUMMY_SPAWN = new Vector2(800, 50);
    private readonly Vector2 PLAYER_SPAWN = new Vector2(-260, -100);

    [SerializeField] private Banner banner;
    [SerializeField] private Player player;
    [SerializeField] private TrainingModeCamera cam;
	[SerializeField] private PlayerUI playerUI;
	[SerializeField] private List<Transform> spawns;
	[SerializeField] private GameObject levelPrefab;
	[SerializeField] private GameObject currentLevel;
	[SerializeField] private Player dummy;
	[SerializeField] private PlayerUI dummyUI;
	private float loadingTimeBeforeAchievement = 1f;
    private int currentScreen = 0;
	private int oldScreen = 0;

    void Awake()
    {
        Instance = this;
    }

	private void Start()
	{
        banner.OpenFully(1f);
        player.GetComponent<Player>().SetSkin(player.GetComponent<Player>().GetSkin());
        player.SetTrainingSpawn(PLAYER_SPAWN);
		player.SetTeam(Color.white);
		dummy.SetTrainingSpawn(DUMMY_SPAWN);
		dummy.SetTeam(Color.red);
		playerUI.SetPlayer(player.GetComponent<Player>());
		dummyUI.SetPlayer(dummy.GetComponent<Player>());

        StartCoroutine(TrainingAchievement());
    }

    private IEnumerator TrainingAchievement()
	{
		yield return new WaitForSeconds(loadingTimeBeforeAchievement);

		//Achievement training 
		Achievements.Unlock(Harmony.Achievements.training);
	}

	private void Update()
	{
		playerUI.UpdatePlayerUI();
		dummyUI.UpdatePlayerUI();
		if (player.transform.position.x > cam.GetTargetPosition().x + 320)
		{
			currentScreen++;
			cam.SetTargetPosition(cam.GetTargetPosition() + new Vector2(640, 0));
		}
		if (player.transform.position.x < cam.GetTargetPosition().x - 320)
		{
			currentScreen--;
			cam.SetTargetPosition(cam.GetTargetPosition() - new Vector2(640, 0));
		}

		if (oldScreen != currentScreen)
		{
			oldScreen = currentScreen;
			if (currentScreen >= 0 && currentScreen < spawns.Count)
			{
				player.SetTrainingSpawn(spawns[currentScreen].position);
			}
		}
	}

	public void UpdatePlayerUIAbility(Sprite abilitySprite)
	{
		playerUI.SetActiveAbility(player.GetActivatedAbility(), abilitySprite);
	}

	public void ReloadLevel(Player deadPlayer)
	{
        if (deadPlayer == player)
        {
			Destroy(currentLevel);
			currentLevel = Instantiate(levelPrefab);
		}
	}
}
