using UnityEngine;

public class SecretSkin : MonoBehaviour
{
	[SerializeField] private float floatSpeed = 100f;
	private Vector2 defaultPosition;
	private float randomFloatOffsetX;
	private float randomFloatOffsetY;


	private void Start()
	{
		randomFloatOffsetX = Random.value;
		randomFloatOffsetY = Random.value;
		defaultPosition = transform.position;
	}
	private void OnTriggerEnter2D(Collider2D collision)
	{
		if (collision.CompareTag(Harmony.Tags.Player))
		{
			Achievements.Unlock(Harmony.Achievements.box);
			Destroy(gameObject);
		}
	}

	private void Update()
	{
		transform.position = new(defaultPosition.x + Mathf.Cos(randomFloatOffsetX + Time.time * floatSpeed), defaultPosition.y + Mathf.Sin(randomFloatOffsetY + Time.time * floatSpeed));
		transform.rotation.eulerAngles.Set(0, 0, Mathf.Cos(Time.time * floatSpeed) * 40f);
	}
}
