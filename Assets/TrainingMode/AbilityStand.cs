using System;
using UnityEngine;

public class AbilityStand : MonoBehaviour
{
	[SerializeField] private string abilityType;
	[SerializeField] private float floatSpeed = 2.5f;
	private Vector2 defaultPosition;
	private float randomFloatOffsetX;
	private float randomFloatOffsetY;
	private SpriteRenderer spriteRenderer;


	private void Start()
	{
		spriteRenderer = GetComponent<SpriteRenderer>();
		randomFloatOffsetX = UnityEngine.Random.value;
		randomFloatOffsetY = UnityEngine.Random.value;
		defaultPosition = transform.position;
	}
	private void OnTriggerEnter2D(Collider2D collision)
	{
		if (collision.CompareTag(Harmony.Tags.Player))
		{
			collision.GetComponent<Player>().ActivateAbilityByType(Type.GetType(abilityType));
			TrainingModeManager.Instance.UpdatePlayerUIAbility(spriteRenderer.sprite);
		}
	}

	private void Update()
	{
		transform.position = new(defaultPosition.x + Mathf.Cos(randomFloatOffsetX + Time.time * floatSpeed), defaultPosition.y + Mathf.Sin(randomFloatOffsetY + Time.time * floatSpeed));
		transform.rotation.eulerAngles.Set(0, 0, Mathf.Cos(Time.time * floatSpeed) * 40f);
	}
}
