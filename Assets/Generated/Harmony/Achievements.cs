// AUTO GENERATED CODE - ANY MODIFICATION WILL BE OVERRIDDEN
// GENERATED ON 2024-02-07 15:34

// Invalid names are commented out. Here are the rules :
// - Non-alphanumerical characters (like spaces) are prohibited. Underscores are allowed.
// - Per C# language rules, starting with a non alphabetic character is prohibited.
// - Per C# language rules, using the same name as it's class is prohibited. Ex : "GameObjects", "Tags" or "Layers".
// - Per C# language rules, using a keyword is prohibited. Ex : "object", "abstract" or "float".

using UnityEngine;

namespace Harmony
{
    public static partial class Achievements
    {
        public const string jumpMan = "jumpMan";
        public const string dash = "dash";
        public const string groundPound = "groundPound";
        public const string hacker = "hacker";
        public const string levelDestroyed = "levelDestroyed";
        public const string builder = "builder";
        public const string _8Players = "8Players";
        public const string endGame = "endGame";
        public const string chief = "chief";
        public const string fastGame = "fastGame";
        public const string betrayal = "betrayal";
        public const string training = "training";
        public const string wallbang = "wallbang";
        public const string box = "box";
        public const string _100Rounds = "100Rounds";
        public const string _1000Rounds = "1000Rounds";
        public const string _240p = "240p";
        public const string _720p = "720p";
        public const string _1080p = "1080p";
        public const string _4K = "4K";
        public const string all = "all";
    }
}