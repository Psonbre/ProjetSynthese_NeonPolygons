// AUTO GENERATED CODE - ANY MODIFICATION WILL BE OVERRIDDEN
// GENERATED ON 2024-02-07 15:34

// Invalid names are commented out. Here are the rules :
// - Non-alphanumerical characters (like spaces) are prohibited. Underscores are allowed.
// - Per C# language rules, starting with a non alphabetic character is prohibited.
// - Per C# language rules, using the same name as it's class is prohibited. Ex : "GameObjects", "Tags" or "Layers".
// - Per C# language rules, using a keyword is prohibited. Ex : "object", "abstract" or "float".

using UnityEngine;

namespace Harmony
{
    public static partial class Stats
    {
        public const string games = "games";
        public const string eliminations = "eliminations";
        public const string jumps = "jumps";
        public const string tbag = "tbag";
        public const string dash = "dash";
        public const string build = "build";
        public const string groundPound = "groundPound";
        public const string pixels = "pixels";
        public const string time = "time";
        public const string shortestRound = "shortestRound";
        public const string longestRound = "longestRound";
        public const string rounds = "rounds";
    }
}