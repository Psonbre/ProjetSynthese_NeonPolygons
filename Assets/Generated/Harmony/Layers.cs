// AUTO GENERATED CODE - ANY MODIFICATION WILL BE OVERRIDDEN
// GENERATED ON 2024-02-07 15:34

// Invalid names are commented out. Here are the rules :
// - Non-alphanumerical characters (like spaces) are prohibited. Underscores are allowed.
// - Per C# language rules, starting with a non alphabetic character is prohibited.
// - Per C# language rules, using the same name as it's class is prohibited. Ex : "GameObjects", "Tags" or "Layers".
// - Per C# language rules, using a keyword is prohibited. Ex : "object", "abstract" or "float".

using UnityEngine;

namespace Harmony
{
    public static partial class Layers
    {
        public static readonly Layer Bounds = new Layer(LayerMask.NameToLayer("Bounds"));
        public static readonly Layer Bullets = new Layer(LayerMask.NameToLayer("Bullets"));
        public static readonly Layer Chain = new Layer(LayerMask.NameToLayer("Chain"));
        public static readonly Layer Default = new Layer(LayerMask.NameToLayer("Default"));
        // "Ignore Raycast" is invalid.
        public static readonly Layer Pixels = new Layer(LayerMask.NameToLayer("Pixels"));
        public static readonly Layer Platforms = new Layer(LayerMask.NameToLayer("Platforms"));
        public static readonly Layer Player = new Layer(LayerMask.NameToLayer("Player"));
        public static readonly Layer PostProcess = new Layer(LayerMask.NameToLayer("PostProcess"));
        public static readonly Layer Shield = new Layer(LayerMask.NameToLayer("Shield"));
        public static readonly Layer TransparentFX = new Layer(LayerMask.NameToLayer("TransparentFX"));
        public static readonly Layer UI = new Layer(LayerMask.NameToLayer("UI"));
        public static readonly Layer Water = new Layer(LayerMask.NameToLayer("Water"));
        public static readonly Layer Laser = new Layer(LayerMask.NameToLayer("Laser"));
        public static readonly Layer PlayerAimAssist = new Layer(LayerMask.NameToLayer("PlayerAimAssist"));
        public struct Layer
        {
            public int Index;
            public int Mask;

            public Layer(int index)
            {
                this.Index = index;
                this.Mask = 1 << index;
            }
        }

    }
}