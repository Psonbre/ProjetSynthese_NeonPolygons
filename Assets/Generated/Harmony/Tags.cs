// AUTO GENERATED CODE - ANY MODIFICATION WILL BE OVERRIDDEN
// GENERATED ON 2024-02-07 15:34

// Invalid names are commented out. Here are the rules :
// - Non-alphanumerical characters (like spaces) are prohibited. Underscores are allowed.
// - Per C# language rules, starting with a non alphabetic character is prohibited.
// - Per C# language rules, using the same name as it's class is prohibited. Ex : "GameObjects", "Tags" or "Layers".
// - Per C# language rules, using a keyword is prohibited. Ex : "object", "abstract" or "float".

using UnityEngine;

namespace Harmony
{
    public static partial class Tags
    {
        public const string Bounds = "Bounds";
        public const string Bullet = "Bullet";
        public const string DestroyArea = "DestroyArea";
        public const string EditorOnly = "EditorOnly";
        public const string Explosion = "Explosion";
        public const string Finish = "Finish";
        public const string GameController = "GameController";
        public const string Ground = "Ground";
        public const string MainCamera = "MainCamera";
        public const string Player = "Player";
        public const string Respawn = "Respawn";
        public const string Untagged = "Untagged";
        public const string ExplosionDestroyArea = "ExplosionDestroyArea";
        public const string ExplosionGround = "ExplosionGround";
        public const string Portal = "Portal";
        public const string Laser = "Laser";
        public const string Mine = "Mine";
        public const string PlayerAimAssist = "PlayerAimAssist";
    }
}