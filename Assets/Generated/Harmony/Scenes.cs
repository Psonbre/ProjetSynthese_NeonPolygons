// AUTO GENERATED CODE - ANY MODIFICATION WILL BE OVERRIDDEN
// GENERATED ON 2024-02-07 15:34

// Invalid names are commented out. Here are the rules :
// - Non-alphanumerical characters (like spaces) are prohibited. Underscores are allowed.
// - Per C# language rules, starting with a non alphabetic character is prohibited.
// - Per C# language rules, using the same name as it's class is prohibited. Ex : "GameObjects", "Tags" or "Layers".
// - Per C# language rules, using a keyword is prohibited. Ex : "object", "abstract" or "float".

using UnityEngine;

namespace Harmony
{
    public static partial class Scenes
    {
        public const string CharacterSelectScene = "CharacterSelectScene";
        public const string EndGameScene = "EndGameScene";
        // "Example Use Cases" is invalid.
        public const string GameScene = "GameScene";
        public const string TrainingScene = "TrainingMode";
        // "Image Preview Scene" is invalid.
        public const string MainMenuScene = "MainMenuScene";
        public const string MainMenuSceneAdam = "MainMenuSceneAdam";
        
    }
}