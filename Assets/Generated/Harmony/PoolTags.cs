// AUTO GENERATED CODE - ANY MODIFICATION WILL BE OVERRIDDEN
// GENERATED ON 2024-02-02 08:39

// Invalid names are commented out. Here are the rules :
// - Non-alphanumerical characters (like spaces) are prohibited. Underscores are allowed.
// - Per C# language rules, starting with a non alphabetic character is prohibited.
// - Per C# language rules, using the same name as it's class is prohibited. Ex : "GameObjects", "Tags" or "Layers".
// - Per C# language rules, using a keyword is prohibited. Ex : "object", "abstract" or "float".

using UnityEngine;

namespace Harmony
{
    public static partial class PoolTags
    {
        public const string trail = "trail";
        public const string double_jump_particles = "double_jump_particles";
        public const string land_particles = "land_particles";
        public const string handgun_bullet = "handgun_bullet";
        public const string ak47_bullet = "ak47_bullet";
        public const string sniper_bullet = "sniper_bullet";
        public const string explosion = "explosion";
        public const string shotgun_bullet = "shotgun_bullet";
        public const string rocket = "rocket";
        public const string grenade = "grenade";
        public const string c4 = "c4";
        public const string mine = "mine";
        public const string bounce_bullet = "bounce_bullet";
        public const string crack = "crack";
        public const string sniper_explosion = "sniper_explosion";
        public const string death_particles = "death_particles";
    }
}