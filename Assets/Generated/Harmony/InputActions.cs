// AUTO GENERATED CODE - ANY MODIFICATION WILL BE OVERRIDDEN
// GENERATED ON 2024-02-07 15:34

// Invalid names are commented out. Here are the rules :
// - Non-alphanumerical characters (like spaces) are prohibited. Underscores are allowed.
// - Per C# language rules, starting with a non alphabetic character is prohibited.
// - Per C# language rules, using the same name as it's class is prohibited. Ex : "GameObjects", "Tags" or "Layers".
// - Per C# language rules, using a keyword is prohibited. Ex : "object", "abstract" or "float".

using UnityEngine;

namespace Harmony
{
    public static partial class InputActions
    {
        public const string Ability = "Ability";
        public const string Cancel = "Cancel";
        public const string Click = "Click";
        public const string Fire = "Fire";
        public const string Jump = "Jump";
        public const string LookLeftStick = "LookLeftStick";
        public const string LookMouse = "LookMouse";
        public const string LookRightStick = "LookRightStick";
        public const string MiddleClick = "MiddleClick";
        public const string Move = "Move";
        public const string Navigate = "Navigate";
        public const string Pause = "Pause";
        public const string Point = "Point";
        public const string RightClick = "RightClick";
        public const string ScrollWheel = "ScrollWheel";
        public const string Submit = "Submit";
        public const string Tbag = "T-bag";
        public const string TrackedDeviceOrientation = "TrackedDeviceOrientation";
        public const string TrackedDevicePosition = "TrackedDevicePosition";
        
    }
}