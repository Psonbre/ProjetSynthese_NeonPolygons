// AUTO GENERATED CODE - ANY MODIFICATION WILL BE OVERRIDDEN
// GENERATED ON 2024-02-07 15:34

// Invalid names are commented out. Here are the rules :
// - Non-alphanumerical characters (like spaces) are prohibited. Underscores are allowed.
// - Per C# language rules, starting with a non alphabetic character is prohibited.
// - Per C# language rules, using the same name as it's class is prohibited. Ex : "GameObjects", "Tags" or "Layers".
// - Per C# language rules, using a keyword is prohibited. Ex : "object", "abstract" or "float".

using UnityEngine;

namespace Harmony
{
    public static partial class GameObjects
    {
        // "1st" is invalid.
        // "2nd" is invalid.
        // "3rd" is invalid.
        public const string AButton = "AButton";
        public const string AK47Bullet = "AK47Bullet";
        public const string Ability = "Ability";
        public const string AbilityBackground = "AbilityBackground";
        public const string AbilityDescBackground = "AbilityDescBackground";
        public const string AbilityImage = "AbilityImage";
        public const string AbilityLeft = "AbilityLeft";
        public const string AbilityName = "AbilityName";
        public const string AbilityRight = "AbilityRight";
        public const string AbilitySelection = "AbilitySelection";
        public const string Ak47 = "Ak47";
        public const string Back = "Back";
        public const string BackButton = "BackButton";
        public const string Background = "Background";
        public const string BackgroundCooldown = "BackgroundCooldown";
        public const string BackgroundUI = "BackgroundUI";
        // "Battery Fill 1" is invalid.
        // "Battery Fill 2" is invalid.
        // "Battery No Composit" is invalid.
        // "Battery No Dash" is invalid.
        public const string Border = "Border";
        public const string BounceBullet = "BounceBullet";
        public const string BounceGun = "BounceGun";
        public const string C4 = "C4";
        public const string C4Projectile = "C4Projectile";
        public const string Canvas = "Canvas";
        public const string Center = "Center";
        public const string CharacterSelectionMenu = "CharacterSelectionMenu";
        public const string Color = "Color";
        public const string ColorLeft = "ColorLeft";
        public const string ColorRight = "ColorRight";
        public const string ColorSelection = "ColorSelection";
        public const string Connected = "Connected";
        public const string Controls = "Controls";
        public const string CooldownUI = "CooldownUI";
        public const string Countdown = "Countdown";
        public const string CountdownBanner = "CountdownBanner";
        public const string CreditsButton = "CreditsButton";
        public const string CursorLeft = "CursorLeft";
        public const string CursorRight = "CursorRight";
        public const string DebugManager = "DebugManager";
        public const string DestructionShape = "DestructionShape";
        // "Directional Light" is invalid.
        public const string Display = "Display";
        public const string DoubleJumpParticles = "DoubleJumpParticles";
        public const string Electricity = "Electricity";
        public const string EventSystem = "EventSystem";
        public const string Explosion = "Explosion";
        public const string FPS = "FPS";
        public const string FPSCanvas = "FPSCanvas";
        public const string Fill = "Fill";
        // "Fill Area" is invalid.
        // "Fill Image (Slide the fill amount)" is invalid.
        // "Free Flat Arrow 1 E Icon" is invalid.
        // "Free Flat Arrow 1 N Icon" is invalid.
        // "Free Flat Arrow 1 NE Icon" is invalid.
        // "Free Flat Arrow 1 NW Icon" is invalid.
        // "Free Flat Arrow 1 S Icon" is invalid.
        // "Free Flat Arrow 1 SE Icon" is invalid.
        // "Free Flat Arrow 1 SW Icon" is invalid.
        // "Free Flat Arrow 1 W Icon" is invalid.
        // "Free Flat Arrow 2 E Icon" is invalid.
        // "Free Flat Arrow 2 N Icon" is invalid.
        // "Free Flat Arrow 2 NE Icon" is invalid.
        // "Free Flat Arrow 2 NW Icon" is invalid.
        // "Free Flat Arrow 2 S Icon" is invalid.
        // "Free Flat Arrow 2 SE Icon" is invalid.
        // "Free Flat Arrow 2 SW Icon" is invalid.
        // "Free Flat Arrow 2 W Icon" is invalid.
        // "Free Flat Arrow 3 E Icon" is invalid.
        // "Free Flat Arrow 3 N Icon" is invalid.
        // "Free Flat Arrow 3 NE Icon" is invalid.
        // "Free Flat Arrow 3 NW Icon" is invalid.
        // "Free Flat Arrow 3 S Icon" is invalid.
        // "Free Flat Arrow 3 SE Icon" is invalid.
        // "Free Flat Arrow 3 SW Icon" is invalid.
        // "Free Flat Arrow 3 W Icon" is invalid.
        // "Free Flat Arrow 4 E Icon" is invalid.
        // "Free Flat Arrow 4 N Icon" is invalid.
        // "Free Flat Arrow 4 NE Icon" is invalid.
        // "Free Flat Arrow 4 NW Icon" is invalid.
        // "Free Flat Arrow 4 S Icon" is invalid.
        // "Free Flat Arrow 4 SE Icon" is invalid.
        // "Free Flat Arrow 4 SW Icon" is invalid.
        // "Free Flat Arrow 4 W Icon" is invalid.
        // "Free Flat Arrow 5 E Icon" is invalid.
        // "Free Flat Arrow 5 N Icon" is invalid.
        // "Free Flat Arrow 5 NE Icon" is invalid.
        // "Free Flat Arrow 5 NW Icon" is invalid.
        // "Free Flat Arrow 5 S Icon" is invalid.
        // "Free Flat Arrow 5 SE Icon" is invalid.
        // "Free Flat Arrow 5 SW Icon" is invalid.
        // "Free Flat Arrow 5 W Icon" is invalid.
        // "Free Flat Arrow 6 E Icon" is invalid.
        // "Free Flat Arrow 6 N Icon" is invalid.
        // "Free Flat Arrow 6 NE Icon" is invalid.
        // "Free Flat Arrow 6 NW Icon" is invalid.
        // "Free Flat Arrow 6 S Icon" is invalid.
        // "Free Flat Arrow 6 SE Icon" is invalid.
        // "Free Flat Arrow 6 SW Icon" is invalid.
        // "Free Flat Arrow 6 W Icon" is invalid.
        // "Free Flat Arrow 7 E Icon" is invalid.
        // "Free Flat Arrow 7 N Icon" is invalid.
        // "Free Flat Arrow 7 NE Icon" is invalid.
        // "Free Flat Arrow 7 NW Icon" is invalid.
        // "Free Flat Arrow 7 S Icon" is invalid.
        // "Free Flat Arrow 7 SE Icon" is invalid.
        // "Free Flat Arrow 7 SW Icon" is invalid.
        // "Free Flat Arrow 7 W Icon" is invalid.
        // "Free Flat Arrow Curved Left 1 Icon" is invalid.
        // "Free Flat Arrow Curved Left 2 Icon" is invalid.
        // "Free Flat Arrow Curved Right 1 Icon" is invalid.
        // "Free Flat Arrow Curved Right 2 Icon" is invalid.
        // "Free Flat Award Icon" is invalid.
        // "Free Flat Bag 1 Icon" is invalid.
        // "Free Flat Bag 2 Icon" is invalid.
        // "Free Flat Banner Icon" is invalid.
        // "Free Flat Battery Empty Icon" is invalid.
        // "Free Flat Battery No Dash Icon" is invalid.
        // "Free Flat Battery No Dashless Icon" is invalid.
        // "Free Flat Battery No Icon" is invalid.
        // "Free Flat Battery Smooth 1 Icon" is invalid.
        // "Free Flat Battery Smooth 2 Icon" is invalid.
        // "Free Flat Battery Smooth 3 Icon" is invalid.
        // "Free Flat Battery Smooth 4 Icon" is invalid.
        // "Free Flat Battery Smooth 5 Icon" is invalid.
        // "Free Flat Battery Smooth Fill Icon" is invalid.
        // "Free Flat Battery Tick 1 Icon" is invalid.
        // "Free Flat Battery Tick 2 Icon" is invalid.
        // "Free Flat Battery Tick 3 Icon" is invalid.
        // "Free Flat Battery Tick 4 Icon" is invalid.
        // "Free Flat Battery Tick 5 Icon" is invalid.
        // "Free Flat Battery Tick 6 Icon" is invalid.
        // "Free Flat Battery Tick Fill Icon" is invalid.
        // "Free Flat Border Double 1-1pt Icon" is invalid.
        // "Free Flat Border Double 1-2pt Icon" is invalid.
        // "Free Flat Border Double 1-3pt Icon" is invalid.
        // "Free Flat Border Double 1-4pt Icon" is invalid.
        // "Free Flat Border Double 1-5pt Icon" is invalid.
        // "Free Flat Border Double 2-1pt Icon" is invalid.
        // "Free Flat Border Double 3-1pt Icon" is invalid.
        // "Free Flat Border Double 4-1pt Icon" is invalid.
        // "Free Flat Border Double 5-1pt Icon" is invalid.
        // "Free Flat Border Single 1pt Icon" is invalid.
        // "Free Flat Border Single 2pt Icon" is invalid.
        // "Free Flat Border Single 3pt Icon" is invalid.
        // "Free Flat Border Single 4pt Icon" is invalid.
        // "Free Flat Border Single 5pt Icon" is invalid.
        // "Free Flat Border Single 6pt Icon" is invalid.
        // "Free Flat Border Single 7pt Icon" is invalid.
        // "Free Flat Border Single 8pt Icon" is invalid.
        // "Free Flat Button Blank A Icon" is invalid.
        // "Free Flat Button Blank B Icon" is invalid.
        // "Free Flat Button Blank Circle Icon" is invalid.
        // "Free Flat Button Blank Cross Icon" is invalid.
        // "Free Flat Button Blank Left 1 Icon" is invalid.
        // "Free Flat Button Blank Left 2 Icon" is invalid.
        // "Free Flat Button Blank Left 3 Icon" is invalid.
        // "Free Flat Button Blank Left Bumper Icon" is invalid.
        // "Free Flat Button Blank Left Icon" is invalid.
        // "Free Flat Button Blank Left Trigger Icon" is invalid.
        // "Free Flat Button Blank Right 1 Icon" is invalid.
        // "Free Flat Button Blank Right 2 Icon" is invalid.
        // "Free Flat Button Blank Right 3 Icon" is invalid.
        // "Free Flat Button Blank Right Bumper Icon" is invalid.
        // "Free Flat Button Blank Right Icon" is invalid.
        // "Free Flat Button Blank Right Trigger Icon" is invalid.
        // "Free Flat Button Blank Square Icon" is invalid.
        // "Free Flat Button Blank Triangle Icon" is invalid.
        // "Free Flat Button Blank X Icon" is invalid.
        // "Free Flat Button Blank Y Icon" is invalid.
        // "Free Flat Button Solid A Icon" is invalid.
        // "Free Flat Button Solid B Icon" is invalid.
        // "Free Flat Button Solid Circle Icon" is invalid.
        // "Free Flat Button Solid Cross Icon" is invalid.
        // "Free Flat Button Solid Left 1 Icon" is invalid.
        // "Free Flat Button Solid Left 2 Icon" is invalid.
        // "Free Flat Button Solid Left 3 Icon" is invalid.
        // "Free Flat Button Solid Left Bumper Icon" is invalid.
        // "Free Flat Button Solid Left Icon" is invalid.
        // "Free Flat Button Solid Left Trigger Icon" is invalid.
        // "Free Flat Button Solid Right 1 Icon" is invalid.
        // "Free Flat Button Solid Right 2 Icon" is invalid.
        // "Free Flat Button Solid Right 3 Icon" is invalid.
        // "Free Flat Button Solid Right Bumper Icon" is invalid.
        // "Free Flat Button Solid Right Icon" is invalid.
        // "Free Flat Button Solid Right Trigger Icon" is invalid.
        // "Free Flat Button Solid Square Icon" is invalid.
        // "Free Flat Button Solid Triangle Icon" is invalid.
        // "Free Flat Button Solid X Icon" is invalid.
        // "Free Flat Button Solid Y Icon" is invalid.
        // "Free Flat Cart Icon" is invalid.
        // "Free Flat Chat 1 Bars Icon" is invalid.
        // "Free Flat Chat 1 Blank Icon" is invalid.
        // "Free Flat Chat 1 Ellipsis Icon" is invalid.
        // "Free Flat Chat 1 Exclaim Icon" is invalid.
        // "Free Flat Chat 1 Question Icon" is invalid.
        // "Free Flat Chat 2 Bars Icon" is invalid.
        // "Free Flat Chat 2 Blank Icon" is invalid.
        // "Free Flat Chat 2 Ellipsis Icon" is invalid.
        // "Free Flat Chat 2 Exclaim Icon" is invalid.
        // "Free Flat Chat 2 Question Icon" is invalid.
        // "Free Flat Chat 3 Bars Icon" is invalid.
        // "Free Flat Chat 3 Blank Icon" is invalid.
        // "Free Flat Chat 3 Ellipsis Icon" is invalid.
        // "Free Flat Chat 3 Exclaim Icon" is invalid.
        // "Free Flat Chat 3 Question Icon" is invalid.
        // "Free Flat Circle Dash Icon" is invalid.
        // "Free Flat Cloud 1 Icon" is invalid.
        // "Free Flat Cloud 2 Icon" is invalid.
        // "Free Flat Cloud Icon" is invalid.
        // "Free Flat Connection 0 Icon" is invalid.
        // "Free Flat Connection 1 Icon" is invalid.
        // "Free Flat Connection 2 Icon" is invalid.
        // "Free Flat Connection 3 Icon" is invalid.
        // "Free Flat Connection 4 Icon" is invalid.
        // "Free Flat Disk 1 Icon" is invalid.
        // "Free Flat Disk 2 Icon" is invalid.
        // "Free Flat Disk 3 Icon" is invalid.
        // "Free Flat Disk 4 Icon" is invalid.
        // "Free Flat Disk 5 Icon" is invalid.
        // "Free Flat Download 1 Icon" is invalid.
        // "Free Flat Download 2 Icon" is invalid.
        // "Free Flat Download Cloud Icon" is invalid.
        // "Free Flat Edit Icon" is invalid.
        // "Free Flat Exit Door Icon" is invalid.
        // "Free Flat Exit Left Icon" is invalid.
        // "Free Flat Exit Right Icon" is invalid.
        // "Free Flat Funnel Icon" is invalid.
        // "Free Flat Gear 1 Icon" is invalid.
        // "Free Flat Gear 2 Icon" is invalid.
        // "Free Flat Gear 3 Icon" is invalid.
        // "Free Flat Gear 4 Icon" is invalid.
        // "Free Flat Gradient Circle (Bottom) Icon" is invalid.
        // "Free Flat Gradient Circle (Left) Icon" is invalid.
        // "Free Flat Gradient Circle (Right) Icon" is invalid.
        // "Free Flat Gradient Circle (Top) Icon" is invalid.
        // "Free Flat Gradient Circle Icon" is invalid.
        // "Free Flat Gradient Horizontal (Left) Icon" is invalid.
        // "Free Flat Gradient Horizontal (Middle) Icon" is invalid.
        // "Free Flat Gradient Horizontal (Right) Icon" is invalid.
        // "Free Flat Gradient Vertical (Bottom) Icon" is invalid.
        // "Free Flat Gradient Vertical (Middle) Icon" is invalid.
        // "Free Flat Gradient Vertical (Top) Icon" is invalid.
        // "Free Flat Hat Solid Blank Icon" is invalid.
        // "Free Flat Hat Solid E Icon" is invalid.
        // "Free Flat Hat Solid ENW Icon" is invalid.
        // "Free Flat Hat Solid ESW Icon" is invalid.
        // "Free Flat Hat Solid EW Icon" is invalid.
        // "Free Flat Hat Solid N Icon" is invalid.
        // "Free Flat Hat Solid NE Icon" is invalid.
        // "Free Flat Hat Solid NES Icon" is invalid.
        // "Free Flat Hat Solid NESW Icon" is invalid.
        // "Free Flat Hat Solid NS Icon" is invalid.
        // "Free Flat Hat Solid NW Icon" is invalid.
        // "Free Flat Hat Solid NWS Icon" is invalid.
        // "Free Flat Hat Solid S Icon" is invalid.
        // "Free Flat Hat Solid SE Icon" is invalid.
        // "Free Flat Hat Solid SW Icon" is invalid.
        // "Free Flat Hat Solid W Icon" is invalid.
        // "Free Flat Hat Split Blank Icon" is invalid.
        // "Free Flat Hat Split E Icon" is invalid.
        // "Free Flat Hat Split ENW Icon" is invalid.
        // "Free Flat Hat Split ESW Icon" is invalid.
        // "Free Flat Hat Split EW Icon" is invalid.
        // "Free Flat Hat Split N Icon" is invalid.
        // "Free Flat Hat Split NE Icon" is invalid.
        // "Free Flat Hat Split NES Icon" is invalid.
        // "Free Flat Hat Split NESW Icon" is invalid.
        // "Free Flat Hat Split NS Icon" is invalid.
        // "Free Flat Hat Split NW Icon" is invalid.
        // "Free Flat Hat Split NWS Icon" is invalid.
        // "Free Flat Hat Split S Icon" is invalid.
        // "Free Flat Hat Split SE Icon" is invalid.
        // "Free Flat Hat Split SW Icon" is invalid.
        // "Free Flat Hat Split W Icon" is invalid.
        // "Free Flat Heart Empty Icon" is invalid.
        // "Free Flat Heart Filled Icon" is invalid.
        // "Free Flat Home Icon" is invalid.
        // "Free Flat Hyphen Icon" is invalid.
        // "Free Flat Leaderboard Icon" is invalid.
        // "Free Flat Lock Closed Icon" is invalid.
        // "Free Flat Lock Open Icon" is invalid.
        // "Free Flat Magnify Icon" is invalid.
        // "Free Flat Magnify Minus Icon" is invalid.
        // "Free Flat Magnify Plus Icon" is invalid.
        // "Free Flat Media Left Double Icon" is invalid.
        // "Free Flat Media Left Full Icon" is invalid.
        // "Free Flat Media Left Icon" is invalid.
        // "Free Flat Media Pause Icon" is invalid.
        // "Free Flat Media Right Double Icon" is invalid.
        // "Free Flat Media Right Full Icon" is invalid.
        // "Free Flat Media Right Icon" is invalid.
        // "Free Flat Media Stop Icon" is invalid.
        // "Free Flat Menu 1 Icon" is invalid.
        // "Free Flat Menu 10 Icon" is invalid.
        // "Free Flat Menu 11 Icon" is invalid.
        // "Free Flat Menu 12 Icon" is invalid.
        // "Free Flat Menu 2 Icon" is invalid.
        // "Free Flat Menu 3 Icon" is invalid.
        // "Free Flat Menu 4 Icon" is invalid.
        // "Free Flat Menu 5 Icon" is invalid.
        // "Free Flat Menu 6 Icon" is invalid.
        // "Free Flat Menu 7 Icon" is invalid.
        // "Free Flat Menu 8 Icon" is invalid.
        // "Free Flat Menu 9 Icon" is invalid.
        // "Free Flat Mic Icon" is invalid.
        // "Free Flat Mic No Cross Icon" is invalid.
        // "Free Flat Mic No Crossless Icon" is invalid.
        // "Free Flat Mic No Icon" is invalid.
        // "Free Flat Mic Off Dash Icon" is invalid.
        // "Free Flat Mic Off Dashless Icon" is invalid.
        // "Free Flat Mic Off Icon" is invalid.
        // "Free Flat Move In Icon" is invalid.
        // "Free Flat Move In_1 Icon" is invalid.
        // "Free Flat Move Out Icon" is invalid.
        // "Free Flat Move Out_1 Icon" is invalid.
        // "Free Flat Music Icon" is invalid.
        // "Free Flat Music No Dash Icon" is invalid.
        // "Free Flat Music No Dashless Icon" is invalid.
        // "Free Flat Music No Icon" is invalid.
        // "Free Flat Mute Icon" is invalid.
        // "Free Flat Pen Icon" is invalid.
        // "Free Flat People 1 Icon" is invalid.
        // "Free Flat People 2 Icon" is invalid.
        // "Free Flat People 3 Icon" is invalid.
        // "Free Flat People 4 Icon" is invalid.
        // "Free Flat Person Icon" is invalid.
        // "Free Flat Plus Icon" is invalid.
        // "Free Flat Signal Up 0 Icon" is invalid.
        // "Free Flat Signal Up 011 Icon" is invalid.
        // "Free Flat Signal Up 1 Icon" is invalid.
        // "Free Flat Signal Up 101 Icon" is invalid.
        // "Free Flat Signal Up 2 Icon" is invalid.
        // "Free Flat Signal Up 3 Icon" is invalid.
        // "Free Flat Signal Up Off Icon" is invalid.
        // "Free Flat Solid Brush Icon" is invalid.
        // "Free Flat Star Empty Icon" is invalid.
        // "Free Flat Star Filled Icon" is invalid.
        // "Free Flat Thumbs Down Icon" is invalid.
        // "Free Flat Thumbs Up Icon" is invalid.
        // "Free Flat Toggle Cage Icon" is invalid.
        // "Free Flat Toggle Centre Icon" is invalid.
        // "Free Flat Toggle Left Icon" is invalid.
        // "Free Flat Toggle Right Icon" is invalid.
        // "Free Flat Toggle Thumb Centre Icon" is invalid.
        // "Free Flat Toggle Thumb Left Icon" is invalid.
        // "Free Flat Toggle Thumb Right Icon" is invalid.
        // "Free Flat Upload 1 Icon" is invalid.
        // "Free Flat Upload 2 Icon" is invalid.
        // "Free Flat Upload Cloud Icon" is invalid.
        // "Free Flat Volume 0 Icon" is invalid.
        // "Free Flat Volume 1 Icon" is invalid.
        // "Free Flat Volume 2 Icon" is invalid.
        // "Free Flat Volume 3 Icon" is invalid.
        // "Free Flat Volume 4 Icon" is invalid.
        // "Free Flat Volume 5 Icon" is invalid.
        public const string GameManager = "GameManager";
        public const string GameTitle = "GameTitle";
        public const string GameUI = "GameUI";
        public const string Grenade = "Grenade";
        public const string GrenadeProjectile = "GrenadeProjectile";
        public const string HandGunBullet = "HandGunBullet";
        public const string Handgun = "Handgun";
        public const string Handle = "Handle";
        // "Handle Slide Area" is invalid.
        public const string HoldBack = "HoldBack";
        public const string Icon = "Icon";
        public const string Icons = "Icons";
        public const string Image = "Image";
        public const string Killbox = "Killbox";
        public const string LB = "LB";
        public const string LT = "LT";
        public const string LandParticleSystem = "LandParticleSystem";
        public const string LazerBeam = "LazerBeam";
        public const string LazerGun = "LazerGun";
        public const string LeftJoystick = "LeftJoystick";
        public const string Level = "Level";
        // "Level 1" is invalid.
        // "Level 2" is invalid.
        // "Main Camera" is invalid.
        public const string MainManager = "MainManager";
        public const string MainPanel = "MainPanel";
        public const string MasterVolume = "MasterVolume";
        // "Mic Off Composit" is invalid.
        // "Mic Off Dash" is invalid.
        public const string Mine = "Mine";
        public const string MultiplicatorUI = "MultiplicatorUI";
        public const string MusicVolume = "MusicVolume";
        public const string NextAbility = "NextAbility";
        public const string NextColor = "NextColor";
        public const string NextTeam = "NextTeam";
        // "No Mic Composit" is invalid.
        // "No Mic Cross" is invalid.
        public const string NotReady = "NotReady";
        public const string OverlayCooldown = "OverlayCooldown";
        public const string ParalaxBackground = "ParalaxBackground";
        // "ParalaxBackground (1)" is invalid.
        public const string ParalaxPlatformBackground = "ParalaxPlatformBackground";
        public const string Pause = "Pause";
        public const string Pixel = "Pixel";
        public const string PixelManager = "PixelManager";
        public const string Platform = "Platform";
        // "Platform (10)" is invalid.
        // "Platform (11)" is invalid.
        // "Platform (16)" is invalid.
        // "Platform (17)" is invalid.
        // "Platform (18)" is invalid.
        // "Platform (19)" is invalid.
        // "Platform (2)" is invalid.
        // "Platform (20)" is invalid.
        // "Platform (3)" is invalid.
        // "Platform (4)" is invalid.
        // "Platform (5)" is invalid.
        // "Platform (6)" is invalid.
        // "Platform (7)" is invalid.
        // "Platform (8)" is invalid.
        // "Platform (9)" is invalid.
        public const string Platforms = "Platforms";
        public const string Play = "Play";
        public const string PlayButton = "PlayButton";
        public const string PlayButtonVideo = "PlayButtonVideo";
        public const string PlayParent = "PlayParent";
        public const string Player = "Player";
        public const string PlayerCharacter = "PlayerCharacter";
        public const string PlayerImage = "PlayerImage";
        public const string PlayerTrail = "PlayerTrail";
        public const string PlayerUI = "PlayerUI";
        // "Post-process Volume" is invalid.
        public const string PressToJoin = "PressToJoin";
        // "PressToJoin (1)" is invalid.
        // "PressToJoin (2)" is invalid.
        // "PressToJoin (3)" is invalid.
        // "PressToJoin (4)" is invalid.
        // "PressToJoin (5)" is invalid.
        // "PressToJoin (6)" is invalid.
        // "PressToJoin (7)" is invalid.
        public const string PreviousAbility = "PreviousAbility";
        public const string PreviousColor = "PreviousColor";
        public const string PreviousTeam = "PreviousTeam";
        public const string Quit = "Quit";
        public const string QuitButton = "QuitButton";
        public const string RB = "RB";
        public const string RT = "RT";
        public const string Ready = "Ready";
        public const string ReadyForeground = "ReadyForeground";
        public const string RightJoystick = "RightJoystick";
        public const string Rocket = "Rocket";
        public const string RocketLauncher = "RocketLauncher";
        public const string RoundStatsBanner = "RoundStatsBanner";
        public const string SFXVolume = "SFXVolume";
        public const string Settings = "Settings";
        public const string SettingsButton = "SettingsButton";
        public const string SettingsPanel = "SettingsPanel";
        public const string Shield = "Shield";
        public const string ShieldDome = "ShieldDome";
        public const string Shotgun = "Shotgun";
        public const string ShotgunBullet = "ShotgunBullet";
        // "Simple Slide Toggle" is invalid.
        public const string Sniper = "Sniper";
        public const string SniperBullet = "SniperBullet";
        public const string Sound = "Sound";
        public const string Spawn = "Spawn";
        // "Spawn (1)" is invalid.
        // "Spawn (2)" is invalid.
        // "Spawn (3)" is invalid.
        // "Spawn (4)" is invalid.
        // "Spawn (5)" is invalid.
        // "Spawn (6)" is invalid.
        // "Spawn (7)" is invalid.
        // "Spawn (8)" is invalid.
        public const string SpawnPointManager = "SpawnPointManager";
        public const string Sprite = "Sprite";
        public const string SpriteUI = "SpriteUI";
        public const string Square = "Square";
        // "Square (1)" is invalid.
        public const string Start = "Start";
        public const string StatsButton = "StatsButton";
        public const string Team = "Team";
        public const string TeamIcon = "TeamIcon";
        public const string TeamLeft = "TeamLeft";
        public const string TeamRight = "TeamRight";
        public const string TeamSelection = "TeamSelection";
        public const string Text = "Text";
        // "Text (Legacy)" is invalid.
        // "Text (TMP)" is invalid.
        public const string Thumb = "Thumb";
        public const string Title = "Title";
        public const string TrainingButton = "TrainingButton";
        // "Video Player" is invalid.
        public const string Weapons = "Weapons";
        public const string WinnerBanner = "WinnerBanner";
        public const string WinnerText = "WinnerText";
        public const string YButton = "YButton";
        // "switch" is invalid.
        public const string TempCollider = "TempCollider";
        public const string Banner = "Banner";
        public const string Lock = "Lock";
        public const string WinCrown = "WinCrown";
        public const string Logo = "Logo";
        public const string KillLines = "KillLines";
        public const string DownLine = "DownLine";
        public const string DefaultSkins = "DefaultSkins";
        public const string Levels = "Levels";
        public const string UnlockableSkins = "UnlockableSkins";
    }
}