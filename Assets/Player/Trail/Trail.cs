using UnityEngine;
using UnityEngine.Rendering;

public class Trail : MonoBehaviour
{
    [SerializeField] GameObject playerTrailPrefab;
    [SerializeField] float spawnRate = 10f;
    private SpriteRenderer spriteRenderer;
    private Player player;
    private float timeUntilNextSpawn = 0;

    private void Start()
    {
        spriteRenderer = GetComponentInChildren<SpriteRenderer>();
        player = GetComponent<Player>();
    }
    private void Update()
    {
        if (timeUntilNextSpawn <= 0)
        {
            GameObject trailPiece = PoolManager.Instance.Get(Harmony.PoolTags.trail);
            trailPiece.SetActive(true);
            SpriteRenderer trailRenderer = trailPiece.GetComponent<SpriteRenderer>();
            trailRenderer.color = player.GetTeam();
            trailRenderer.GetComponent<SortingGroup>().sortingLayerName = spriteRenderer.sortingLayerName;
            trailRenderer.sortingOrder = spriteRenderer.sortingOrder - 1;
            trailPiece.GetComponent<SpriteMask>().sprite = spriteRenderer.sprite;
            trailPiece.transform.position = transform.position;
            trailPiece.transform.rotation = transform.rotation;
            trailPiece.transform.localScale = spriteRenderer.transform.lossyScale;
            timeUntilNextSpawn = 1f / spawnRate;
        }
        timeUntilNextSpawn -= Time.deltaTime;
    }
}
