using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAbilityManager : MonoBehaviour
{
    private Ability[] abilities;
    private bool canUseAbility = true;

    private void Awake()
    {
        this.abilities = GetComponents<Ability>();
    }
    public bool CanUseAbility()
    {
        return canUseAbility;
    }

    public void SetCanUseAbility(bool state)
    {
        this.canUseAbility = state;
    }

    public void ResetAbilityCooldown()
    {
        GetActivatedAbility().ResetCooldown();
    }

    public void DisableAllAbilities() {
        for (int i = 0; i < abilities.Length; i++) {
            abilities[i].enabled = false;
        }
    }

    public void ActivateAbilityByIndex(int index) {
        DisableAllAbilities();
        if (index < abilities.Length && index > -1)
        {
            abilities[index].enabled = true;
        }
    }

    public void ActivateAbilityByType(Type abilityType) {
        for (int i = 0; i < abilities.Length; i++) {
            if (abilities[i].GetType() == abilityType) {
                DisableAllAbilities();
                abilities[i].enabled = true;
                break;
            }
        }
    }

    public void ResetAbilities() {
        for (int i = 0; i < abilities.Length; i++)
        {
            abilities[i].ResetAbility();
        }
    }

    public Ability[] GetAbilities() { 
        return abilities;
    }

    public Ability GetActivatedAbility()
    {
        for (int i = 0; i < abilities.Length; i++)
        {
            if (abilities[i].enabled)
            {
                return abilities[i];
            }
        }
        return null;
    }
}
