using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PlayerDamageManager : MonoBehaviour
{
    private bool hurtCoroutineRunning = false;
    private Player player;
    private Player lastHitBy;
    private PlayerUI playerUI;
    private SpriteRenderer spriteRenderer;
    private Vector2 trainingModeSpawn;
    private List<Killline> killlines;

    [Header("Sounds")]
    [SerializeField] private List<AudioClip> deathSounds;
    [SerializeField] private float deathSoundsVolume = 1f;

    [Header("Animators")]
    [SerializeField] Animator hitmark;

    [Header("Materials")]
    [SerializeField] private Material invertedColorsMaterial;
    [SerializeField] private Material normalColorsMaterial;

    [Header("Vibrations")]
    [SerializeField] private float deathLowVibration = .8f;
    [SerializeField] private float deathHighVibration = .5f;
    [SerializeField] private float deathVibrationDuration = .5f;

    private void Start()
    {
        spriteRenderer = GetComponentInChildren<SpriteRenderer>();
        player = GetComponent<Player>();
    }

    private void Update()
    {
        CheckKillLines();
    }

    public bool GetIsHurtCoroutineRunning()
    {
        return hurtCoroutineRunning;
    }
    public void SetTrainingSpawn(Vector2 spawn)
    {
        trainingModeSpawn = spawn;
    }
    public void SetPlayerUI(PlayerUI ui)
    {
        playerUI = ui;
    }
    public void ShowInGameHUD()
    {
        if (playerUI) playerUI.Show();
    }

    public void HideInGameHUD()
    {
        if (playerUI) playerUI.Hide();
    }
    public void Heal(float amount)
    {
        player.Heal(amount);
    }

    public void SetLastHitBy(Player player)
    {
        this.lastHitBy = player;
    }

    public void Hurt(float amount)
    {
        player.IncrementDamage(amount);

        if (playerUI) playerUI.PlayHurtAnimation();
        StartCoroutine(HurtAnimation());

        player.DecreaseFreezeTimeByDamage(amount);
    }
    private IEnumerator HurtAnimation()
    {
        hurtCoroutineRunning = true;
        Color skinColor = spriteRenderer.sprite.texture.GetPixel(1, spriteRenderer.sprite.texture.height / 2);
        hitmark.gameObject.SetActive(true);
        hitmark.transform.position = spriteRenderer.bounds.center;
        hitmark.GetComponent<SpriteRenderer>().color = new(1 - skinColor.r, 1 - skinColor.g, 1 - skinColor.b, 1);
        hitmark.transform.eulerAngles = new(0, 0, Random.value * 360f);
        hitmark.Play("hitmark");
        spriteRenderer.material = invertedColorsMaterial;
        spriteRenderer.transform.localEulerAngles = new(0, 0, 60 * (Random.value - 0.5f));
        yield return new WaitForSeconds(0.05f);
        hurtCoroutineRunning = true;
        spriteRenderer.material = invertedColorsMaterial;
        spriteRenderer.transform.localEulerAngles = new(0, 0, 60 * (Random.value - 0.5f));
        yield return new WaitForSeconds(0.15f);
        ResetHurtAnimation();
    }

    private void ResetHurtAnimation()
    {
        hitmark.gameObject.SetActive(false);
        spriteRenderer.transform.localEulerAngles = new(0, 0, 0);
        spriteRenderer.material = normalColorsMaterial;
        hurtCoroutineRunning = false;
    }

    public void Die()
    {
        StopAllCoroutines();

        Rigidbody2D rb = GetComponent<Rigidbody2D>();

        if (MultiplayerManager.Instance) Statistics.IncrementStat(Harmony.Stats.eliminations, 1);

        if (lastHitBy && lastHitBy.GetTeam() == player.GetTeam() && lastHitBy.GetSkin() != player.GetSkin())
        {
            //Achievement betrayal
            Achievements.Unlock(Harmony.Achievements.betrayal);
        }

        DeathParticles deathParticles = PoolManager.Instance.Get(Harmony.PoolTags.death_particles).GetComponent<DeathParticles>();
        deathParticles.gameObject.SetActive(true);
        deathParticles.transform.position = transform.position;
        deathParticles.Play(player.GetTeam(), Mathf.Atan2(rb.velocity.y, rb.velocity.x) * Mathf.Rad2Deg + 90);

        if (MultiplayerManager.Instance != null)
        {
            if (playerUI)
            {
                playerUI.Hide();
                playerUI.Reset();
            }

            ResetHurtAnimation();
            player.SetIsDead(true);
            player.IncrementDamage(1);
            player.UnequipAllWeapons();
            MultiplayerManager.Instance.CheckForWinner();
        }
        else
        {
            player.SetDamage(1);
            rb.velocity = Vector2.zero;
            transform.position = trainingModeSpawn;
            TrainingModeManager.Instance.ReloadLevel(player);
        }
        SoundManager.Instance.PlayRandomSoundEffect(deathSounds, deathSoundsVolume);
        player.TryVibrateController(deathLowVibration, deathHighVibration, deathVibrationDuration);
    }

    private void CheckKillLines()
    {
        if (!player.GetIsDead())
        {
            if (killlines == null || killlines.Count == 0)
            {
                killlines = FindObjectsByType<Killline>(FindObjectsSortMode.None).ToList();
            }
            else
            {
                bool anyNull = killlines.Any(item => item == null);
                if (anyNull)
                    killlines = FindObjectsByType<Killline>(FindObjectsSortMode.None).ToList();
            }


            if (killlines != null)
            {
                foreach (Killline line in killlines)
                {
                    if (line && line.ShouldDelete(this.transform.position))
                    {
                        Die();
                    }
                }
            }
        }
    }

    public Ability GetActivatedAbility()
    {
        throw new System.NotImplementedException();
    }
}
