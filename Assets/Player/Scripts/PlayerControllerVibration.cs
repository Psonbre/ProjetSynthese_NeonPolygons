using System.Collections;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerControllerVibration : MonoBehaviour
{
    private PlayerInput controls;
    private Gamepad playerGamepad;
    private void Start()
    {
        controls = GetComponentInParent<PlayerInput>();
        UpdatePlayerGamepad();
    }

    private void UpdatePlayerGamepad()
    {
        playerGamepad = null;
        foreach (var device in controls.devices)
        {
            if (device is Gamepad)
            {
                playerGamepad = device as Gamepad;
                break;
            }
        }
    }

    public void TryVibrateController(float lowFrequency, float highFrequency, float duration)
    {
        if (controls.currentControlScheme == PlayerControls.GAMEPAD_CONTROL_SCHEME && playerGamepad != null)
        {
            StartCoroutine(VibrateGamepad(lowFrequency, highFrequency, duration));
        }
    }

    public IEnumerator VibrateGamepad(float lowFrequency, float highFrequency, float duration)
    {
        playerGamepad.SetMotorSpeeds(Mathf.Min(lowFrequency, 1), Mathf.Min(highFrequency, 1));
        yield return new WaitForSeconds(Mathf.Min(duration, 1));
        playerGamepad.SetMotorSpeeds(0, 0);
    }

    // Fonction pour g�rer le cas o� la coroutine "VibrateGamepad" est cancel�
    // avant d'�tre termin�, ce qui n'arr�terait pas la manette de vibrer
    public void TryResetVibration()
    {
        if (controls.currentControlScheme == PlayerControls.GAMEPAD_CONTROL_SCHEME && playerGamepad != null)
        {
            playerGamepad.SetMotorSpeeds(0, 0);
        }
    }
}
