using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerControls : MonoBehaviour
{
	public const string GAMEPAD_CONTROL_SCHEME = "Gamepad";
	public const string KEYBOARD_CONTROL_SCHEME = "Keyboard&Mouse";

	[Header("Links")]
	[SerializeField] private SpriteRenderer crown;
	[SerializeField] private SpriteRenderer arrow;
    private Camera cam;
    private SpriteRenderer spriteRenderer;
    private Collider2D playerCollider;
    public Rigidbody2D rb;
    private Player player;
    private MultiplayerManager multiplayerManager;

    [Header("Materials")]
    [SerializeField] private Material reversedColorsMaterial;
    [SerializeField] private Material defaultMaterial;

    [Header("Multipliers")]
    [SerializeField] private float squashAndStretchIntensity = 1;
    [SerializeField] private float freezeInputGroundDrag = .98f;
    [SerializeField] private float powerToFreezeDivisor = 20000f;
    [SerializeField] private float tazerHurtCooldownReductionMult = 1f;
    [SerializeField] private float cancelMovementSpeedMult = 1.2f;
    private float freezeTime = 0f;
    private float freezeInputsTime = 0f;

    [Header("Movement")]
	[SerializeField] private float speed = 5f;
    [SerializeField] private float groundCheckDistance = 1f;
    [SerializeField] private float jumpForce = 5;
    [SerializeField] private int maxJumps = 2;
    [SerializeField] private float bounceFriction = .98f;
	[SerializeField] private float tBagHeightMult = .5f;
    private bool isGrounded = false;
    private int jumpsLeft = 2;
    private Vector2 previousVelocity;
    private float defaultGravity = 1;
    private bool isTbaging = false;
    private bool wasTbaging = false;
    private bool isLanding = false;
    private bool preventAllInputs = false;

    [Header("Sounds")]
    [SerializeField] private List<AudioClip> jumpSounds;
    [SerializeField] private float jumpSoundsVolume = 1f;
    [SerializeField] private List<AudioClip> landSounds;
    [SerializeField] private float landSoundsVolume = 1f;

	[Header("Vibrations")]
    [SerializeField] private float vibrateForceMult = .1f;
    [SerializeField] private float vibrateDurationMult = .1f;

	[Header ("Others")]
    [SerializeField] private float camSizeToShowArrow = 90;
    [SerializeField] private float knockBackAngleUp = 20f;

    // Inputs
    private bool isFiring = false;
    private PlayerInput controls;
    private float horizontalInput;
    private Vector2 lookInput;
    private Vector2 lookLeftStickInput;
    private Vector2 lookRightStickInput;
    private Vector2 mousePosition;


    private void Awake()
    {
        spriteRenderer = GetComponentInChildren<SpriteRenderer>();
        player = GetComponent<Player>();
    }
    private void Start()
    {
        controls = GetComponentInParent<PlayerInput>();

        multiplayerManager = FindFirstObjectByType<MultiplayerManager>();

        controls.actions[Harmony.InputActions.Move].performed += ctx => horizontalInput = ctx.ReadValue<Vector2>()[0];
        controls.actions[Harmony.InputActions.Move].canceled += ctx => horizontalInput = 0;

        controls.actions[Harmony.InputActions.LookLeftStick].performed += ctx => lookLeftStickInput = ctx.ReadValue<Vector2>();

        controls.actions[Harmony.InputActions.LookRightStick].performed += ctx => lookRightStickInput = ctx.ReadValue<Vector2>();
        controls.actions[Harmony.InputActions.LookRightStick].canceled += ctx => lookRightStickInput = Vector2.zero;

        controls.actions[Harmony.InputActions.LookMouse].performed += ctx => mousePosition = ctx.ReadValue<Vector2>();

        controls.actions[Harmony.InputActions.Fire].performed += ctx => isFiring = true;
        controls.actions[Harmony.InputActions.Fire].canceled += ctx => isFiring = false;

        controls.actions[Harmony.InputActions.Tbag].performed += ctx => isTbaging = true;
        controls.actions[Harmony.InputActions.Tbag].canceled += ctx => isTbaging = false;

        controls.actions[Harmony.InputActions.Pause].performed += ctx => Pause();

        rb = GetComponent<Rigidbody2D>();
        playerCollider = GetComponent<Collider2D>();
        defaultGravity = rb.gravityScale;
        jumpsLeft = maxJumps;
    }
    void Update()
    {
        if (!cam)
        {
            cam = Camera.main;
        }
        if (arrow != null)
        {
            arrow.color = new(player.GetTeam().r, player.GetTeam().g, player.GetTeam().b, (cam.orthographicSize - camSizeToShowArrow) / 60f);
        }

        if (freezeInputsTime > 0f)
            freezeInputsTime -= Time.deltaTime;

        if (freezeTime > 0f)
            freezeTime -= Time.deltaTime;

        previousVelocity = this.rb.velocity;

        if (controls.currentControlScheme == GAMEPAD_CONTROL_SCHEME) lookInput = lookRightStickInput != Vector2.zero ? lookRightStickInput : lookLeftStickInput;
        if (controls.currentControlScheme == KEYBOARD_CONTROL_SCHEME && cam) lookInput = (cam.ScreenToWorldPoint(mousePosition) - spriteRenderer.bounds.center).normalized;

        if (freezeTime <= 0f && !player.GetIsDead())
        {
            if (rb.gravityScale == 0)
                rb.gravityScale = defaultGravity;

            CheckGrounded();

            if (!isGrounded) spriteRenderer.transform.localScale = new(Mathf.Max(1f - rb.velocityY / (1000f / squashAndStretchIntensity), 0.5f), Mathf.Max(1f + rb.velocityY / (1000f / squashAndStretchIntensity), 0.5f));

            if (freezeInputsTime <= 0f && !preventAllInputs)
            {
                if (Mathf.Abs(rb.velocityX) <= speed * cancelMovementSpeedMult)
                {
                    rb.drag = 0;
                    float move = horizontalInput;
                    Vector2 moveDirection = new Vector2(move * speed, rb.velocity.y);
                    rb.velocity = moveDirection;
                    if (spriteRenderer.material != defaultMaterial && !player.GetIsHurtCoroutineRunning()) spriteRenderer.material = defaultMaterial;
                }
                else
                {
                    if (spriteRenderer.material != reversedColorsMaterial) spriteRenderer.material = reversedColorsMaterial;
                    rb.drag = 1;
                }

                if (isGrounded && !isLanding)
                {
                    if (isTbaging)
                    {
                        spriteRenderer.transform.localScale = new(1f, tBagHeightMult);
                        wasTbaging = true;
                    }
                    else if (wasTbaging)
                    {
                        wasTbaging = false;
                        if (multiplayerManager)
                        {
                            Statistics.IncrementStat(Harmony.Stats.tbag, 1);
                        }
                        spriteRenderer.transform.localScale = Vector2.one;
                    }
                }

                if (controls.actions[Harmony.InputActions.Jump].triggered)
                    TryJump();
            }
            else
            {
                if (isGrounded)
                    rb.velocityX *= freezeInputGroundDrag;
            }
        }
        else

        {
            rb.velocity = Vector2.zero;
            rb.gravityScale = 0;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag(Harmony.Tags.Ground) && Mathf.Abs(previousVelocity.x) > speed * cancelMovementSpeedMult)
        {
            Vector2 reflectionAxis = collision.contacts[0].normal;
            Vector2 result = Vector2.Reflect(previousVelocity, reflectionAxis);
            this.rb.velocity = result * bounceFriction;
        }
    }

    public void SetDefaultGravity(float gravity)
    {
        defaultGravity = gravity;
    }

    public float GetKnockbackMult() {
		return player.GetKnockbackMult();
	}

	public void SetCrown(bool crownOn)
	{
		crown.gameObject.SetActive(crownOn);
		crown.transform.parent = transform.parent.parent;
        crown.transform.position = transform.position;
    }

	public void SetPreventAllInputs(bool state) 
	{
		preventAllInputs = state;
    }

    public bool GetAllInputsFrozen()
    {
		return preventAllInputs;
    }

	public bool IsPlayerFrozen() {
		return freezeTime > 0f;
    }

	public bool GetPlayerInputsFrozen()
	{
		return freezeInputsTime > 0f;
	}

	public void Unfreeze() {
		this.freezeTime = 0;
	}

	public void UnfreezeInputs() {
		this.freezeInputsTime = 0;
	}

	public void FreezeFor(float seconds)
	{
		this.freezeTime = Mathf.Max(freezeTime, seconds);
	}

    public void TazeFor(float seconds)
    {
		FreezeInputsFor(seconds);
    }

	public void FreezeInputsFor(float seconds) {
        this.freezeInputsTime = Mathf.Max(freezeInputsTime, seconds);
    }

    public void ApplyExplosion(Vector2 explosionPoint, float power, Vector2 extraForce) {
		Vector2 direction = (new Vector2(transform.position.x, transform.position.y) - explosionPoint).normalized;
		Vector2 force = direction * power + extraForce;

        float angle = Mathf.Atan2(force.y, force.x);
		angle = Mathf.MoveTowardsAngle(angle, 90f * Mathf.Deg2Rad, knockBackAngleUp * Mathf.Deg2Rad);
		direction = new(Mathf.Cos(angle), Mathf.Sin(angle));
		force = direction * force.magnitude;

		ApplyKnockback(force);
    }

    public void ApplyKnockback(Vector2 force)
    {
        if (!player.GetIsDead()) {
            player.TryVibrateController(force.magnitude * vibrateForceMult, force.magnitude * vibrateForceMult, force.magnitude * vibrateDurationMult);
            rb.AddForce(force, ForceMode2D.Impulse);
        }
    }
    public void RemoveJumps()
    {
        jumpsLeft = 0;
    }

    public void TryJump()
    {
        if (jumpsLeft > 0)
        {
            Jump();
            jumpsLeft--;
        }
    }

    public void Jump()
    {
        SoundManager.Instance.PlayRandomSoundEffect(jumpSounds, jumpSoundsVolume);
        if (multiplayerManager) Statistics.IncrementStat(Harmony.Stats.jumps, 1);
        GameObject jumpParticles = PoolManager.Instance.Get(Harmony.PoolTags.double_jump_particles);
        jumpParticles.transform.position = new(transform.position.x, transform.position.y);
        jumpParticles.GetComponent<ParticleSystem>().startColor = player.GetTeam();
        jumpParticles.SetActive(true);

        rb.velocityY = jumpForce;
    }

    public bool IsGrounded()
    {
        Vector2 boxCastPosition = new Vector2(transform.position.x, transform.position.y - 2.1f);
        Vector2 boxCastSize = new Vector2(playerCollider.bounds.size.x, 4f);
        float boxCastDistance = groundCheckDistance;
        RaycastHit2D[] hits = Physics2D.BoxCastAll(boxCastPosition, boxCastSize, 0, Vector2.down, boxCastDistance);
        foreach (RaycastHit2D hit in hits)
            if (hit.collider != null && (hit.collider.CompareTag(Harmony.Tags.Ground) || hit.collider.CompareTag(Harmony.Tags.Player) || hit.collider.CompareTag(Harmony.Tags.Mine)))
                return true;
        return false;
    }

    public GameObject GetSelectedPlatform()
    {
        Vector2 boxCastPosition = new Vector2(transform.position.x, transform.position.y - 2.1f);
        Vector2 boxCastSize = new Vector2(playerCollider.bounds.size.x, 4f);
        float boxCastDistance = groundCheckDistance;
        RaycastHit2D[] hits = Physics2D.BoxCastAll(boxCastPosition, boxCastSize, 0, Vector2.down, boxCastDistance);
        foreach (RaycastHit2D hit in hits)
            if (hit.collider != null && hit.collider.CompareTag(Harmony.Tags.Ground))
                return hit.collider.gameObject;
        return null;
    }
    public void DecreaseFreezeTimeByDamage(float damage)
    {
        if (this.freezeInputsTime > 0)
        {
            this.freezeInputsTime -= damage * tazerHurtCooldownReductionMult;
        }
    }

    public Vector2 GetLookInput()
    {
        return this.lookInput;
    }

    public bool GetIsFiring()
    {
        return (isFiring && !preventAllInputs);
    }

    private void CheckGrounded()
	{
        if (isGrounded)
        {
			isGrounded = IsGrounded();
			if (jumpsLeft <= 0) 
				jumpsLeft = maxJumps;
        }
		else
		{
			isGrounded = IsGrounded();
			if (isGrounded)
				StartCoroutine(Land(rb.velocityY));
		}
    }

    private IEnumerator Land(float velocity)
    {
		isLanding = true;
		GameObject landParticles = PoolManager.Instance.Get(Harmony.PoolTags.land_particles);
		SoundManager.Instance.PlayRandomSoundEffect(landSounds, landSoundsVolume);
		landParticles.transform.position = new(transform.position.x, spriteRenderer.bounds.min.y);
		landParticles.GetComponent<ParticleSystem>().startColor = player.GetTeam();
		landParticles.SetActive(true);
		jumpsLeft = maxJumps;
		spriteRenderer.transform.localScale = new Vector2(
            Mathf.Max(1f - velocity / (500f / squashAndStretchIntensity), 0.3f),
            Mathf.Max(1f + velocity / (500f / squashAndStretchIntensity), 0.3f)
        );

        float duration = 0.1f;
        float elapsed = 0f;

        Vector2 startScale = spriteRenderer.transform.localScale;
        Vector2 targetScale = new Vector2(1, 1);

        while (elapsed < duration)
        {
            elapsed += Time.deltaTime;
            float lerpFactor = elapsed / duration;
            spriteRenderer.transform.localScale = Vector2.Lerp(startScale, targetScale, lerpFactor);
            yield return null;
        }
		isLanding = false;
        spriteRenderer.transform.localScale = targetScale;
    }

	private void Pause()
	{
		if (this != null && gameObject.activeSelf && !player.GetIsDead())
		{
            PauseManager pauseManager = FindAnyObjectByType<PauseManager>();
            if (pauseManager)
            {
				PlayerInput playerInput = transform.parent.GetComponent<PlayerInput>();
				if (playerInput) pauseManager.TogglePause(playerInput);
            }
        }
    }
}
