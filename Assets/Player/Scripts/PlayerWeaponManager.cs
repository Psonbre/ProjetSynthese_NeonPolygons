using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PlayerWeaponManager : MonoBehaviour
{
    [SerializeField] private float equipRandomWeaponDelay = 1f;

    private Holdable equippedWeapon;
    private List<Holdable> weapons;

    private void Awake()
    {
        weapons = transform.Find("Weapons").GetComponentsInChildren<Holdable>().ToList();
        weapons.ForEach(w => w.gameObject.SetActive(false));
    }
    public void EquipRandomWeapon()
    {
        StartCoroutine(EquipRandomWeaponCoroutine());

    }
    public void EquipWeaponByType(System.Type type)
    {
        UnequipAllWeapons();
        if (weapons != null && weapons.Count > 0)
        {
            GameObject weapon = weapons[0].transform.parent.GetComponentInChildren(type, true).gameObject;
            if (weapon)
            {
                weapon.gameObject.SetActive(true);
                equippedWeapon = weapon.gameObject.GetComponent<Holdable>();
            }
        }
    }

    public void UnequipAllWeapons()
    {
        equippedWeapon = null;
        weapons.ForEach(weapon => weapon.gameObject.SetActive(false));
    }

    private IEnumerator EquipRandomWeaponCoroutine()
    {
        UnequipAllWeapons();

        yield return new WaitForSeconds(equipRandomWeaponDelay);

        equippedWeapon = weapons[Random.Range(0, weapons.Count)].GetComponent<Holdable>();
        equippedWeapon.gameObject.SetActive(true);
    }


}
