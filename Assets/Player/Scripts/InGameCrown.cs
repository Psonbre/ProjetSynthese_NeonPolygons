using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InGameCrown : MonoBehaviour
{
    private Player player;
    [SerializeField] float headOffset = 5.64f;
    [SerializeField] float gravity = 100f;
    [SerializeField] float rotationSpeedDivider = 100f;
    SpriteRenderer playerSpriteRenderer;
    private float speed = 0;
    private float currentRotationProgress = 0;
    private void Awake()
    {
        player = GetComponentInParent<Player>();
        gameObject.SetActive(false);
        playerSpriteRenderer = player.GetComponentInChildren<SpriteRenderer>();
    }

    private void Update()
    {
        float playerHead = playerSpriteRenderer.bounds.center.y + headOffset;

        if (transform.position.y < playerHead)
        {
            speed = 0;
        }
        else if (transform.position.y > playerHead)
        {
            speed -= gravity * Time.deltaTime;
            transform.position = new(player.transform.position.x, transform.position.y + speed * Time.deltaTime);
            currentRotationProgress += speed * Time.deltaTime / rotationSpeedDivider;
            transform.eulerAngles = new(0, 0, Mathf.Sin(currentRotationProgress) * 10f);
        }

        transform.position = new(player.transform.position.x, Mathf.Max(playerHead, transform.position.y));
    }
}
