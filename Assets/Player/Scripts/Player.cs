using System;
using UnityEngine;

public class Player : MonoBehaviour
{
    //La classe player g�re les liens entre tous les scripts de player.

    // Toutes les autres classes de joueur (ex : PlayerDamageManager, PlayerWeaponManager, etc)
    // ne devraient �tre utilis�es qu'� l'int�rieur de cette classe, et la classe Player devrait �tre la seule classe utilis� par le reste du code

    private SpriteRenderer spriteRenderer;
    private PlayerAbilityManager abilityManager;
    private PlayerWeaponManager weaponManager;
    private PlayerControllerVibration vibrationManager;
    private PlayerControls controls;
    private PlayerDamageManager damageManager;
    private Color team = new Color(0, 0, 0);
    private int points = 0;
    private float damage = 1;
    private int nbOfDeaths = 0;
    private bool isDead = false;

    private void Awake()
    {
        weaponManager = GetComponent<PlayerWeaponManager>();
        vibrationManager = GetComponent<PlayerControllerVibration>();
        spriteRenderer = GetComponentInChildren<SpriteRenderer>();
        abilityManager = GetComponent<PlayerAbilityManager>();
        controls = GetComponent<PlayerControls>();
        damageManager = GetComponent<PlayerDamageManager>();
    }
    
    public Sprite GetSkin() { return spriteRenderer.sprite; }
    public void SetSkin(Sprite skin) { spriteRenderer.sprite = skin; }
    public float GetKnockbackMult() { return damage; }
    public float GetDamage() { return damage; }
    public void SetDamage(float damage) { this.damage = damage; }
    public void IncrementDamage(float amount) { damage += amount; }
    public void Heal(float amount) { damage = Mathf.Max(damage - amount, 0); }
    public Color GetTeam() { return team; }
    public void SetTeam(Color team) { this.team = team; }
    public int GetPoints() { return points; }
    public void SetPoints(int points) {  this.points = points; }
    public void IncrementPoints(int amount) {  points += amount; }
    public int GetDeaths() { return nbOfDeaths; }
    public void SetDeaths(int deaths) { nbOfDeaths = deaths;}
    public void IncrementDeaths(int amount) { nbOfDeaths += amount; }
    public bool GetIsDead() { return isDead; }
    public void SetIsDead(bool isDead) { this.isDead = isDead; }
    public Ability GetActivatedAbility() { return abilityManager.GetActivatedAbility(); }
    public void ApplyKnockback(Vector2 force) { controls.ApplyKnockback(force); }
    public bool GetIsFiring() { return controls.GetIsFiring(); }
    public void EquipWeaponByType(Type weaponType) { weaponManager.EquipWeaponByType(weaponType); }
    public void Hurt(float damage) { damageManager.Hurt(damage); }
    public void SetLastHitBy(Player player) { damageManager.SetLastHitBy(player); }
    public void ActivateAbilityByType(Type type) { abilityManager.ActivateAbilityByType(type); }
    public void UnequipAllWeapons() { weaponManager.UnequipAllWeapons(); }
    public void ResetAbilities() { abilityManager.ResetAbilities(); }
    public void SetCanUseAbility(bool canUseAbility) { abilityManager.SetCanUseAbility(canUseAbility); }
    public void Unfreeze() { controls.Unfreeze(); }
    public void UnfreezeInputs() { controls.UnfreezeInputs(); }
    public void ShowInGameHUD() { damageManager.ShowInGameHUD(); }
    public void DecreaseFreezeTimeByDamage(float amount) { controls.DecreaseFreezeTimeByDamage(amount); }
    public bool CanUseAbility() { return abilityManager.CanUseAbility(); }
    public bool IsPlayerFrozen() { return controls.IsPlayerFrozen(); }
    public bool GetPlayerInputsFrozen() { return controls.GetPlayerInputsFrozen(); }
    public bool GetAllInputsFrozen() { return controls.GetAllInputsFrozen(); }
    public GameObject GetSelectedPlatform() { return controls.GetSelectedPlatform(); }
    public void RemoveJumps() { controls.RemoveJumps(); }
    public void Jump() { controls.Jump(); }
    public bool IsGrounded() { return controls.IsGrounded(); }
    public void FreezeFor(float duration) { controls.FreezeFor(duration); }
    public void SetDefaultGravity(int gravity) { controls.SetDefaultGravity(gravity); }
    public void EquipRandomWeapon() { weaponManager.EquipRandomWeapon(); }
    public void Die() { damageManager.Die(); }
    public void TazeFor(float tazeTime) { controls.TazeFor(tazeTime); }
    public void TryResetVibration() { vibrationManager.TryResetVibration(); }
    public void SetPreventAllInputs(bool preventAllInputs) {controls.SetPreventAllInputs(preventAllInputs);}
    public void SetCrown(bool crown) { controls.SetCrown(crown); }
    public void ApplyExplosion(Vector3 position, float power, Vector2 extraForce) { controls.ApplyExplosion(position, power, extraForce); }
    public Vector2 GetLookInput() { return controls.GetLookInput(); }
    public void TryVibrateController(float deathLowVibration, float deathHighVibration, float deathVibrationDuration) { vibrationManager.TryVibrateController(deathLowVibration, deathHighVibration, deathVibrationDuration); }
    public void FreezeInputsFor(float freezeForSeconds) { controls.FreezeInputsFor(freezeForSeconds); }
    public void HideInGameHUD() { damageManager.HideInGameHUD(); }
    public void SetTrainingSpawn(Vector3 position) { damageManager.SetTrainingSpawn(position); }
    public void SetPlayerUI(PlayerUI newPlayerUI) { damageManager.SetPlayerUI(newPlayerUI); }
    public bool GetIsHurtCoroutineRunning() { return damageManager.GetIsHurtCoroutineRunning(); }
}
