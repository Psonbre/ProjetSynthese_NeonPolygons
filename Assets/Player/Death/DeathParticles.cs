using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class DeathParticles : MonoBehaviour
{
    [SerializeField] private float duration = 0.4f;
    [SerializeField] private float cameraShakeStrength = 5f;
    [SerializeField] private float cameraShakeFrequency = 10f;
    [SerializeField] private float cameraShakeDuration = .5f;
    private ParticleSystem ps;
    private Camera cam;

    void Awake()
    {
        ps = GetComponent<ParticleSystem>();
    }

    public void Play(Color color, float angle)
    {
        StartCoroutine(ExplodeCoroutine(color, angle));
    }

    private IEnumerator ExplodeCoroutine(Color color, float angle)
    {
        StartCoroutine(ShakeCamera(cameraShakeStrength, cameraShakeFrequency, cameraShakeDuration));
        transform.eulerAngles = new(0, 0, angle);
        ps.startColor = color;
        ps.emissionRate = 200f;
        ps.Play();
        yield return new WaitForSeconds(duration);
        ps.emissionRate = 0;
        yield return new WaitForSeconds(0.5f);
        ps.Stop();
        PoolManager.Instance.ReturnToPool(Harmony.PoolTags.death_particles, this.gameObject);
    }

    private IEnumerator ShakeCamera(float strength, float frequency, float duration)
    {
        float timeUntilNextShake = 1f / frequency;
        cam = Camera.main;

        while (duration > 0)
        {
            duration -= Time.deltaTime;
            timeUntilNextShake -= Time.deltaTime;

            if (timeUntilNextShake < 0)
            {
                if (cam == null) cam = Camera.main;
                cam.transform.position = new(cam.transform.position.x + (Random.value - 0.5f) * strength, cam.transform.position.y + (Random.value - 0.5f) * strength, cam.transform.position.z);
                cam.transform.eulerAngles = new(0, 0, (Random.value - 0.5f) * strength);
                timeUntilNextShake = 1 / frequency;
            }
            yield return null;
        }
        cam.transform.eulerAngles = new(0, 0, 0);
    }

}
