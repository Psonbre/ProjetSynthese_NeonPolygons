using UnityEngine;

public class Killline : MonoBehaviour
{
    [SerializeField] private KillDirection direction;

    public bool ShouldDelete(Vector2 position)
    {
      
        switch (direction)
        {
            case KillDirection.UP:
                if (position.y > this.transform.position.y)
                    return true;
                break;
            case KillDirection.DOWN:
                if (position.y < this.transform.position.y)
                    return true;
                break;
            case KillDirection.RIGHT:
                if (position.x > this.transform.position.x)
                    return true;
                break;
            case KillDirection.LEFT:
                if (position.x < this.transform.position.x)
                    return true;
                break;
        }
        return false;
    }
}
public enum KillDirection
{
    UP,
    DOWN,
    LEFT,
    RIGHT
}