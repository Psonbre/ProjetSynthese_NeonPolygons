using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloudBackground : MonoBehaviour
{

    [SerializeField] private float verticalWaveAmplitude;
    [SerializeField] private float verticalWaveFrequency;
    [SerializeField] private float horizontalSpeed = 1f;
    [SerializeField] private float wrapBackDistance = 100f;
    private Vector2 originalPosition;
    private float time;

    void Start()
    {
        originalPosition = transform.position;
    }

    void Update()
    {
        time += Time.deltaTime;
        float yOffset = Mathf.Sin(time * verticalWaveFrequency) * verticalWaveAmplitude;
        transform.position = new Vector2((transform.position.x + horizontalSpeed * Time.deltaTime) % wrapBackDistance, originalPosition.y + yOffset);
    }


}
