using UnityEngine;
public class ParalaxBackground : MonoBehaviour
{
	[SerializeField] private float paralaxSpeed = 1f;
	private Material material;

	void Start()
	{
		material = GetComponent<Renderer>().material;
	}

	void Update()
	{
		material.mainTextureOffset = new Vector2(Time.time * paralaxSpeed, 0);
	}
}

