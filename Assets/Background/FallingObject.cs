using System.Collections;
using UnityEngine;


public class FallingObject : MonoBehaviour {
    [SerializeField] private float acceleration = 1f;
    [SerializeField] private float startSpeed = 10f;
    [SerializeField] private float killY = -500f;
    [SerializeField] private float spawnY = 500f;
    [SerializeField] private float maximumSpawnX;
    [SerializeField] private float minimumSpawnX;
    [SerializeField] private float minRandomSize;
    [SerializeField] private float maxRandomSize;
    [SerializeField] private float minimumRandomDelay = 1f;
    [SerializeField] private float maximumRandomDelay = 2f;
    private bool canMove = false;
    private float speed;

    void Start()
    {
        StartCoroutine(TryRespawn());
    }
    void Update()
    {
        if (canMove)
        {
            if (transform.position.y < killY)
            {
                StartCoroutine(TryRespawn());
            }
            else
            {
                speed += acceleration * Time.deltaTime;
                transform.position -= new Vector3(0, speed * Time.deltaTime, 0);
            }
        }
    }


    private IEnumerator TryRespawn()
    {
        canMove = false;
        yield return new WaitForSeconds(UnityEngine.Random.Range(minimumRandomDelay, maximumRandomDelay));
        canMove = true;
        Respawn();
    }
    private void Respawn()
    {
        transform.position = new Vector3(UnityEngine.Random.Range(minimumSpawnX, maximumSpawnX), spawnY, 0);
        transform.eulerAngles = new(0, 0, UnityEngine.Random.Range(0f, 360f));
        float randomScale = UnityEngine.Random.Range(minRandomSize, maxRandomSize);
        transform.localScale = new Vector3(randomScale, randomScale, 1);
        speed = startSpeed;
    }
}