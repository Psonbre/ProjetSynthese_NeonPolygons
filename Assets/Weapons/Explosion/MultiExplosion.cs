using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class MultiExplosion : Explosion
{

    protected override void Awake()
    {
        colliders2d = new List<Collider2D>();
        for (int i = 0; i < gameObject.transform.childCount; i++)
        {
            colliders2d.Add(gameObject.transform.GetChild(i).GetComponent<Collider2D>());
        }

        ps = GetComponent<ParticleSystem>();
    }

}
