using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion : MonoBehaviour
{
    protected List<Collider2D> colliders2d;
    private float explosionPower = 1000f;
    private float explosionDamage = 0.1f;
    private Color team = new Color(1f, 1f, 1f);
    private Vector2 extraForce;
    private Player owner;
    protected ParticleSystem ps;
    [SerializeField] private string poolTag;
    [SerializeField] private List<AudioClip> quietExplosions;
    [SerializeField] private List<AudioClip> normalExplosions;
    [SerializeField] private List<AudioClip> loudExplosions;
    [SerializeField] private float minimumNormalExplosionPower = 20f;
    [SerializeField] private float maximumNormalExplosionPower = 30f;
    [SerializeField] private float explosionSoundVolumeMult = 1f;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag(Harmony.Tags.Player))
        {
            Player player = collision.GetComponent<Player>();

            if (player)
            {
                if (player.GetTeam() != team) player.Hurt(explosionDamage);
                player.ApplyExplosion(this.transform.position, explosionPower * player.GetKnockbackMult(), extraForce);
                player.SetLastHitBy(owner);
            }
        }
    }

    public void SetExplosionSoundVolumeMult(float explosionSoundVolumeMult) {
        this.explosionSoundVolumeMult = explosionSoundVolumeMult;
    }

    public void SetExplosionPower(float explosionPower)
    {
        this.explosionPower = explosionPower;
    }

    public void SetExplosionDamage(float explosionDamage)
    {
        this.explosionDamage = explosionDamage;
    }

    public void SetTeam(Color team)
    {
        this.team = team;
        if (ps) ps.startColor = team;
    }

    public void SetOwner(Player owner)
    {
        this.owner = owner;
    }

    protected virtual void Awake()
    {
        colliders2d = new List<Collider2D>(gameObject.GetComponents<Collider2D>());
        ps = GetComponent<ParticleSystem>();
    }
    public void Explode(Vector2 size, Vector2 extraForce)
    {
        this.extraForce = extraForce;
        transform.localScale = size;
        StopAllCoroutines();
        StartCoroutine(ExplodeCoroutine());
    }

    public void Explode(float size, Vector2 extraForce)
    {
        Explode(size * Vector2.one, extraForce);
    }

    public IEnumerator ExplodeCoroutine()
    {
        for (int i = 0; i < colliders2d.Count; i++)
        {
            colliders2d[i].enabled = true;
        }

        if (explosionPower < minimumNormalExplosionPower)
            SoundManager.Instance.PlayRandomSoundEffect(quietExplosions, explosionSoundVolumeMult);
        else if (explosionPower > maximumNormalExplosionPower)
            SoundManager.Instance.PlayRandomSoundEffect(loudExplosions, explosionSoundVolumeMult);
        else
            SoundManager.Instance.PlayRandomSoundEffect(normalExplosions, explosionSoundVolumeMult);

        yield return new WaitForSeconds(0.1f);

        for (int i = 0; i < colliders2d.Count; i++)
            colliders2d[i].enabled = false;

        yield return new WaitForSeconds(0.3f);
        PoolManager.Instance.ReturnToPool(poolTag, gameObject);
    }
}
