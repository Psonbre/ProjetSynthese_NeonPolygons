using UnityEngine;

public class BounceGun : Gun
{
    [SerializeField] private float throwingStrength = 200;
    [SerializeField] private float fuseTime = 3f;
    [SerializeField] private float explosionRadius = 10f;
    [SerializeField] private float explosionPower = 1000;
    [SerializeField] private float explosionDamage = 0.1f;
    protected override void shoot()
    {
        GameObject bulletToShoot = PoolManager.Instance.Get(bulletType);

        Vector2 direction = new Vector2(Mathf.Cos(rotation * Mathf.Deg2Rad), Mathf.Sin(rotation * Mathf.Deg2Rad));
        RaycastHit2D hit = Physics2D.Raycast(transform.position - (Vector3)(direction * offsetFromCenter), direction, offsetFromCenter + bulletSpawnOffset, terrainMask);

        if (hit)
        {
            bulletToShoot.transform.position = hit.point;
        }
        else
        {
            bulletToShoot.transform.position = transform.position + new Vector3(
                bulletSpawnOffset * Mathf.Cos(rotation * Mathf.Deg2Rad),
                bulletSpawnOffset * Mathf.Sin(rotation * Mathf.Deg2Rad),
                0
            );
        }

        bulletToShoot.transform.rotation = Quaternion.Euler(0, 0, rotation * Mathf.Deg2Rad);
        Vector2 shootDirection = new Vector2(
            Mathf.Cos(rotation * Mathf.Deg2Rad),
            Mathf.Sin(rotation * Mathf.Deg2Rad)
        );
        BounceBullet bounceBullet = bulletToShoot.GetComponent<BounceBullet>();
        SoundManager.Instance.PlayRandomSoundEffect(shootSounds, volumeMultiplicator);
        bounceBullet.SetFuse(fuseTime);
        bounceBullet.SetExplosionRadius(explosionRadius);
        bounceBullet.SetExplosionPower(explosionPower);
        bounceBullet.SetExplosionDamage(explosionDamage);
        bounceBullet.SetTeam(player.GetTeam());
        bounceBullet.SetOwner(player);
        bulletToShoot.SetActive(true);
        bulletToShoot.GetComponent<Rigidbody2D>().velocity = shootDirection * throwingStrength;
        Recoil();
    }
}
