using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class BounceBullet : MonoBehaviour
{
    private float fuseTimeLeft;
    private float explosionRadius;
    private float explosionPower;
    private float explosionDamage;
    private Color team;
    private Player owner;
    private Vector2 previousVelocity;
    private Rigidbody2D rb;
    private SpriteRenderer spriteRenderer;
    private List<Killline> killlines;
    [SerializeField] private List<AudioClip> bounceSounds;
    [SerializeField] private float bounceSoundVolume;
    [SerializeField] private float bounceFriction = .9f;


    private void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag(Harmony.Tags.Ground))
        {
            SoundManager.Instance.PlayRandomSoundEffect(bounceSounds, bounceSoundVolume);
            Vector2 reflectionAxis = collision.contacts[0].normal;
            Vector2 result = Vector2.Reflect(previousVelocity, reflectionAxis);
            this.rb.velocity = result * bounceFriction;
        }
        else if (collision.gameObject.CompareTag(Harmony.Tags.Player))
        {
            Player playerData = collision.gameObject.GetComponent<Player>();
            if (playerData && playerData.GetTeam() != this.team)
            {
                Explode();
            }
        }

    }

    public void SetTeam(Color team)
    {
        this.team = team;
        spriteRenderer.color = team;
    }

    public Color GetTeam() 
    { 
        return team; 
    }

    public void SetOwner(Player owner)
    {
        this.owner = owner;
    }

    public Player GetOwner()
    {
        return owner;
    }

    public void SetFuse(float fuseTime)
    {
        fuseTimeLeft = fuseTime;
    }
    public void SetExplosionRadius(float radius)
    {
        explosionRadius = radius;
    }

    public void SetExplosionPower(float power)
    {
        explosionPower = power;
    }

    public void SetExplosionDamage(float damage)
    {
        explosionDamage = damage;
    }

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        if (rb) {
            previousVelocity = rb.velocity;
        }

        fuseTimeLeft -= Time.deltaTime;
        if (fuseTimeLeft < 0)
        {
            Explode();
        }

        CheckKillLines();
    }

    private void CheckKillLines()
    {
        if (killlines == null || killlines.Count == 0)
        {
            killlines = FindObjectsByType<Killline>(FindObjectsSortMode.None).ToList();
        }
        else
        {
            bool anyNull = killlines.Any(item => item == null);
            if (anyNull)
                killlines = FindObjectsByType<Killline>(FindObjectsSortMode.None).ToList();
        }


        if (killlines != null)
        {
            foreach (Killline line in killlines)
            {
                if (line && line.ShouldDelete(this.transform.position))
                {
                    PoolManager.Instance.ReturnToPool(Harmony.PoolTags.bounce_bullet, this.gameObject);
                }
            }
        }
    }
    private void Explode()
    {
        Explosion explosion = PoolManager.Instance.Get(Harmony.PoolTags.explosion).GetComponent<Explosion>();

        explosion.gameObject.SetActive(true);
        explosion.transform.position = transform.position;
        explosion.SetExplosionPower(explosionPower);
        explosion.SetExplosionDamage(explosionDamage);
        explosion.SetTeam(team);
        explosion.SetOwner(owner);

        explosion.Explode(explosionRadius, new Vector2(0, 0));
        PoolManager.Instance.ReturnToPool(Harmony.PoolTags.bounce_bullet, gameObject);
    }
}
