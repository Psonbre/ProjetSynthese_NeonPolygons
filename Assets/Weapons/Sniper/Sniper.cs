using System.Collections.Generic;
using UnityEngine;

public class Sniper : Holdable
{
    [SerializeField] protected float bulletCooldown = 0.1f;
    [SerializeField] private LayerMask playerMask;
    [SerializeField] private LayerMask groundMask;
    [SerializeField] private LayerMask shieldMask;
    [SerializeField] private float digDistance = 10f;
    [SerializeField] private float maxSniperDistance = 1000f;
    [SerializeField] private float explosionWidth = 5f;
    [SerializeField] private float sniperDamage = 2f;
    [SerializeField] private float knockbackPower = 1000f;
    [SerializeField] private float beamOffset = 1f;
    [SerializeField] private float beamWidth = 1f;
    [SerializeField] private float colliderMoveBackAjust = 4f;
    [SerializeField] protected List<AudioClip> shootSounds;
    [SerializeField] protected float volumeMultiplicator = 1f;
    private LineRenderer lineRenderer;
    protected float cooldownLeft = 0f;

    protected void Start()
    {
        lineRenderer = GetComponent<LineRenderer>();
        lineRenderer.positionCount = 2;
        lineRenderer.startWidth = beamWidth;
        lineRenderer.endWidth = beamWidth;
        lineRenderer.enabled = true;
    }

    protected override void Awake()
    {
        this.cooldownLeft = 0;
        base.Awake();
    }

    private void OnEnable()
    {
        cooldownLeft = 0;
    }

    protected virtual void tryShoot()
    {
        if (cooldownLeft <= 0f)
        {
            cooldownLeft = bulletCooldown;
            shoot();
        }
    }

    protected virtual void shoot()
    {
        Vector2 direction = new Vector2(Mathf.Cos(rotation * Mathf.Deg2Rad), Mathf.Sin(rotation * Mathf.Deg2Rad));
        RaycastHit2D platformHit = Physics2D.Raycast((Vector2)transform.position + direction * beamOffset, direction, maxSniperDistance, groundMask);
        RaycastHit2D[] playerHits = Physics2D.RaycastAll((Vector2)transform.position + direction * beamOffset, direction, maxSniperDistance, playerMask);


        RaycastHit2D[] shieldHits = Physics2D.RaycastAll((Vector2)transform.position + direction * beamOffset, direction, maxSniperDistance, shieldMask);
        RaycastHit2D firstValidShieldHit = new RaycastHit2D();

        for (int i = 0; i < shieldHits.Length; i++)
        { 
            ShieldDomeController shield = shieldHits[i].collider.gameObject.GetComponent<ShieldDomeController>();
            if (shield) {
                if (shield.GetTeam() != player.GetTeam()) {
                    firstValidShieldHit = shieldHits[i];
                    break;
                }
            }
        }


        // Collision avec le sol (aucune collision si on entre en collision avec shield)
        if (platformHit && (!firstValidShieldHit || platformHit.distance < firstValidShieldHit.distance))
        {
            Explosion explosion = PoolManager.Instance.Get(Harmony.PoolTags.sniper_explosion).GetComponent<Explosion>();
            CapsuleCollider2D collider2D = explosion.GetComponent<CapsuleCollider2D>();

            // Si on entre en collision dans une plateforme mais qu'on commencerait � creuser dans un shield, on diminue la longueur du trou pour qu'elle touche la limite du shield.
            float collisionLength = digDistance;
            if (firstValidShieldHit)
            {
                if (platformHit.distance + digDistance > firstValidShieldHit.distance)
                {
                    collisionLength = firstValidShieldHit.distance - platformHit.distance;
                }
            }

            if (collider2D)
            {
                collider2D.transform.position = platformHit.point + (direction * (collisionLength - colliderMoveBackAjust)) / 2f;
                collider2D.transform.rotation = Quaternion.Euler(0, 0, rotation);
                collider2D.size = new Vector2(collisionLength, explosionWidth);

                explosion.gameObject.SetActive(true);
                explosion.SetExplosionPower(0);
                explosion.SetExplosionDamage(0);
                explosion.SetTeam(player.GetTeam());
                explosion.Explode(1, new Vector2(0, 0));
            }
        }

        // Collisions avec le joueur
        for (int i = 0; i < playerHits.Length; i++)
        {
            bool canHitPlayer = true;

            if (platformHit && playerHits[i].distance > platformHit.distance + digDistance)
                canHitPlayer = false;

            if (platformHit && playerHits[i].distance > platformHit.distance && playerHits[i].distance < platformHit.distance + digDistance) {

                //Achievement wallbang
                Achievements.Unlock(Harmony.Achievements.wallbang);
            }

            if (firstValidShieldHit && playerHits[i].distance > firstValidShieldHit.distance)
                canHitPlayer = false;

            if (canHitPlayer)
            {
                Player player = playerHits[i].collider.gameObject.GetComponent<Player>();
                player.ApplyKnockback(knockbackPower * player.GetKnockbackMult() * direction);
                if (this.player.GetTeam() != player.GetTeam())
                {
                    player.Hurt(sniperDamage);
                }
            }
        }

        SoundManager.Instance.PlayRandomSoundEffect(shootSounds, volumeMultiplicator);

        Recoil();
    }

    protected override void Update()
    {
        if (!player.GetIsDead())
        {
            Vector2 direction = new Vector2(Mathf.Cos(rotation * Mathf.Deg2Rad), Mathf.Sin(rotation * Mathf.Deg2Rad));


            RaycastHit2D anyHit = Physics2D.Raycast((Vector2)transform.position + direction * beamOffset, direction, maxSniperDistance, groundMask | playerMask);
            Vector2 anyHitPoint;

            RaycastHit2D[] shieldHits = Physics2D.RaycastAll((Vector2)transform.position + direction * beamOffset, direction, maxSniperDistance, shieldMask);
            RaycastHit2D firstValidShieldHit = new RaycastHit2D();

            for (int i = 0; i < shieldHits.Length; i++)
            {
                ShieldDomeController shield = shieldHits[i].collider.gameObject.GetComponent<ShieldDomeController>();
                if (shield)
                {
                    if (shield.GetTeam() != player.GetTeam())
                    {
                        firstValidShieldHit = shieldHits[i];
                        break;
                    }
                }
            }

            // On met le pointeur laser sur la plateforme/joueur/shield �nemie le plus proche.
            if (anyHit && ((firstValidShieldHit && anyHit.distance < firstValidShieldHit.distance) || !firstValidShieldHit))
                anyHitPoint = anyHit.point;
            else if (firstValidShieldHit)
                anyHitPoint = firstValidShieldHit.point;
            else
                anyHitPoint = (Vector2)transform.position + maxSniperDistance * direction;


            this.lineRenderer.enabled = true;
            lineRenderer.SetPosition(0, new Vector3(this.transform.position.x, this.transform.position.y, 1));
            lineRenderer.SetPosition(1, new Vector3(anyHitPoint.x, anyHitPoint.y, 1));

        }
        else {
            this.lineRenderer.enabled = false;
        }

        if (cooldownLeft > 0f)
            cooldownLeft -= Time.deltaTime;

        if (player.GetIsFiring())
        {
            tryShoot();
        }

        base.Update();
    }
}
