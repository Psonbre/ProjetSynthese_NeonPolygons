using NUnit.Framework;
using System.Collections.Generic;
using UnityEngine;

public class Grenade : Gun
{
	[SerializeField] private float throwingStrength = 200;
	[SerializeField] private float fuseTime = 3f;
	[SerializeField] private float explosionRadius = 10f;
	[SerializeField] private float explosionPower = 1000;
	[SerializeField] private float explosionDamage = 0.1f;
    [SerializeField] private List<AudioClip> throwSounds;
	[SerializeField] private float throwVolume = 1f;
	protected override void shoot()
	{
		GameObject bulletToShoot = PoolManager.Instance.Get(bulletType);

		bulletToShoot.transform.position = transform.position + new Vector3(
			bulletSpawnOffset * Mathf.Cos(rotation * Mathf.Deg2Rad),
			bulletSpawnOffset * Mathf.Sin(rotation * Mathf.Deg2Rad),
			0
		);
        bulletToShoot.transform.rotation = Quaternion.Euler(0, 0, rotation * Mathf.Deg2Rad);
		Vector2 shootDirection = new Vector2(
			Mathf.Cos(rotation * Mathf.Deg2Rad),
			Mathf.Sin(rotation * Mathf.Deg2Rad)
		);
        SoundManager.Instance.PlayRandomSoundEffect(throwSounds, throwVolume);
        GrenadeProjectile grenadeProjectile = bulletToShoot.GetComponent<GrenadeProjectile>();
		grenadeProjectile.SetFuse(fuseTime);
		grenadeProjectile.SetExplosionRadius(explosionRadius);
		grenadeProjectile.SetExplosionPower(explosionPower);
		grenadeProjectile.SetExplosionDamage(explosionDamage);
		grenadeProjectile.SetTeam(player.GetTeam());
		grenadeProjectile.SetOwner(player);
		bulletToShoot.SetActive(true);
		bulletToShoot.GetComponent<Rigidbody2D>().velocity = shootDirection * throwingStrength;
		Recoil();
	}
}
