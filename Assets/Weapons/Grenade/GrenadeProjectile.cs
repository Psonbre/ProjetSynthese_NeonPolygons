using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GrenadeProjectile : MonoBehaviour
{
	[SerializeField] private float fuseTimeAfterCollision = 1f;
	private float fuseTimeLeft = 10f;
	private float explosionRadius = 10f;
	private float explosionPower = 1000;
	private float explosionDamage = 0.1f;
	private Color team;
	private Player owner;
    private List<Killline> killlines;

    public void SetTeam(Color color)
	{
		team = color;
	}

	public Color GetTeam()
	{ 
		return team; 
	}

    public void SetOwner(Player owner)
    {
        this.owner = owner;
    }

    public Player GetOwner()
    {
        return owner;
    }

    public void SetFuse(float fuseTime)
	{
		fuseTimeLeft = fuseTime;
	}
	public void SetExplosionRadius(float radius)
	{
		explosionRadius = radius;
	}

	public void SetExplosionPower(float power)
	{
		explosionPower = power;
	}

	public void SetExplosionDamage(float damage)
	{
		explosionDamage = damage;
	}

	private void Update()
	{
		fuseTimeLeft -= Time.deltaTime;
		if (fuseTimeLeft < 0)
		{
			Explode();
		}

		TryKill();
    }

	private void OnCollisionEnter2D(Collision2D collision)
	{
		if (collision.gameObject.CompareTag(Harmony.Tags.Ground))
		{
			fuseTimeLeft = Mathf.Min(fuseTimeLeft, fuseTimeAfterCollision);
		}
	}

    private void TryKill()
    {
        if (killlines == null || killlines.Count == 0)
        {
            killlines = FindObjectsByType<Killline>(FindObjectsSortMode.None).ToList();
        }
        else
        {
            bool anyNull = killlines.Any(item => item == null);
            if (anyNull)
                killlines = FindObjectsByType<Killline>(FindObjectsSortMode.None).ToList();
        }


        if (killlines != null)
        {
            foreach (Killline line in killlines)
            {
                if (line && line.ShouldDelete(this.transform.position))
                {
                    PoolManager.Instance.ReturnToPool(Harmony.PoolTags.grenade, this.gameObject);
                }
            }
        }
    }
    private void Explode()
	{
		Explosion explosion = PoolManager.Instance.Get(Harmony.PoolTags.explosion).GetComponent<Explosion>();

		explosion.gameObject.SetActive(true);
		explosion.transform.position = transform.position;
		explosion.SetExplosionPower(explosionPower);
		explosion.SetExplosionDamage(explosionDamage);
		explosion.SetTeam(team);
		explosion.SetOwner(owner);

		explosion.Explode(explosionRadius, new Vector2(0, 0));
		PoolManager.Instance.ReturnToPool(Harmony.PoolTags.grenade, gameObject);
	}
}
