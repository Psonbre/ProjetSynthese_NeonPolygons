using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseballBat : Holdable
{
    [SerializeField] protected float bulletCooldown = 0.1f;
    [SerializeField] protected float hitOffset = 8f;
    [SerializeField] protected float hitRadius = 5f;
    [SerializeField] protected float knockbackPower = 1000f;
    [SerializeField] protected float damage = .5f;
    [SerializeField] protected LayerMask playerLayer;
    [SerializeField] protected LayerMask objectToReflectLayer;
    [SerializeField] protected float minimumReflectForce = 500f;
    [SerializeField] protected float reflectDuration = .2f;
    [SerializeField] protected List<AudioClip> hitSounds;
    [SerializeField] protected List<AudioClip> swingSounds;
    [SerializeField] protected float hitVolumeMultiplicator = 1f;
    [SerializeField] protected float swingVolumeMultiplicator = 1f;
    protected float cooldownLeft = 0f;

    protected override void Awake()
    {
        this.cooldownLeft = 0;
        base.Awake();
    }

    private void OnEnable()
    {
        cooldownLeft = 0;
    }

    protected virtual void tryHit()
    {
        if (cooldownLeft <= 0f)
        {
            cooldownLeft = bulletCooldown;
            Hit();
        }
    }

    protected IEnumerator ReflectCoroutine(float time) {

        float cooldown = time;
        bool hitSomething = false;
        while (cooldown >= 0f)
        {
            cooldown -= Time.deltaTime;

            Vector2 center = (Vector2)this.transform.position + hitOffset * new Vector2(Mathf.Cos(rotation * Mathf.Deg2Rad), Mathf.Sin(rotation * Mathf.Deg2Rad));
            Collider2D[] collisionsReflect = Physics2D.OverlapCircleAll(center, hitRadius, objectToReflectLayer);

            foreach (Collider2D collision in collisionsReflect)
            {
                Bullet bullet = collision.gameObject.GetComponent<Bullet>();
                GrenadeProjectile grenade = collision.gameObject.GetComponent<GrenadeProjectile>();
                C4Projectile c4 = collision.gameObject.GetComponent<C4Projectile>();
                BounceBullet bounceBall = collision.gameObject.GetComponent<BounceBullet>();

                if (bullet)
                {
                    bullet.transform.rotation = Quaternion.Euler(0, 0, this.rotation);
                    bullet.SetTeam(player.GetTeam());
                    if (!hitSomething)
                    {
                        SoundManager.Instance.PlayRandomSoundEffect(hitSounds, hitVolumeMultiplicator);
                        hitSomething = true;
                    }
                }

                if (grenade || c4 || bounceBall)
                {
                    Vector2 direction = new Vector2(Mathf.Cos(rotation * Mathf.Deg2Rad), Mathf.Sin(rotation * Mathf.Deg2Rad)).normalized;
                    Rigidbody2D rb = collision.GetComponent<Rigidbody2D>();
                    if (rb)
                    {
                        rb.velocity = direction * Mathf.Max(minimumReflectForce, rb.velocity.magnitude);
                    }

                    if (grenade)
                        grenade.SetTeam(player.GetTeam()); 
                    if (c4)
                        c4.SetTeam(player.GetTeam()); 
                    if (bounceBall)
                        bounceBall.SetTeam(player.GetTeam());


                    if (!hitSomething)
                    {
                        SoundManager.Instance.PlayRandomSoundEffect(hitSounds, swingVolumeMultiplicator);
                        hitSomething = true;
                    }
                }
            }
            yield return null;
        }
        
    }


    protected virtual void Hit() {

        Vector2 center = (Vector2)this.transform.position + hitOffset * new Vector2(Mathf.Cos(rotation * Mathf.Deg2Rad), Mathf.Sin(rotation * Mathf.Deg2Rad));
        Collider2D[] collisionsPlayer = Physics2D.OverlapCircleAll(center, hitRadius, playerLayer);

        bool hitSomeone = false;

        foreach (Collider2D collision in collisionsPlayer)
        {
            if (!collision.gameObject.Equals(player.gameObject))
            {
                Player playerToHit = collision.gameObject.GetComponent<Player>();
                if (playerToHit != null)
                {
                    hitSomeone = true;
                    Vector2 force = knockbackPower * playerToHit.GetKnockbackMult() * new Vector2(Mathf.Cos(rotation * Mathf.Deg2Rad), Mathf.Sin(rotation * Mathf.Deg2Rad)).normalized;
                    playerToHit.ApplyKnockback(force);
                    playerToHit.SetLastHitBy(player);
                    if (playerToHit.GetTeam() != this.player.GetTeam())
                    {
                        playerToHit.Hurt(damage);
                    }
                }
            }
        }

        if (hitSomeone)
            SoundManager.Instance.PlayRandomSoundEffect(hitSounds, hitVolumeMultiplicator);
        else
            SoundManager.Instance.PlayRandomSoundEffect(swingSounds, swingVolumeMultiplicator);

        StopAllCoroutines();
        StartCoroutine(ReflectCoroutine(reflectDuration));

        Recoil();
    }

    private void OnDrawGizmos()
    {
        Vector2 center = (Vector2)this.transform.position + hitOffset * new Vector2(Mathf.Cos(rotation * Mathf.Deg2Rad), Mathf.Sin(rotation * Mathf.Deg2Rad));
        Gizmos.DrawWireSphere(center, hitRadius);
    }

    protected override void Update()
    {

        if (cooldownLeft > 0f)
            cooldownLeft -= Time.deltaTime;

        if (player.GetIsFiring())
        {
            tryHit();
        }
        base.Update();
    }
}
