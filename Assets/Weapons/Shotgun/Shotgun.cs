using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shotgun : Gun
{
    [SerializeField] private int shotCount = 10;
    [SerializeField] private float shotRange = 30f;
    protected override void shoot()
    {
        SoundManager.Instance.PlayRandomSoundEffect(shootSounds, volumeMultiplicator);

        for (int i = 0; i < shotCount; i++)
        {
            GameObject bulletToShoot = PoolManager.Instance.Get(bulletType);

            float randomRotation = (rotation + Random.Range(-shotRange / 2f, shotRange / 2f) + 360) % 360;

            Vector2 direction = new Vector2(Mathf.Cos(randomRotation * Mathf.Deg2Rad), Mathf.Sin(randomRotation * Mathf.Deg2Rad));
            RaycastHit2D hit = Physics2D.Raycast(transform.position, direction, offsetFromCenter + bulletSpawnOffset, terrainMask);

            if (hit)
            {
                bulletToShoot.transform.position = hit.point;
            }
            else
            {
                bulletToShoot.transform.position = transform.position + new Vector3(
                    bulletSpawnOffset * Mathf.Cos(randomRotation * Mathf.Deg2Rad),
                    bulletSpawnOffset * Mathf.Sin(randomRotation * Mathf.Deg2Rad),
                    0
                );
            }

            Bullet bullet = bulletToShoot.GetComponent<Bullet>();
            if (bullet) bullet.SetTeam(player.GetTeam());

            bulletToShoot.transform.rotation = Quaternion.Euler(0, 0, randomRotation);
            bulletToShoot.SetActive(true);
            
        }
		Recoil();
	}
}
