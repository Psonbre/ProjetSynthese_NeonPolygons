using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lazer : Gun
{
	[SerializeField] private LazerBeam lazerBeam;
	[SerializeField] private float startingFadeLenght = 3;
	[SerializeField] private float activeLazerDuration = 1;
	[SerializeField] private float endingFadeLenght = 1;
	[SerializeField] private float damageInterval = 0.1f;
	[SerializeField] private float spinSpeedWhenCharging = 0.001f;
    [SerializeField] private AudioClip laserSound;
	[SerializeField] private float laserVolume = 1f;
    private float defaultSpinSpeed;

	protected override void Start()
	{
		base.Start();
		defaultSpinSpeed = spinSpeed;
	}

    protected override void OnEnable()
    {
		base.OnEnable();
		if (defaultSpinSpeed != 0)
			spinSpeed = defaultSpinSpeed;
    }

    protected override void shoot()
	{
		lazerBeam.SetTeam(player.GetTeam());
		lazerBeam.SetOwner(player);
		StartCoroutine(activateLazer());
	}

	private IEnumerator activateLazer()
	{
		spinSpeed = spinSpeedWhenCharging;
		player.FreezeFor(startingFadeLenght + activeLazerDuration + endingFadeLenght);
		lazerBeam.ActivatePreviewFade(startingFadeLenght, 0, 0.5f, 0, 0);

        SoundManager.Instance.PlaySoundEffect(laserSound, laserVolume);
        yield return new WaitForSeconds(startingFadeLenght);
		spinSpeed = 0;
		lazerBeam.Activate(damageInterval);
        yield return new WaitForSeconds(activeLazerDuration);
		lazerBeam.Deactivate();
		lazerBeam.ActivatePreviewFade(endingFadeLenght, 1, 0, 0.05f, 0);

        yield return new WaitForSeconds(endingFadeLenght);
		spinSpeed = defaultSpinSpeed;
	}
}
