using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LazerBeam : MonoBehaviour
{
	[SerializeField] SpriteRenderer spriteRendererCenter;
	[SerializeField] SpriteRenderer spriteRendererOutline;
	[SerializeField] Collider2D lazerCollider;
    [SerializeField] float explosionSize = 20f;
    [SerializeField] float explosionDamage = .3f;
    [SerializeField] float speedMultWhenHit = 500f;

    private float defaultScale = 0.2f;
	private float damageInterval;
	private float timeUntilNextDamage;
	private Color team;
	private Player owner;

	private void Start()
	{
		defaultScale = transform.localScale.x;
		lazerCollider = GetComponent<CapsuleCollider2D>();
	}

	private void OnEnable()
	{
		spriteRendererCenter.color = new(spriteRendererCenter.color.r, spriteRendererCenter.color.g, spriteRendererCenter.color.b, 0);
		spriteRendererOutline.color = new(spriteRendererOutline.color.r, spriteRendererOutline.color.g, spriteRendererOutline.color.b, 0);
		lazerCollider.enabled = false;
	}

	public void SetTeam(Color team)
	{
		this.team = team;
		spriteRendererOutline.color = team;
	}

    public void SetOwner(Player owner)
    {
        this.owner = owner;
    }

    public void ActivatePreviewFade(float duration, float startAlpha, float endAlpha, float startScaleModifier, float endScaleModifier)
	{
		StopAllCoroutines();
		StartCoroutine(PreviewFade(duration, startAlpha, endAlpha, startScaleModifier, endScaleModifier));
	}
	
	private IEnumerator PreviewFade(float duration, float startAlpha, float endAlpha, float startScaleModifier, float endScaleModifier)
	{
		float time = 0;
		
		while (time < duration)
		{
			time += Time.deltaTime;

			float normalizedTime = time / duration;
			spriteRendererCenter.color = new(spriteRendererCenter.color.r, spriteRendererCenter.color.g, spriteRendererCenter.color.b, Mathf.Lerp(startAlpha, endAlpha, normalizedTime));
			spriteRendererOutline.color = new(spriteRendererOutline.color.r, spriteRendererOutline.color.g, spriteRendererOutline.color.b, Mathf.Lerp(startAlpha, endAlpha, normalizedTime));
			transform.localScale = Mathf.Lerp(defaultScale + startScaleModifier, defaultScale + endScaleModifier, normalizedTime) * Vector2.one;
			spriteRendererOutline.transform.localScale = Mathf.Lerp(defaultScale + startScaleModifier, defaultScale + endScaleModifier, normalizedTime) * Vector2.one;
			yield return null;
		}
	}

	public void Activate(float damageInterval) 
	{
		this.damageInterval = damageInterval;
		timeUntilNextDamage = this.damageInterval;
		StopAllCoroutines();
		transform.localScale = (defaultScale + 0.05f) * Vector2.one;
		spriteRendererOutline.transform.localScale = (defaultScale + 0.05f) * Vector2.one;
		lazerCollider.enabled = true;
		spriteRendererCenter.color = new(spriteRendererCenter.color.r, spriteRendererCenter.color.g, spriteRendererCenter.color.b, 1);
		spriteRendererOutline.color = new(spriteRendererOutline.color.r, spriteRendererOutline.color.g, spriteRendererOutline.color.b, 1);
	}

	public void Deactivate()
	{
		StopAllCoroutines();
		lazerCollider.enabled = false;
	}

    private void Update()
    {
        timeUntilNextDamage -= Time.deltaTime;

		if (timeUntilNextDamage < 0)
		{
			timeUntilNextDamage = damageInterval;
			if (lazerCollider.isActiveAndEnabled)
			{
				List<Collider2D> colliders = new();
				lazerCollider.Overlap(colliders);

				colliders.ForEach(collider =>
				{
					if (collider.CompareTag(Harmony.Tags.Player) && collider.GetComponent<Player>().GetTeam() != team)
					{
						Vector2 direction = new Vector2(Mathf.Cos(transform.eulerAngles.z * Mathf.Deg2Rad), Mathf.Sin(transform.eulerAngles.z));
                        collider.GetComponent<Rigidbody2D>().velocity = direction * speedMultWhenHit;
						Explosion explosion = PoolManager.Instance.Get(Harmony.PoolTags.explosion).GetComponent<Explosion>();
						explosion.transform.position = collider.transform.position;
						explosion.gameObject.SetActive(true);
						explosion.SetTeam(team);
						explosion.SetOwner(owner);
						explosion.SetExplosionDamage(explosionDamage);
						explosion.Explode(explosionSize, Vector2.zero);
					}
				});
			}
		}
    }
}
