using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class C4Projectile : MonoBehaviour
{
    [SerializeField] private float explosionRadius = 1;
    [SerializeField] private LayerMask groundLayer;
    [SerializeField] float explosionPower = 1f;
    [SerializeField] float explosionDamage = 1f;
    [SerializeField] Color team;
    [SerializeField] Color skinColor;
    [SerializeField] float c4OnPlayerOffset = 5f;
    [SerializeField] private List<AudioClip> stickSounds;
    [SerializeField] private float stickSoundsVolume = 1f;

    private Rigidbody2D rb;
    private Player owner;
    private GameObject platform;
    private GameObject attachedPlayer;
    private List<Killline> killlines;


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag(Harmony.Tags.Player) && !platform && collision.gameObject.GetComponent<Player>() && collision.gameObject.GetComponent<Player>().GetTeam() != this.team) {
            SoundManager.Instance.PlayRandomSoundEffect(stickSounds, stickSoundsVolume);
            this.attachedPlayer = collision.gameObject;
        }
        else if (collision.gameObject.CompareTag(Harmony.Tags.Ground) && !attachedPlayer)
        {
            SoundManager.Instance.PlayRandomSoundEffect(stickSounds, stickSoundsVolume);
            this.platform = collision.gameObject;
        }
        else if (collision.gameObject.CompareTag(Harmony.Tags.DestroyArea)) {
            Explode(new Vector2(0, 0));
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (platform && collision.gameObject.CompareTag(Harmony.Tags.Ground) && collision.gameObject == platform.gameObject)
        {
            this.platform = null;
        }
    }


    private void OnEnable()
    {
        this.attachedPlayer = null;
        this.platform = null;
        setKinematicState(false);
    }

    public void setVelocity(Vector2 velocity)
    {
        if (rb)
            rb.velocity = velocity;
    }

    public void setKinematicState(bool isKinematic)
    {
        if (rb)
        {
            if (isKinematic) {
                rb.velocity = new Vector2(0, 0);
            }
            rb.isKinematic = isKinematic;
        }
    }

    public void attachTo(bool shouldAttach, GameObject objToAttachTo) {
        if (rb)
        {
            if (shouldAttach && objToAttachTo)
            {
                this.transform.parent = objToAttachTo.transform;
                rb.constraints = RigidbodyConstraints2D.FreezeAll;
            }
            else if (!shouldAttach)
            {
                this.transform.parent = PoolManager.Instance.transform;
                rb.constraints = RigidbodyConstraints2D.None;
            }
        }
    }

    private void Awake()
    {
        this.rb = GetComponent<Rigidbody2D>();
    }

    public void Update()
    {
        if (rb)
        {
            bool hasPlatform = platform != null;
            bool hasPlayer = attachedPlayer != null;
            setKinematicState(hasPlatform || hasPlayer);

            if (hasPlayer)
            {
                attachTo(hasPlayer, attachedPlayer);
                this.transform.position = attachedPlayer.transform.position + new Vector3(0, c4OnPlayerOffset, 0);
            }
            else if (hasPlatform)
                attachTo(hasPlatform, platform);
            else
                attachTo(false, null);

        }

        TryKill();
    }

    public void SetTeam(Color team)
    {
        this.team = team;
    }

    public Color GetTeam()
    {
        return team;
    }

    public void SetOwner(Player owner)
    {
        this.owner = owner;
    }

    public Player GetOwner()
    {
        return owner;
    }

    public void Explode(Vector2 extraForce)
    {
        Explosion explosion = PoolManager.Instance.Get(Harmony.PoolTags.explosion).GetComponent<Explosion>();
        explosion.gameObject.SetActive(true);
        explosion.transform.position = transform.position;
        explosion.SetExplosionPower(explosionPower);
        explosion.SetExplosionDamage(explosionDamage);
        explosion.SetTeam(this.team);
        explosion.SetOwner(this.owner);
        explosion.Explode(explosionRadius, extraForce);
        attachTo(false, null);
        this.gameObject.SetActive(false);
    }

    private void TryKill()
    {
        if (killlines == null || killlines.Count == 0)
        {
            killlines = FindObjectsByType<Killline>(FindObjectsSortMode.None).ToList();
        }
        else
        {
            bool anyNull = killlines.Any(item => item == null);
            if (anyNull)
                killlines = FindObjectsByType<Killline>(FindObjectsSortMode.None).ToList();
        }


        if (killlines != null)
        {
            foreach (Killline line in killlines)
            {
                if (line && line.ShouldDelete(this.transform.position))
                {
                    this.gameObject.SetActive(false);
                    break;
                }
            }
        }
    }

    private void OnDisable()
    {
        PoolManager.Instance.ReturnToPool(Harmony.PoolTags.c4, this.gameObject);
    }
}
