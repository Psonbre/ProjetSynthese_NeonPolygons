using NUnit.Framework;
using System.Collections.Generic;
using UnityEngine;
public class Gun : Holdable
{
    [SerializeField] protected float bulletCooldown = 0.1f;
    [SerializeField] protected string bulletType;
    [SerializeField] protected float bulletSpawnOffset = 2;
    [SerializeField] protected LayerMask terrainMask;
    [SerializeField] protected List<AudioClip> shootSounds;
    [SerializeField] protected float volumeMultiplicator = 1f;
    protected ParticleSystem ps;
    protected float cooldownLeft = 0f;

    protected virtual void Start()
    {
        ps = GetComponent<ParticleSystem>();
    }

    protected override void Awake()
    {
        this.cooldownLeft = 0;
        base.Awake();
    }

	protected virtual void OnEnable()
	{
        cooldownLeft = 0;
	}

	protected virtual void tryShoot() {
        if (cooldownLeft <= 0f)
        {
            cooldownLeft = bulletCooldown;
            shoot();
        }
    }

    protected virtual void shoot() {
        GameObject bulletToShoot = PoolManager.Instance.Get(bulletType);

        Vector2 direction = new Vector2(Mathf.Cos(rotation * Mathf.Deg2Rad), Mathf.Sin(rotation * Mathf.Deg2Rad));
        RaycastHit2D hit = Physics2D.Raycast(transform.position - (Vector3)(direction * offsetFromCenter), direction, offsetFromCenter + bulletSpawnOffset, terrainMask);

        if (hit) {
            bulletToShoot.transform.position = hit.point;
        }
        else {
            bulletToShoot.transform.position = transform.position + new Vector3(
                bulletSpawnOffset * Mathf.Cos(rotation * Mathf.Deg2Rad),
                bulletSpawnOffset * Mathf.Sin(rotation * Mathf.Deg2Rad),
                0
            );
        }

        Bullet bullet = bulletToShoot.GetComponent<Bullet>();
        if (bullet)
        {
            bullet.SetTeam(player.GetTeam());
            bullet.SetOwner(player);
        }
        bulletToShoot.transform.rotation = Quaternion.Euler(0, 0, rotation);
        if (ps)
        {
            ps.startColor = player.GetTeam();
			ps.Play();
		}

        SoundManager.Instance.PlayRandomSoundEffect(shootSounds, volumeMultiplicator);
        bulletToShoot.SetActive(true);
        Recoil();
    }

    protected override void Update()
    {
        
        if (cooldownLeft > 0f)
            cooldownLeft -= Time.deltaTime;

        if (player.GetIsFiring()) {
            tryShoot();
        }
        base.Update();
    }
}
