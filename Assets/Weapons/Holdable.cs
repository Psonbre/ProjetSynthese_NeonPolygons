using UnityEngine;

public class Holdable : MonoBehaviour
{
    [SerializeField] protected float spinSpeed = .2f;
    [SerializeField] protected float offsetFromCenter = 8f;
    [SerializeField] protected float recoilRecoverySpeed = 1f;
	[SerializeField] protected float recoil = 10f;

    [SerializeField] protected float recoilAngle = 0;
    [SerializeField] protected float recoilAngleSpeed = 0;

    [SerializeField] protected float precoilSpeed = 0;

    [SerializeField] protected float aimAssistDistance = 1000f;
    [SerializeField] protected bool shouldAimAssist = true;
    [SerializeField] protected bool shouldAimAssistThroughWalls = false;
    [SerializeField] protected float angleBeforeAssist = 10f;
    [SerializeField] protected LayerMask aimAssistLayer;
    [SerializeField] protected LayerMask groundLayer;

    private bool isPrecoil = false;
    protected float recoilAngleCooldown;
    protected float defaultOffsetFromCenter = 8f;
    protected Player player;
    protected SpriteRenderer playerSpriteRenderer;
    protected float rotation = 0f;

    protected virtual void Awake()
    {
        defaultOffsetFromCenter = offsetFromCenter;
        this.player = null;
        if (transform.parent != null && transform.parent.GetComponentInParent<Player>()) 
        {
			this.player = transform.parent.GetComponentInParent<Player>();
            playerSpriteRenderer = player.GetComponentInChildren<SpriteRenderer>();
		}    
    }
	protected void Recoil()
	{
		offsetFromCenter -= recoil;
        isPrecoil = true;
    }

	protected void UpdateRotationAndPosition(float goalRotation)
    {
        float newRotation = Mathf.MoveTowardsAngle(this.rotation, goalRotation, spinSpeed * Time.deltaTime);

        newRotation = (newRotation + 360) % 360;

        rotation = newRotation;

        Vector3 localScale = this.transform.localScale;
        localScale.y = Mathf.Abs(localScale.y);
        if (newRotation > 90f && newRotation < 270f)
        {
            localScale.y *= -1;
            newRotation -= recoilAngleCooldown;
        }
        else {
            newRotation += recoilAngleCooldown;
        }

        newRotation = (newRotation + 360) % 360;

        transform.rotation = Quaternion.Euler(0, 0, newRotation);

        transform.localScale = localScale;

        transform.position = playerSpriteRenderer.bounds.center + new Vector3(
            offsetFromCenter * Mathf.Cos(newRotation * Mathf.Deg2Rad),
            offsetFromCenter * Mathf.Sin(newRotation * Mathf.Deg2Rad),
            0
        );
    }

    private float AngleAfterAimAssist(float goalRotation)
    {
        float newAngle = goalRotation;

        RaycastHit2D[] hits = Physics2D.RaycastAll(player.transform.position, new Vector2(Mathf.Cos(goalRotation), Mathf.Sin(goalRotation)), aimAssistDistance, aimAssistLayer | groundLayer);
        foreach (RaycastHit2D hit in hits) {
            if (!shouldAimAssistThroughWalls && hit.collider.CompareTag(Harmony.Tags.Ground))
                break;

            Player hitPlayer = hit.collider.transform.parent.GetComponent<Player>();
            if (hitPlayer && !hitPlayer.GetIsDead() && hitPlayer.GetTeam() != this.player.GetTeam()) {
                Vector2 direction = hit.collider.transform.position - player.transform.position;
                float angle = Mathf.Atan2(direction.y, direction.x);
                if (Mathf.Abs(goalRotation - angle) < angleBeforeAssist * Mathf.Deg2Rad)
                    newAngle = angle;
                break;
            }
        }
        return newAngle;

    }


    protected virtual void Update() {
        offsetFromCenter = Mathf.MoveTowards(offsetFromCenter, defaultOffsetFromCenter, Mathf.Abs(defaultOffsetFromCenter - offsetFromCenter) * recoilRecoverySpeed * Time.deltaTime);

        if (isPrecoil)
        {
            if (recoilAngleCooldown < recoilAngle)
                recoilAngleCooldown = Mathf.Min(recoilAngle, recoilAngleCooldown + (Time.deltaTime * precoilSpeed));
            else
                isPrecoil = false;
        }
        else {
            recoilAngleCooldown = Mathf.Max(0, recoilAngleCooldown - (Time.deltaTime * recoilAngleSpeed));
        }

        if (player && !player.GetAllInputsFrozen())
        {
            Vector2 lookDirection = player.GetLookInput();
            float angle = Mathf.Atan2(lookDirection.y, lookDirection.x);
            angle = AngleAfterAimAssist(angle);
            UpdateRotationAndPosition(angle * Mathf.Rad2Deg);
        }
    }
}
