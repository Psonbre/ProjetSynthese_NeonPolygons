using System.Collections;
using System.Collections.Generic;
using System.Security;
using UnityEngine;

public class OneShotParticles : MonoBehaviour
{
	[SerializeField] private string poolManagerTag;
    private ParticleSystem particle;

	private void Awake()
	{
		particle = GetComponent<ParticleSystem>();
	}

	private void OnEnable()
	{
		particle.Play();
	}

	private void OnDisable()
	{
		PoolManager.Instance.ReturnToPool(poolManagerTag, gameObject);
	}
}
