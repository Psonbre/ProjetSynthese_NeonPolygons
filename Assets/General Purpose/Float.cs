using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Float : MonoBehaviour
{
    [SerializeField] private float intensity = 1;

    private float randomX = 0;
    private float randomY = 0;
    private Vector2 initialPosition;
    private RectTransform rectTransform;


    void Start()
    {
        rectTransform = GetComponent<RectTransform>();
        initialPosition = rectTransform.anchoredPosition;
        randomX = 200f * (Random.value -0.5f);
        randomY = 200f * (Random.value - 0.5f);
    }

    void Update()
    {
        rectTransform.anchoredPosition = new(initialPosition.x + Mathf.Cos(Time.time + randomX) * intensity, initialPosition.y + Mathf.Sin(Time.time + randomY) * intensity);
    }
}
