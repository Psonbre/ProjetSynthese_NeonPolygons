using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class CharacterSelectionManager : MonoBehaviour
{
    public static CharacterSelectionManager Instance { get; private set; }

    [Header("Countdown")]
    [SerializeField] private TextMeshProUGUI countdown;
    [SerializeField] private GameObject countdownBanner;
    [SerializeField] private float smallCountdownFontsize = 36f;
    [SerializeField] private float largeCountdownFontsize = 46f;
    [SerializeField] private int countdownDuration = 3;
    [SerializeField] private float countdownSoundVolume = 1f;
    [SerializeField] private List<AudioClip> countdownSound;
    [SerializeField] private float countdownShrinkSpeed = 40f;

    [Header("Canvas")]
    [SerializeField] private Transform characterSelectionCanvas;

    [Header("Customization")]
    [SerializeField] private List<Color> teams;
    [SerializeField] private List<AbilitySelection> abilities;

    [Header("Sounds")]
    [SerializeField] private List<AudioClip> joinSound;
    [SerializeField] private float joinSoundVolume = 1f;

    [Header("Other")]
    [SerializeField] private GameObject tutorial;
    [SerializeField] private float timeBeforeSwitchFromCharacterToGame = 1.5f;

    List<Vector2> slots = new List<Vector2>() {
        new(-120,40), new(-40,40), new(40,40), new(120, 40),
        new(-120,-40), new(-40,-40), new(40,-40), new(120,-40),
    };

    private List<GameObject> playerCustomizationMenus = new();
    private List<Sprite> playerSkins;

    private MultiplayerManager multiplayerManager;

    private Banner[] banners;

    private void Awake()
    {

        if (Instance == null)
        {
            Instance = this;
            playerSkins = Resources.LoadAll<Sprite>(Harmony.GameObjects.DefaultSkins).ToList();
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        multiplayerManager = MultiplayerManager.Instance;

        List<Sprite> unlockableSkins = Resources.LoadAll<Sprite>(Harmony.GameObjects.UnlockableSkins).ToList();
        foreach (Sprite skin in unlockableSkins)
        {
            if (Achievements.achievements.TryGetValue(skin.name, out Achievement achievement) && achievement.GetTimeUnlocked() != null) playerSkins.Add(skin);
        }

        GetBanners()[0].OpenFully(0.5f);
    }
    private void Update()
    {
        countdown.fontSize = Mathf.MoveTowards(countdown.fontSize, smallCountdownFontsize, Time.deltaTime * countdownShrinkSpeed);
    }

    public Sprite GetClosestAvailableSkin(int skinIndex)
    {
        int actualIndex = skinIndex % playerSkins.Count;
        actualIndex = actualIndex < 0 ? actualIndex + playerSkins.Count : actualIndex;

        Sprite skin = playerSkins[actualIndex];

        foreach (Player player in multiplayerManager.GetAllPlayers())
        {
            if (player.GetSkin() == skin)
            {
                return null;
            }
        }

        return skin;
    }

    public List<Color> GetAllTeams()
    {
        return teams;
    }

    public Color GetTeam(int teamIndex)
    {
        return teams[teamIndex];
    }

    public int GetTeamNumber(Color color)
    {
        return teams.IndexOf(color) + 1;
    }

    public void CheckAllPlayersReady()
    {
        List<CharacterSelectionMenu> menus = characterSelectionCanvas.GetComponentsInChildren<CharacterSelectionMenu>().ToList();
        Color nullTeam = new(0, 0, 0, 0);
        Color team1 = nullTeam;
        bool atLeastTwoTeams = false;
        bool allReady = true;
        foreach (CharacterSelectionMenu menu in menus)
        {
            if (!menu.IsReady)
            {
                allReady = false;
                break;
            }
            if (team1 == nullTeam) team1 = menu.GetCurrentTeam();
            else if (team1 != menu.GetCurrentTeam()) atLeastTwoTeams = true;
        }

        if (allReady && atLeastTwoTeams && menus.Count > 1)
        {
            StopAllCoroutines();
            StartCoroutine(Countdown());
        }
        else
        {
            StopAllCoroutines();
            countdownBanner.gameObject.SetActive(false);
        }
    }

    public void RemoveCustomizationMenu(GameObject menu)
    {
        playerCustomizationMenus.Remove(menu);
    }
    public List<AbilitySelection> GetAbilities()
    {
        return abilities;
    }

    public void JoinPlayer(PlayerInput playerInput)
    {
        DontDestroyOnLoad(playerInput.gameObject);
        SoundManager.Instance.PlayRandomSoundEffect(joinSound, joinSoundVolume);
        playerInput.transform.SetParent(characterSelectionCanvas, false);
        playerInput.transform.position = slots[playerInput.playerIndex];
        playerCustomizationMenus.Add(playerInput.gameObject);
        multiplayerManager.AddPlayerToList(playerInput.GetComponentInChildren<Player>());
    }

    private IEnumerator Countdown()
    {
        countdownBanner.gameObject.SetActive(true);
        int timeLeft = countdownDuration;

        while (timeLeft > 0)
        {
            countdown.text = timeLeft.ToString();
            countdown.fontSize = largeCountdownFontsize;
            SoundManager.Instance.PlayRandomSoundEffect(countdownSound, countdownSoundVolume);
            yield return new WaitForSeconds(1f);
            timeLeft--;
        }
        countdownBanner.gameObject.SetActive(false);

        List<CharacterSelectionMenu> menus = characterSelectionCanvas.GetComponentsInChildren<CharacterSelectionMenu>().ToList();
        foreach (CharacterSelectionMenu menu in menus)
        {
            PlayerInput playerInput = menu.transform.parent.GetComponent<PlayerInput>();
            playerInput.transform.SetParent(multiplayerManager.transform);

            Player playerCharacter = playerInput.GetComponentInChildren<Player>();
            playerCharacter.GetComponent<Rigidbody2D>().gravityScale = 60;
            playerCharacter.SetDefaultGravity(60);
            playerCharacter.Jump();

            playerCharacter.GetComponentInChildren<SpriteRenderer>().sortingLayerName = Harmony.Layers.Default.ToString();

            Destroy(menu.transform.Find(Harmony.GameObjects.Platform).gameObject);
            Destroy(menu.gameObject);
        }
        Destroy(characterSelectionCanvas.gameObject);
        MultiplayerManager.Instance.GetComponent<PlayerInputManager>().joinBehavior = PlayerJoinBehavior.JoinPlayersManually;

        yield return new WaitForSeconds(timeBeforeSwitchFromCharacterToGame);

        foreach (Player player in multiplayerManager.GetAllPlayers())
        {
            player.transform.localScale = Vector3.one * 1.2f;
            player.GetComponentInParent<PlayerInput>().SwitchCurrentActionMap(Harmony.InputActionMaps.Player);
            player.SetPreventAllInputs(true);
        }

        GetBanners()[0].CloseFully(0.5f);
        tutorial.SetActive(true);
    }

    private void LoadGameScene()
    {
        SceneManager.LoadScene(Harmony.Scenes.GameScene);
    }

    private Banner[] GetBanners()
    {
        if (banners == null || banners.Length <= 0)
            return FindObjectsByType<Banner>(FindObjectsSortMode.InstanceID);
        return banners;
    }


}

[System.Serializable]
public class AbilitySelection
{
    public string abilityName;
    public string abilityTitle;
    public string abilityDesc;
    public Sprite abilitySprite;
}