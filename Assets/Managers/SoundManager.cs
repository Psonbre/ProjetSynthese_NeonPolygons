using System.Collections.Generic;
using UnityEngine;
using System;


[Serializable]
public struct ThemeClip
{
    public string themeName;
    public AudioClip clip;
}

public class SoundManager : MonoBehaviour
{
    private static SoundManager instance = null;
    public static SoundManager Instance { get { return instance; } }

    [SerializeField] private AudioClip mainMenuMusic;
    [SerializeField] private List<ThemeClip> gameMusics;
    [SerializeField] private AudioClip introMusic;
    [SerializeField] private AudioClip endMusic;

    private AudioSource audioSource;

    public AudioClip mainMenuMusicClip { get { return mainMenuMusic; } }

    public AudioClip introMusicClip { get { return introMusic; } }
    public AudioClip endMusicClip { get { return endMusic; } }

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(instance);
        }
        else if (instance != this)
            Destroy(gameObject);
    }

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }
    public AudioClip GetMusicByTheme(string theme)
    {
        return gameMusics.Find(x => x.themeName == theme).clip;
    }

    public void PlayRandomSoundEffect(List<AudioClip> audioClips, float volume)
    {
        if (audioClips == null || audioClips.Count == 0) return;
        PlaySoundEffect(audioClips[UnityEngine.Random.Range(0, audioClips.Count)], volume);
    }

    public void PlaySoundEffect(AudioClip audioClip, float volume = 1)
    {
        audioSource.volume = GetSFXVolume();
        audioSource.PlayOneShot(audioClip, volume);
    }

    public void PlaySound(AudioClip audioClip, float volume) {
        audioSource.volume = volume;
        audioSource.PlayOneShot(audioClip);
    }

    public float GetMusicVolume()
    {
        return SettingsManager.GetMusicVolume() * SettingsManager.GetMasterVolume();
    }

    public float GetSFXVolume()
    {
        return SettingsManager.GetSFXVolume() * SettingsManager.GetMasterVolume();
    }
}
