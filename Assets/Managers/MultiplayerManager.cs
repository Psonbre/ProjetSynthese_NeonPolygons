using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.InputSystem;

public class MultiplayerManager : MonoBehaviour
{
	public static MultiplayerManager Instance { get; private set; }
    private List<Player> players = new();

    private void Awake()
	{
		if (Instance == null)
		{
			Instance = this;
			DontDestroyOnLoad(gameObject);
		}
		else if (Instance != this)
		{
			Destroy(gameObject);
		}
	}

	public void PrepareAllPlayersForNextRound()
	{
		players.ForEach(p => StartCoroutine(PreparePlayerForRound(p)));
		SetPlayerPositions();
	}
    public void EmptyDontDestroyOnLoad()
    {
        foreach (var root in gameObject.scene.GetRootGameObjects())
        {
            if (root != SoundManager.Instance.gameObject && root != SaveSystem.Instance.gameObject && root != AchievementPopUpManager.Instance.gameObject) Destroy(root);
        }
    }

    public void UnfreezeAllLivingPlayers()
	{
		GetAllLivingPlayers().ForEach(p => p.Unfreeze());
	}

	public void TryResetAllVibrations() {
        GetAllPlayers().ForEach(p => p.TryResetVibration());
    }


    public List<Player> GetAllLivingPlayers()
	{
		return players.FindAll(p => !p.GetIsDead());
	}

	public void AddPlayerToList(Player player)
	{
		players.Add(player);
	}

	public void RemovePlayer(Player player)
	{
		players.Remove(player);
	}
	public void DeactivateAllPlayers()
	{
		players.ForEach(p => p.gameObject.SetActive(false));
	}

    public void CheckForWinner()
	{
		UpdateMusicSpeed();
		List<Player> winners = new List<Player>();
		bool foundTeam = false;
		bool roundOver = true;
		Color winningTeam = Color.black;

		foreach (Player player in players)
		{
			if(!player.GetIsDead())
			{
				if(!foundTeam)
				{
					winningTeam = player.GetTeam();
					foundTeam = true;
				}
				else if(winningTeam != player.GetTeam())
				{
					roundOver = false;
					break;
				}
			}
		}

        if (roundOver)
		{
			MusicManager.Instance.StopGameMusic();
            foreach (Player player in this.players)
            {
                if (player.GetTeam() == winningTeam)
                    winners.Add(player);
            }
            GameManager.Instance.EndRound(winners);
        }
	}
    public void SetupPlayersInEndLevel(float freezeForSeconds) {
        SetPlayerPositionsOnPodium();
        foreach (Player player in players)
        {
            player.gameObject.SetActive(true);
			player.SetIsDead(false);
            player.SetDamage(1);
            player.UnequipAllWeapons();
            player.ResetAbilities();
            player.SetCanUseAbility(false);
            player.Unfreeze();
            player.UnfreezeInputs();
            player.FreezeInputsFor(freezeForSeconds);
        }
    }

    public List<Player> GetAllPlayers()
    {
        return this.players;
    }

    private void OnPlayerJoined(PlayerInput playerInput)
    {
		CharacterSelectionManager.Instance.JoinPlayer(playerInput);
    }

    private void SetPlayerPositions()
	{
		SpawnPointManager spawnPointManager = FindFirstObjectByType<SpawnPointManager>();

		for (int i = 0; i < players.Count; i++) {
			players[i].transform.position = spawnPointManager.GetSpawnPoint(i);
		}
	}

	public List<Player> GetSortedPlayers() {
        return players.OrderByDescending(obj => obj.GetPoints()).ToList();
    }

    public List<int> GetScoresForPodium()
    {
		List<Player> sortedPlayers = GetSortedPlayers();

        List<int> scores = new List<int>();
        for (int i = 0; i < sortedPlayers.Count; i++)
            if (!scores.Contains(sortedPlayers[i].GetPoints()))
                scores.Add(sortedPlayers[i].GetPoints());

		return scores;
    }


    private void SetPlayerPositionsOnPodium()
	{
		SpawnPointManager spawnPointManager = FindFirstObjectByType<SpawnPointManager>();
		List<int> scores = GetScoresForPodium();

		for (int i = 0; i < players.Count; i++)
		{
			int spawnIndex = scores.FindIndex(x => x == players[i].GetPoints());
			players[i].transform.position = spawnPointManager.GetSpawnPoint(spawnIndex);
		}
	}

	private void UpdateMusicSpeed()
	{
		float ratio = (float) players.Count(p => p.GetIsDead()) / (float)players.Count;
		MusicManager.Instance.SetMusicPlaybackSpeed(Mathf.Lerp(1, 1.3f, ratio));
	}

    private IEnumerator PreparePlayerForRound(Player player)
	{
        player.gameObject.SetActive(true);
        player.SetIsDead(false);
        player.SetDamage(1);
        player.UnequipAllWeapons();
		player.ResetAbilities();
        player.Unfreeze();
        player.UnfreezeInputs();

        yield return new WaitForSeconds(0.6f);

        player.ShowInGameHUD();
    }

}
