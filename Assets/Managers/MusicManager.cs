using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting.Antlr3.Runtime.Tree;
using UnityEngine;
using UnityEngine.Timeline;

public class MusicManager : MonoBehaviour
{
    [SerializeField] private float fadeDuration;
    [SerializeField] private float transitionPitchSpeed = .5f;
    
    private AudioSource musicSource;
    private float musicVolume;
    private float targetPitch = 1;

    private static MusicManager instance = null;
    public static MusicManager Instance { get { return instance; } }

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(instance);
        }
        else if (instance != this)
            Destroy(gameObject);
    }


    void Start()
    {
        musicSource = GetComponent<AudioSource>();
        UpdateMusicVolume();
    }

	private void Update()
	{
        if (musicSource.pitch != targetPitch) musicSource.pitch = Mathf.MoveTowards(musicSource.pitch, targetPitch, transitionPitchSpeed);
	}

	public void UpdateMusicVolume()
    {
        musicVolume = SoundManager.Instance.GetMusicVolume();
        musicSource.volume = musicVolume;
    }

    public void PlayMainMusic()
    {
        StartCoroutine(PlayMusic(SoundManager.Instance.mainMenuMusicClip));
    }

    public void PlayIntroMusic()
    {
        StartCoroutine(PlayMusic(SoundManager.Instance.introMusicClip));
    }

    public void PlayEndMusic()
    {
        StartCoroutine(PlayMusic(SoundManager.Instance.endMusicClip));
    }

    public void PlayGameMusic(string theme)
    {
		StartCoroutine(PlayMusic(SoundManager.Instance.GetMusicByTheme(theme)));
	}
	public void StopGameMusic()
    {
		StartCoroutine(PlayMusic(null));
	}

    public void SetMusicPlaybackSpeed(float playbackSpeed)
    {
        targetPitch = playbackSpeed;
    }

    public IEnumerator PlayMusic(AudioClip nextMusic)
    {
        if (musicSource.clip != nextMusic)
        {
			AudioSource nextSource = gameObject.AddComponent<AudioSource>();
			nextSource.clip = nextMusic;
			nextSource.loop = true;
			nextSource.volume = 0f;
			nextSource.Play();

			float timer = 0f;

			while (timer < fadeDuration)
			{
				float normalizedTime = timer / fadeDuration;
				musicSource.volume = Mathf.Lerp(musicVolume, 0f, normalizedTime);
				nextSource.volume = Mathf.Lerp(0f, musicVolume, normalizedTime);
				timer += Time.deltaTime;
				yield return null;
			}

			// Ensure volumes are set correctly at the end of the crossfade
			musicSource.volume = 0f;
			nextSource.volume = musicVolume;

			// Swap the music sources
			Destroy(musicSource);
			musicSource = nextSource;
            musicSource.pitch = 1;
		}
        targetPitch = 1;
    }
}
