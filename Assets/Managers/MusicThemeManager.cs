using UnityEngine;

public class MusicThemeManager : MonoBehaviour
{
    public string themeName; 
    void Start()
    {
        MusicManager.Instance.PlayGameMusic(themeName);
    }

}
