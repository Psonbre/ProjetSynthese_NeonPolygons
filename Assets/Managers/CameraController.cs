using Harmony;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.PlayerLoop;
using UnityEngine.UIElements;

public class CameraController : MonoBehaviour
{
    [SerializeField] private float panningSpeed = 1.0f;
    [SerializeField] private float zoomingSpeed = 1.0f;
    [SerializeField] private float minimumPadding = 50f;
    [SerializeField] private float maximumWorldPadding = 100f;
    [SerializeField] private float offstagePadding = 100f;
    [SerializeField] private float minimumPortalPadding = 100f;

    [SerializeField] private float minimumWinningPadding = 20;
    [SerializeField] private float maximumWinningWorldPadding = 1000;

    private bool isWinningCamera = false;

    private float initialWidth = 0;
    private float initialHeight = 0;
    private Camera cam;

    private void Start()
    {
        cam = GetComponent<Camera>();
        if (cam) {
            Vector2 bottomLeft = cam.BottomLeftWorldPoint();
            Vector2 topRight = cam.TopRightWorldPoint();

            this.initialWidth = topRight.x - bottomLeft.x;
            this.initialHeight = topRight.y - bottomLeft.y;
        }
    }
    void Update()
    {
        float padding;
        float worldPadding;
        float portalPadding;

        if (!isWinningCamera) {
            padding = this.minimumPadding;
            worldPadding = this.maximumWorldPadding;
            portalPadding = this.minimumPortalPadding;
        }
        else {
            padding = this.minimumWinningPadding;
            worldPadding = this.maximumWinningWorldPadding;
            portalPadding = 0;
        }

        float minimumWidth = initialWidth - worldPadding * 2;
        float minimumHeight = initialHeight - worldPadding * 2;

        if (MultiplayerManager.Instance)
        {
            List<Player> players = MultiplayerManager.Instance.GetAllPlayers().Where(p => p != null && !p.GetIsDead()).ToList();
            List<PortalController> portals = new List<PortalController>();

            foreach (Player player in players) {
                Ability activatedAbility = player.GetActivatedAbility();
                if (activatedAbility.GetType().Equals(typeof(Portal)))
                {
                    PortalController firstPortal = activatedAbility.GetComponent<Portal>().GetFirstPortal();
                    PortalController secondPortal = activatedAbility.GetComponent<Portal>().GetSecondPortal();

                    if (firstPortal && firstPortal.gameObject.activeSelf) portals.Add(firstPortal);
                    if (secondPortal && secondPortal.gameObject.activeSelf) portals.Add(secondPortal);
                }
            }

            if (players.Count(p => !p.GetIsDead()) > 0)
            {
                // Positions max/min des joueurs/portails en prenant en compte les padding minimum et maximum.
                float minX = int.MaxValue;
                if (players.Count > 0) minX = Mathf.Min(minX, players.Min(p => p.transform.position.x - padding));
                if (portals.Count > 0 && !isWinningCamera) minX = Mathf.Min(minX, portals.Min(p => p.transform.position.x - portalPadding));
                minX = Mathf.Max(minX, -initialWidth / 2f - offstagePadding);
                minX = Mathf.Min(minX, -initialWidth / 2f + worldPadding);

                float maxX = int.MinValue;
                if (players.Count > 0) maxX = Mathf.Max(maxX, players.Max(p => p.transform.position.x + padding));
                if (portals.Count > 0 && !isWinningCamera) maxX = Mathf.Max(maxX, portals.Max(p => p.transform.position.x + portalPadding));
                maxX = Mathf.Min(maxX, initialWidth / 2f + offstagePadding);
                maxX = Mathf.Max(maxX, initialWidth / 2f - worldPadding);

                float minY = int.MaxValue;
                if (players.Count > 0) minY = Mathf.Min(minY, players.Min(p => p.transform.position.y - padding));
                if (portals.Count > 0 && !isWinningCamera) minY = Mathf.Min(minY, portals.Min(p => p.transform.position.y - portalPadding));
                minY = Mathf.Max(minY, -initialHeight / 2f - offstagePadding);
                minY = Mathf.Min(minY, -initialHeight / 2f + worldPadding);

                float maxY = int.MinValue;
                if (players.Count > 0) maxY = Mathf.Max(maxY, players.Max(p => p.transform.position.y + padding));
                if (portals.Count > 0 && !isWinningCamera) maxY = Mathf.Max(maxY, portals.Max(p => p.transform.position.y + portalPadding));
                maxY = Mathf.Min(maxY, initialHeight / 2f + offstagePadding);
                maxY = Mathf.Max(maxY, initialHeight / 2f - worldPadding);

                float distanceX = Mathf.Max(maxX - minX, minimumWidth);
                float distanceY = Mathf.Max(maxY - minY, minimumHeight);

                float currentRatio = distanceX / distanceY;
                float goalRatio = initialWidth / initialHeight;

                if (currentRatio > goalRatio)
                {
                    // Augmente y pour respecter le ratio
                    float mult = (distanceX * initialHeight) / (distanceY * initialWidth);
                    distanceY *= mult;
                }
                else if (currentRatio < goalRatio)
                {
                    // Augmente x pour respecter le ratio
                    float mult = (distanceY * initialWidth) / (distanceX * initialHeight);
                    distanceX *= mult;
                }

                Vector2 middlePoint = new((minX + maxX) / 2f, (minY + maxY) / 2f);

                float distanceToTarget = Vector2.Distance(transform.position, middlePoint) / 150f;

                float targetSize = distanceY / 2f;
                cam.orthographicSize = Mathf.MoveTowards(cam.orthographicSize, targetSize, Mathf.Abs(zoomingSpeed * (targetSize - cam.orthographicSize) / 100f));
                transform.position = new(Mathf.MoveTowards(transform.position.x, middlePoint.x, panningSpeed * distanceToTarget), Mathf.MoveTowards(transform.position.y, middlePoint.y, panningSpeed * distanceToTarget));
            }
        }
    }

    public void SetWinningState(bool state) {
        this.isWinningCamera = state;
    }
}
