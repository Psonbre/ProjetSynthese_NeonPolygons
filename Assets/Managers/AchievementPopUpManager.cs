using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SocialPlatforms.Impl;
using UnityEngine.UI;

public class AchievementPopUpManager : MonoBehaviour
{
    [SerializeField] private float achievementPopUpDuration;
    [SerializeField] private float movingSpeed;
	[SerializeField] private RectTransform popUp;
    [SerializeField] private string unlockableSkinPath = "UnlockableSkins";
    [SerializeField] private float distanceBeforeStop = .01f;

    private Queue<string> achievementsToShow = new Queue<string>();
    private bool isShowingAchievement = false;
	private Vector2 hiddenPosition;

    public static AchievementPopUpManager Instance { get; private set; }

	private void Start()
	{
        if (Instance == null)
        {
            Instance = this;
			DontDestroyOnLoad(gameObject);
			hiddenPosition = popUp.anchoredPosition;
		}
        else Destroy(gameObject);

	}
	public void ShowAchievement(string slug)
    {
        achievementsToShow.Enqueue(slug);
    }

    void Update()
    {
        if(achievementsToShow.Count > 0 && !isShowingAchievement)
        {
            string slug = achievementsToShow.Dequeue();
			StartCoroutine(ShowAchievementCouroutine(slug));
            
        }
    }

    private IEnumerator ShowAchievementCouroutine(string slug)
    {
        Achievement achievement = Achievements.achievements[slug];

        popUp.GetComponentInChildren<TextMeshProUGUI>().text = achievement.GetTitle();
        popUp.transform.Find(Harmony.GameObjects.Logo).GetComponent<Image>().sprite =  Resources.Load<Sprite>(unlockableSkinPath  + "/" + slug);

        isShowingAchievement = true;

        popUp.anchoredPosition = hiddenPosition;

        while (Vector2.Distance(popUp.anchoredPosition, Vector2.zero) > distanceBeforeStop)
        {
			popUp.anchoredPosition = Vector2.MoveTowards(popUp.anchoredPosition, Vector2.zero, movingSpeed * Time.deltaTime);
            yield return null;
        }

        yield return new WaitForSeconds(achievementPopUpDuration);

        while (Vector2.Distance(popUp.anchoredPosition, hiddenPosition) > distanceBeforeStop)
        {
			popUp.anchoredPosition = Vector2.MoveTowards(popUp.anchoredPosition, hiddenPosition, movingSpeed * Time.deltaTime);
            yield return null;
        }

		popUp.anchoredPosition = hiddenPosition;

        isShowingAchievement = false;
    }
}
