using NUnit.Framework.Internal;
using UnityEngine;

public class DebugManager : MonoBehaviour
{
    [SerializeField] private GameObject currentMap;
    [SerializeField] private GameObject mapPrefab;
	[SerializeField] private float switchTextDuration = 2;
    public static bool useJobs = false;
	private Camera cam;

	private void Start()
	{
		cam = Camera.main;
	}
	void Update()
    {
        if (Input.GetKeyDown(KeyCode.R)) 
        {
            Destroy(currentMap);
            currentMap = Instantiate(mapPrefab);
		}

        if (Input.GetKeyDown(KeyCode.J)) useJobs = !useJobs;

        if (Input.GetKeyDown(KeyCode.L)) GameUI.Instance.ShowSwitchText(switchTextDuration);

        if (Input.GetKeyDown(KeyCode.X))
		{
			GameObject explosion = PoolManager.Instance.Get(Harmony.PoolTags.explosion);
			Vector2 position = cam.ScreenToWorldPoint(Input.mousePosition);
			explosion.transform.position = position;
			explosion.SetActive(true);
			explosion.GetComponent<Explosion>().Explode(200, new Vector2(0, 0));
		}

		if(Input.GetKeyDown(KeyCode.Y))
		{
			AchievementPopUpManager manager = FindAnyObjectByType<AchievementPopUpManager>();
			manager.ShowAchievement(Harmony.Achievements._4K);
		}
	}
}
