using System.Collections;
using UnityEngine;
using TMPro;

public class TimerManager : MonoBehaviour
{
    private const float SECONDS_IN_MINUTE = 60f;
    private TextMeshProUGUI timerText;
    private float time = 0f;

    private void Awake()
    {
        timerText = GetComponent<TextMeshProUGUI>();
    }

    public void StartTimer()
    {
        StartCoroutine(Timer());
    }

    public void StopTimer()
    {
        StopCoroutine(Timer());
    }

    public float GetTime()
    {
        return time;
    }

    private IEnumerator Timer()
    {
        while (true)
        {
            time += Time.deltaTime;

            UpdateTimerText();

            yield return null;
        }
    }

    private void UpdateTimerText()
    {
        int minutes = Mathf.FloorToInt(time / SECONDS_IN_MINUTE);
        int seconds = Mathf.FloorToInt(time % SECONDS_IN_MINUTE);
        string timeString = string.Format("{0:00}:{1:00}", minutes, seconds);
        
        timerText.text = timeString;
    }
}