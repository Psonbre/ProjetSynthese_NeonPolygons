using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class WeaponSwitchManager : MonoBehaviour
{
    public static WeaponSwitchManager Instance {  get; private set; }

    [Header("Countdown values")]
    [SerializeField] private float weaponSwitchingInterval = 15f;
    [SerializeField] private float timeLeftForCountdownStart = 2f;
    [SerializeField] private float startCountdownAt = 3f;

    [Header("Sound")]
    [SerializeField] List<AudioClip> countdownSounds;
    [SerializeField] float countdownVolume;

    private readonly System.Type FIRST_PLACE_WEAPON = typeof(RocketLauncher);
    private readonly System.Type SECOND_PLACE_WEAPON = typeof(Shotgun);
    private readonly System.Type THIRD_PLACE_WEAPON = typeof(Ak47);
    private readonly System.Type PODIUMLESS_WEAPON = typeof(Handgun);

    private bool isWeaponSwitchingActive = false;
    private float timeUntilNextWeaponSwitch = 0;
    private float lastCountdownNb = 0;

    private GameManager gameManager;
    private MultiplayerManager multiplayerManager;

    private void Awake()
    {

        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        gameManager = GameManager.Instance;
        multiplayerManager = MultiplayerManager.Instance;
    }

    void Update()
    {
        if (isWeaponSwitchingActive)
        {
            timeUntilNextWeaponSwitch -= Time.deltaTime;
            if (timeUntilNextWeaponSwitch <= timeLeftForCountdownStart && !gameManager.HasRoundEnded())
            {
                
                GameUI.Instance.ShrinkWeaponSwitchCountdown();
                if (Mathf.Ceil((timeUntilNextWeaponSwitch / timeLeftForCountdownStart) * startCountdownAt) != lastCountdownNb)
                {
                    GameUI.Instance.ShowSwitchCooldownText(Mathf.Ceil((timeUntilNextWeaponSwitch / timeLeftForCountdownStart) * startCountdownAt));
                    GameUI.Instance.GrowWeaponSwitchCountdown();
                    SoundManager.Instance.PlayRandomSoundEffect(countdownSounds, countdownVolume);
                    lastCountdownNb = Mathf.Ceil((timeUntilNextWeaponSwitch / timeLeftForCountdownStart) * startCountdownAt);
                }
            }
            if (timeUntilNextWeaponSwitch < 0 && !gameManager.HasRoundEnded())
            {
                GameUI.Instance.HideSwitchCooldownText();
                timeUntilNextWeaponSwitch = weaponSwitchingInterval;
                GameUI.Instance.ShowSwitchText(1f);

                multiplayerManager.UnfreezeAllLivingPlayers();
                multiplayerManager.GetAllLivingPlayers().ForEach(p => p.EquipRandomWeapon());
            }
        }
        if (gameManager.HasRoundEnded())
        {
            timeUntilNextWeaponSwitch = 0;
            lastCountdownNb = 0;
            GameUI.Instance.HideSwitchCooldownText();
        }
    }


    public void ActivateWeaponSwitchWithDelay(float time)
    {
        Invoke("ActivateWeaponSwitch", time);
    }

    public void DeactivateWeaponSwitch()
    {
        timeUntilNextWeaponSwitch = weaponSwitchingInterval;
        isWeaponSwitchingActive = false;
    }

    public void EquipFinalRoundWeapons(List<Player> sortedPlayers, List<int> scores)
    {
        for (int i = 0; i < sortedPlayers.Count; i++)
        {
            switch (scores.FindIndex(x => x == sortedPlayers[i].GetPoints()))
            {
                case 0:
                    sortedPlayers[i].EquipWeaponByType(FIRST_PLACE_WEAPON);
                    break;
                case 1:
                    sortedPlayers[i].EquipWeaponByType(SECOND_PLACE_WEAPON);
                    break;
                case 2:
                    sortedPlayers[i].EquipWeaponByType(THIRD_PLACE_WEAPON);
                    break;
                default:
                    sortedPlayers[i].EquipWeaponByType(PODIUMLESS_WEAPON);
                    break;
            }

        }
    }

    private void ActivateWeaponSwitch()
    {
        isWeaponSwitchingActive = true;
    }
}
