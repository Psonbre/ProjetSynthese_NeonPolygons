using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance { get; private set; }
    public static readonly int nbOfPointsToWinGame = 5;

    [SerializeField] private CameraController cam;
    [SerializeField] private AudioClip winRoundJingle;
    [SerializeField] private GameObject finalLevelPrefab;

    [Header("Weapon switch activations")]
    [SerializeField] private float timeBeforeActivatingWeaponSwitch = 1.5f;
    [SerializeField] private float timeBeforeGiveWeaponFinalRound = 3f;

    private MultiplayerManager multiplayerManager;

    private Banner[] banners;

    private List<GameObject> availableLevels;
    private List<GameObject> usedLevels = new();
    private GameObject currentLevel;
    private GameObject previousLevel;

    private float timeSinceRoundStarted = 0;
    private bool roundEnded = false;
    private bool isLastLevel = false;

    private void Awake()
    {

        if (Instance == null)
        {
            Instance = this;
            availableLevels = Resources.LoadAll<GameObject>(Harmony.GameObjects.Levels).ToList();
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }
    }

    void Start()
    {
        multiplayerManager = MultiplayerManager.Instance;
        StartFirstRound();
    }

    public void EndRound(List<Player> winners)
    {
        if (isLastLevel) StartCoroutine(LoadCharacterSelectScene());
        else StartCoroutine(EndRoundCoroutine(winners));

    }

    public bool HasRoundEnded()
    {
        return roundEnded;
    }

    public int GetNbOfPointsToWin()
    {
        return nbOfPointsToWinGame;
    }

    private void StartFirstRound()
    {
        SetupGameUI();
        SetupScoreUI();
        LoadRandomLevel();
        GetBanners()[1].OpenFully(0.2f);
        StartCoroutine(InGameTimer());
        WeaponSwitchManager.Instance.ActivateWeaponSwitchWithDelay(timeBeforeActivatingWeaponSwitch);

        //Achievement 8Players
        if (multiplayerManager.GetAllPlayers().Count() == 8)
        {
            Achievements.Unlock(Harmony.Achievements._8Players);
        }
    }
    private void LoadRandomLevel()
    {
        WeaponSwitchManager.Instance.ActivateWeaponSwitchWithDelay(timeBeforeActivatingWeaponSwitch);
        if (cam) cam.transform.eulerAngles = Vector3.zero;

        timeSinceRoundStarted = 0f;
        roundEnded = false;

        if (currentLevel != null) Destroy(currentLevel);

        if (availableLevels.Count <= 0)
        {
            availableLevels.AddRange(usedLevels);
            usedLevels.Clear();

            //Emp�che le m�me niveau de loader deux fois de suite au refill de availableLevels
            availableLevels.Remove(previousLevel);
            usedLevels.Add(previousLevel);
        }

        GameObject nextLevel = availableLevels[Random.Range(0, availableLevels.Count)];
        availableLevels.Remove(nextLevel);
        usedLevels.Add(nextLevel);
        previousLevel = nextLevel;
        currentLevel = Instantiate(nextLevel);

        multiplayerManager.PrepareAllPlayersForNextRound();
    }

    private void SetupGameUI()
    {
        GameUI gameUI = FindAnyObjectByType(typeof(GameUI)).GetComponent<GameUI>();
        gameUI.CreateUI();
    }

    private void SetupScoreUI()
    {
        ScoreUI scoreUI = FindAnyObjectByType(typeof(ScoreUI)).GetComponent<ScoreUI>();
        scoreUI.CreateUI();
    }

    private IEnumerator InGameTimer()
    {
        while (true)
        {
            timeSinceRoundStarted += Time.deltaTime;

            yield return null;
        }
    }
    private Banner[] GetBanners()
    {
        if (banners == null || banners.Length <= 0)
            return FindObjectsByType<Banner>(FindObjectsSortMode.InstanceID);
        return banners;
    }

    private IEnumerator EndRoundCoroutine(List<Player> winners)
    {
        if (!roundEnded)
        {
			roundEnded = true;

			StopCoroutine(InGameTimer());
			WeaponSwitchManager.Instance.DeactivateWeaponSwitch();

			List<Player> players = multiplayerManager.GetAllPlayers();

			//Stats et achievements
			Statistics.IncrementStat(Harmony.Stats.time, (int)timeSinceRoundStarted);
			if (timeSinceRoundStarted > Statistics.GetValue(Harmony.Stats.longestRound) || Statistics.GetValue(Harmony.Stats.longestRound) == 0) Statistics.SetStat(Harmony.Stats.longestRound, (int)timeSinceRoundStarted);
			if (timeSinceRoundStarted < Statistics.GetValue(Harmony.Stats.shortestRound) || Statistics.GetValue(Harmony.Stats.shortestRound) == 0) Statistics.SetStat(Harmony.Stats.shortestRound, (int)timeSinceRoundStarted);
			if (timeSinceRoundStarted <= Achievements.achievements[Harmony.Achievements.fastGame].GetTotalProgress()) Achievements.Unlock(Harmony.Achievements.fastGame);
			Statistics.IncrementStat(Harmony.Stats.rounds, 1);

			CheckForAnihilationAchievement();

			winners.ForEach(w => w.IncrementPoints(1));
			ScoreUI.Instance.UpdateScores();

			//Hide des ui des joueurs
			players.ForEach(p => p.HideInGameHUD());

			cam.SetWinningState(true);

			//G�re les banni�res
			GetBanners()[0].CloseHalfWay(1);
			SoundManager.Instance.PlaySoundEffect(winRoundJingle, 2);

			yield return new WaitForSeconds(2);

			GetBanners()[0].CloseFully(0.5f);

			yield return new WaitForSeconds(1.5f);

			if (winners.Count > 0 && winners[0].GetPoints() >= nbOfPointsToWinGame)
			{
				//Stat games
				Statistics.IncrementStat(Harmony.Stats.games, 1);

				GetBanners()[1].CloseFully(1f);
				yield return new WaitForSeconds(1.5f);
			}

			cam.SetWinningState(false);

			// On clean le PoolManager
			PoolManager.Instance.DisableAll();
			MultiplayerManager.Instance.DeactivateAllPlayers();

			SaveSystem.Instance.SaveStats();

			if (winners.Count > 0 && winners[0].GetPoints() >= nbOfPointsToWinGame)
			{
				LoadEndLevel();

				GetBanners()[0].OpenFully(1);
				GetBanners()[1].OpenFully(1);

                MusicManager.Instance.PlayEndMusic();

                yield return new WaitForSeconds(1);

                winners.ForEach(p => p.SetCrown(true));
                multiplayerManager.SetupPlayersInEndLevel(timeBeforeGiveWeaponFinalRound);

				yield return new WaitForSeconds(timeBeforeGiveWeaponFinalRound);

                roundEnded = false;

                List<Player> sortedPlayers = MultiplayerManager.Instance.GetSortedPlayers();
                List<int> scores = MultiplayerManager.Instance.GetScoresForPodium();
                WeaponSwitchManager.Instance.EquipFinalRoundWeapons(sortedPlayers, scores);

				Achievements.Unlock(Harmony.Achievements.endGame);
				if (players.Find(p => p.GetDeaths() == 0) != null) Achievements.Unlock(Harmony.Achievements.hacker);
			}
			else
			{
				// On load une map al�atoire
				LoadRandomLevel();
				yield return new WaitForSeconds(0.5f);
				GetBanners()[0].OpenFully(0.5f);
			}
		}
    }


	private void CheckForAnihilationAchievement()
    {
        if (currentLevel)
        {
            List<DestroyablePlatform> platforms = currentLevel.transform.Find(Harmony.GameObjects.Platforms).GetComponentsInChildren<DestroyablePlatform>().ToList();
            int nbOfPixels = 0;
            foreach (DestroyablePlatform platform in platforms)
            {
                if (platform.transform.position.y > currentLevel.transform.Find(Harmony.GameObjects.KillLines).Find(Harmony.GameObjects.DownLine).position.y)
                {
                    nbOfPixels += platform.GetPixelCount();
                }
            }
            if (nbOfPixels <= 0) Achievements.Unlock(Harmony.Achievements.levelDestroyed);
        }
    }

    private IEnumerator LoadCharacterSelectScene()
    {
        GetBanners()[1].CloseFully(1);
        yield return new WaitForSeconds(1.5f);
        MultiplayerManager.Instance.EmptyDontDestroyOnLoad();
        MultiplayerManager.Instance.TryResetAllVibrations();
        MusicManager.Instance.PlayMainMusic();
        SceneManager.LoadScene(Harmony.Scenes.CharacterSelectScene);
    }

    private void LoadEndLevel()
    {
        multiplayerManager.GetAllPlayers().ForEach(p => p.SetCrown(false));
        cam.transform.position = Vector2.zero;
        cam.GetComponent<Camera>().orthographicSize = 100;
        Destroy(cam);
        if (currentLevel) Destroy(currentLevel);
        currentLevel = Instantiate(finalLevelPrefab);
        isLastLevel = true;
    }

}
