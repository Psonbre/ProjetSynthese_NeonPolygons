using System.Collections.Generic;
using UnityEngine;

public class SpawnPointManager : MonoBehaviour
{
    [SerializeField] private List<GameObject> spawnPoints;

    public Vector2 GetSpawnPoint(int spawnIndex)
    {
        return spawnPoints[spawnIndex].transform.position;
    }
}
