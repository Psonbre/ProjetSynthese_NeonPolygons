using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

[System.Serializable]
public class Pool
{
	public string tag;
	public GameObject prefab;
	public int size;
}

public class PoolManager : MonoBehaviour
{
	public static PoolManager Instance;

	public List<Pool> pools;
	private Dictionary<string, Queue<GameObject>> poolDictionary;
	private Dictionary<string, GameObject> prefabDictionary;
	private List<GameObject> allObjects;

	private void Awake()
	{
		Instance = this;

		poolDictionary = new Dictionary<string, Queue<GameObject>>();
		prefabDictionary = new Dictionary<string, GameObject>();
		allObjects = new List<GameObject>();


        foreach (Pool pool in pools)
		{
			Queue<GameObject> objectPool = new Queue<GameObject>();

			for (int i = 0; i < pool.size; i++)
			{
				GameObject obj = Instantiate(pool.prefab);
				obj.transform.parent = transform;
				obj.SetActive(false);
				allObjects.Add(obj);
				objectPool.Enqueue(obj);
			}

			poolDictionary.Add(pool.tag, objectPool);
			prefabDictionary.Add(pool.tag, pool.prefab);
		}
	}

	public GameObject Get(string tag)
	{
		if (!poolDictionary.ContainsKey(tag))
		{
			return null;
		}

		Queue<GameObject> poolQueue = poolDictionary[tag];
		GameObject objectToSpawn;

		if (poolQueue.Count > 0)
		{
			objectToSpawn = poolQueue.Dequeue();
		}
		else
		{
			objectToSpawn = Instantiate(prefabDictionary[tag]);
			allObjects.Add(objectToSpawn);
            objectToSpawn.transform.parent = transform;
		}

		return objectToSpawn;
	}

	public void ReturnToPool(string tag, GameObject objectToReturn)
	{
		if (!poolDictionary.ContainsKey(tag))
		{
			return;
		}

        objectToReturn.SetActive(false);
		poolDictionary[tag].Enqueue(objectToReturn);
	}

	public void DisableAll()
	{
		for (int i = 0; i < allObjects.Count; i++)
		{
			if (!allObjects[i].GetComponent<PlayerInput>())
                allObjects[i].transform.parent = this.transform;
		}

		for (int i = 0; i < this.gameObject.transform.childCount; i++)
		{
			Transform child = transform.GetChild(i);

            if (!child.GetComponent<PlayerInput>() && child.name != Harmony.GameObjects.WinCrown)
				child.gameObject.SetActive(false);
		}
	}

}
