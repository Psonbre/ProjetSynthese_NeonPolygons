using TMPro;
using UnityEngine;

public class FPSCounter : MonoBehaviour
{
    TextMeshProUGUI text;
	private void Awake()
	{
		text = GetComponent<TextMeshProUGUI>();
	}
	void Update()
    {
		text.text = "FPS :" + Mathf.Round((1 / Time.deltaTime));
    }
}
