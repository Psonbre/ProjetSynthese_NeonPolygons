using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class MovingPlatform : MonoBehaviour
{
    [SerializeField] private float speed = 100f;
    [SerializeField] private bool goClockwise = true;
    [SerializeField] private float distanceBeforeSwitchPoint = 10f;
    [SerializeField] private LayerMask groundLayer;
    [SerializeField] private float groundCheckDistance = 3f;
    [SerializeField] private bool shouldUseCirclePath = false;
    [SerializeField] private float radius = 200;
    [SerializeField] private float maxTimeBeforeMoveAgain = 1f;
    private DestroyablePlatform platform;
    private float timeBeforeMoveAgain = 0f;


    private int currentPathIndex = 0;
    private List<GameObject> objectsOnPlatform;

    public Vector2 circlePoint;
    public List<Vector2> path;

    private Rigidbody2D rb;
    private SpringJoint2D joint;
    public float Speed => speed;
    public bool GoClockwise => goClockwise;
    public float DistanceBeforeSwitchPoint => distanceBeforeSwitchPoint;
    public bool ShouldUseCirclePath => shouldUseCirclePath;
    public float Radius => radius;

    private void OnCollisionStay2D(Collision2D collision)
    {
        if (!(collision.gameObject.CompareTag(Harmony.Tags.Ground) && collision.gameObject.GetComponent<SpringJoint2D>() && collision.gameObject.GetComponent<SpringJoint2D>().enabled) && !this.objectsOnPlatform.Contains(collision.gameObject))
            this.objectsOnPlatform.Add(collision.gameObject);
        else if (collision.gameObject.CompareTag(Harmony.Tags.Ground) && collision.gameObject.GetComponent<SpringJoint2D>() && collision.gameObject.GetComponent<SpringJoint2D>().enabled)
            timeBeforeMoveAgain = maxTimeBeforeMoveAgain;
    }



    void Start()
    {
        this.objectsOnPlatform = new List<GameObject>();
        this.rb = GetComponent<Rigidbody2D>();
        this.joint = GetComponent<SpringJoint2D>();
        this.currentPathIndex = GetClosestPointIndex();
    }

    private int GetClosestPointIndex() {
        float smallestDistance = int.MaxValue;
        int closestIndex = 0;
        for (int i = 0; i < path.Count; i++)
        {
            float distance = Vector2.Distance(this.transform.position, path[i]);
            if (distance < smallestDistance)
            {
                smallestDistance = distance;
                closestIndex = i;
            }
        }
        return closestIndex;
    }

    private int GetNextIndex() {
        int nextIndex = currentPathIndex + 1;
        if (nextIndex > path.Count - 1)
            nextIndex = 0;
        return nextIndex;
    }

    private int GetPreviousIndex()
    {
        int previousIndex = currentPathIndex - 1;
        if (previousIndex < 0)
            previousIndex = path.Count - 1;
        return previousIndex;
    }

    private void MovePlatformTowards(Vector2 point) {
        
        if (!joint)
            this.joint = GetComponent<SpringJoint2D>();
        if (rb && joint)
        {
            Vector2 direction = (point - (Vector2)this.transform.position).normalized * this.speed * Time.deltaTime;
            transform.position += (Vector3)direction;
            joint.connectedAnchor += direction;

            foreach (GameObject obj in objectsOnPlatform)
            {
                if (!obj)
                    objectsOnPlatform.Remove(obj);
                else
                    obj.transform.position += (Vector3)direction;
            }
        }
        else {
            if (this.objectsOnPlatform.Count > 0)
                this.objectsOnPlatform = this.objectsOnPlatform = new List<GameObject>();
        }
    }

    private Vector2 CalculateCirclePathPosition(float angle)
    {
        float x = circlePoint.x + radius * Mathf.Cos(angle);
        float y = circlePoint.y + radius * Mathf.Sin(angle);
        return new Vector2(x, y);
    }

    void Update()
    {
        if (!platform)
            platform = GetComponent<DestroyablePlatform>();

        if (timeBeforeMoveAgain > 0f)
            this.timeBeforeMoveAgain -= Time.deltaTime;


        List<GameObject> objectsToRemove = new List<GameObject>();
        foreach (GameObject obj in objectsOnPlatform)
        {
            if (obj)
            {
                RaycastHit2D[] hits = Physics2D.RaycastAll(obj.transform.position, Vector2.down, groundCheckDistance, groundLayer);
                bool hitPlatform = false;
                foreach (RaycastHit2D hit in hits)
                {
                    if (hit.collider.gameObject.Equals(this.gameObject))
                    {
                        hitPlatform = true;
                        break;
                    }
                }
                if (!hitPlatform)
                {
                    objectsToRemove.Add(obj);
                }
            }
            else {
                objectsToRemove.Add(obj);
            }
        }

        foreach (GameObject obj in objectsToRemove) {
            objectsOnPlatform.Remove(obj);
        }


        if (timeBeforeMoveAgain <= 0f && platform && !platform.GetIsLocked())
        {
            if (shouldUseCirclePath)
            {
                float angle = Mathf.Atan2(transform.position.y - circlePoint.y, transform.position.x - circlePoint.x);

                Vector2 destination = CalculateCirclePathPosition(angle);

                float distance = Vector2.Distance(this.transform.position, destination);
                if (distance <= distanceBeforeSwitchPoint)
                {
                    float circumference = 2f * Mathf.PI * radius;
                    float angularVelocity = (speed * Time.deltaTime) / circumference;
                    if (!goClockwise)
                        angularVelocity *= -1;
                    angle += angularVelocity * 2f * Mathf.PI;
                    angle = (angle + 2f * Mathf.PI) % (2f * Mathf.PI);
                }

                Vector2 targetPosition = CalculateCirclePathPosition(angle);
                MovePlatformTowards(targetPosition);

            }
            else
            {
                float distance = Vector2.Distance(this.transform.position, path[currentPathIndex]);
                if (distance <= distanceBeforeSwitchPoint)
                {
                    if (goClockwise)
                        this.currentPathIndex = GetNextIndex();
                    else
                        this.currentPathIndex = GetPreviousIndex();
                }
                MovePlatformTowards(path[currentPathIndex]);
            }
        }
    }
}
