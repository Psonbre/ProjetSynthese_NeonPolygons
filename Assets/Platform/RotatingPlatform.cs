using UnityEngine;

public class RotatingPlatform : MonoBehaviour
{
    [SerializeField] private float spinSpeed = 1f;
    [SerializeField] private bool clockwise = true;
    private DestroyablePlatform platform;
    private SpringJoint2D joint;

    void Update()
    {
        if (!platform)
            platform = GetComponent<DestroyablePlatform>();

        if (!joint)
            this.joint = GetComponent<SpringJoint2D>();

        if (joint && platform && !platform.GetIsLocked()) {
            float rotation = this.transform.rotation.eulerAngles.z * Mathf.Deg2Rad;
            float rotationOffset = spinSpeed * Time.deltaTime;
            if (!clockwise)
                rotationOffset *= -1;
            rotation += rotationOffset;
            rotation = (rotation + 2 * Mathf.PI) % (2 * Mathf.PI);
            this.transform.rotation = Quaternion.Euler(0, 0, rotation * Mathf.Rad2Deg);
        }
    }
}
