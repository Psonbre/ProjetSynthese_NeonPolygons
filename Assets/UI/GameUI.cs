using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GameUI : MonoBehaviour
{
    [SerializeField] private PlayerUI playerUIPrefab;
    [SerializeField] private int numPlayersBeforeRescale = 5;
    [SerializeField] private float rescaleMultLow = .3f;
    [SerializeField] private float rescaleMultHigh = .2f;
    [SerializeField] private RectTransform switchText;
    [SerializeField] private RectTransform cooldownSwitchText;
    [SerializeField] private int cooldownSwitchMaxSize = 48;
    [SerializeField] private int cooldownSwitchMinSize = 32;
    private List<PlayerUI> UIs;
    private Canvas canvas;

    public static GameUI Instance { get; private set; }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }
    }

    public void CreateUI() {
        this.canvas = GameObject.Find(Harmony.GameObjects.GameUI).GetComponent<Canvas>();
        List<Player> players = MultiplayerManager.Instance.GetAllPlayers();
        UIs = new List<PlayerUI>();

        int gapCount = players.Count * 2 + 1;
        float gap = canvas.GetComponent<CanvasScaler>().referenceResolution.x / (gapCount + 1);

        for (int i = 0; i < players.Count; i++)
        {
            PlayerUI newPlayerUI = Instantiate(playerUIPrefab);
            newPlayerUI.transform.SetParent(transform);
            newPlayerUI.SetPlayer(players[i]);
            newPlayerUI.SetActiveAbility(players[i].GetActivatedAbility());
            newPlayerUI.GetComponent<RectTransform>().anchoredPosition = new Vector3((i * 2 + 2) * gap - canvas.GetComponent<CanvasScaler>().referenceResolution.x / 2f, -184, 1);
            players[i].SetPlayerUI(newPlayerUI);
            if (players.Count >= numPlayersBeforeRescale)
                newPlayerUI.transform.localScale = new Vector3(rescaleMultHigh, rescaleMultHigh, 1f);
            else
                newPlayerUI.transform.localScale = new Vector3(rescaleMultLow, rescaleMultLow, 1f);

            UIs.Add(newPlayerUI);
        }
    }

    void Update()
    {
        if (UIs != null)
        {
            for (int i = 0; i < UIs.Count; i++)
            {
                UIs[i].UpdatePlayerUI();
            }
        }
    }

    public void ShowSwitchText(float duration)
    {
        StartCoroutine(ShowSwitchTextCoroutine(duration));
    }

    public void ShowSwitchCooldownText(float time)
    {
        if (time > 0)
        {
            cooldownSwitchText.gameObject.SetActive(true);
            cooldownSwitchText.GetComponentInChildren<TextMeshProUGUI>().SetText(((int)time).ToString());
        }
        else 
        {
            HideSwitchCooldownText();
        }
    }

    public void HideSwitchCooldownText()
    {
        cooldownSwitchText.gameObject.SetActive(false);
    }

    public void ShrinkWeaponSwitchCountdown()
    {
        TextMeshProUGUI text = cooldownSwitchText.GetComponentInChildren<TextMeshProUGUI>();
        text.fontSize = Mathf.MoveTowards(text.fontSize, cooldownSwitchMinSize, Time.deltaTime * 40f);
    }

    public void GrowWeaponSwitchCountdown()
    {
        cooldownSwitchText.GetComponentInChildren<TextMeshProUGUI>().fontSize = cooldownSwitchMaxSize;
    }

    private IEnumerator ShowSwitchTextCoroutine(float duration)
    {
        float totalCosineWeight = 0;
        float simulationTime = 0;

        while (simulationTime < duration)
        {
            float normalizedSimulationTime = simulationTime / duration;
            totalCosineWeight += Mathf.Max(Mathf.Abs(Mathf.Cos(normalizedSimulationTime * Mathf.PI)), 0.2f) * Time.deltaTime;
            simulationTime += Time.deltaTime;
        }

        float normalizationFactor = 1 / totalCosineWeight;

        float time = 0;
        float lerpPosition = 0;
        Vector2 startPosition = new Vector2(-630, switchText.anchoredPosition.y);
        Vector2 endPosition = new Vector2(630, switchText.anchoredPosition.y);

        while (time < duration)
        {
            float normalizedTime = time / duration;

            float cosValue = Mathf.Max(Mathf.Abs(Mathf.Cos(normalizedTime * Mathf.PI)), 0.2f) * Time.deltaTime * normalizationFactor;
            lerpPosition += cosValue;

            lerpPosition = Mathf.Clamp(lerpPosition, 0, 1);

            Vector2 currentPosition = Vector2.Lerp(startPosition, endPosition, lerpPosition);
            switchText.anchoredPosition = currentPosition;

            time += Time.deltaTime;
            yield return null;
        }

        switchText.anchoredPosition = endPosition;
    }
}
