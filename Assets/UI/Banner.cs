using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Banner : MonoBehaviour
{
    [SerializeField] private RectTransform topBanner;
    [SerializeField] private RectTransform bottomBanner;


    [SerializeField] private float halfwayBannerHeight = 500f;
    [SerializeField] private float finalBannerHeight = 400f;
    [SerializeField] private float openedBannerHeight = 625f;


    public void CloseHalfWay(float time)
    {
        StopAllCoroutines();
        StartCoroutine(MoveBannerCoroutine(time, halfwayBannerHeight));
    }

    public void CloseFully(float time, bool showControls = false)
    {
        StopAllCoroutines();
        if (showControls)
        {
            if (topBanner.transform.Find(Harmony.GameObjects.Controls) != null) { topBanner.transform.Find(Harmony.GameObjects.Controls).transform.gameObject.SetActive(true); }
            if (bottomBanner.transform.Find(Harmony.GameObjects.Controls) != null) { bottomBanner.transform.Find(Harmony.GameObjects.Controls).transform.gameObject.SetActive(true); }
        }
        StartCoroutine(MoveBannerCoroutine(time, finalBannerHeight));
    }

    public void OpenFully(float time)
    {
        StopAllCoroutines();
        StartCoroutine(MoveBannerCoroutine(time, openedBannerHeight));
    }

    private IEnumerator MoveBannerCoroutine(float time, float goalY)
    {
        float cooldown = time;
        float originalYtop = topBanner.anchoredPosition.y;
        float originalYbottom = bottomBanner.anchoredPosition.y;
        while (cooldown > 0)
        {
            cooldown -= Time.deltaTime;
            topBanner.anchoredPosition = new(0, Mathf.Lerp(originalYtop, goalY, 1 - (cooldown / time)));
            bottomBanner.anchoredPosition = new(0, Mathf.Lerp(originalYbottom, -goalY, 1 - (cooldown / time)));
            yield return null;
        }
    }
}
