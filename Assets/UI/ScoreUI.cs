using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreUI : MonoBehaviour
{
    [SerializeField] private PlayerScoreUI playerScoreUIPrefab;
    [SerializeField] private int numPlayersBeforeRescale = 5;
    [SerializeField] private float rescaleMultLow = .3f;
    [SerializeField] private float rescaleMultHigh = .2f;
    [SerializeField] private float UIYlevel = 340f;
    private List<PlayerScoreUI> UIs;
    private Canvas canvas;

    public static ScoreUI Instance { get; private set; }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }
    }

    public void CreateUI()
    {
        this.canvas = transform.parent.GetComponentInParent<Canvas>();
        List<Player> players = MultiplayerManager.Instance.GetAllPlayers();
        UIs = new List<PlayerScoreUI>();

        int gapCount = players.Count * 2 + 1;
        float gap = canvas.GetComponent<CanvasScaler>().referenceResolution.x / (gapCount + 1);

        for (int i = 0; i < players.Count; i++)
        {
            PlayerScoreUI newPlayerScoreUI = Instantiate(playerScoreUIPrefab);
            newPlayerScoreUI.transform.SetParent(transform);
            newPlayerScoreUI.SetPlayer(players[i]);
            newPlayerScoreUI.GetComponent<RectTransform>().anchoredPosition = new Vector3((i * 2 + 2) * gap - canvas.GetComponent<CanvasScaler>().referenceResolution.x / 2f, UIYlevel, 1);
            if (players.Count >= numPlayersBeforeRescale)
                newPlayerScoreUI.transform.localScale = new Vector3(rescaleMultHigh, rescaleMultHigh, 1f);
            else
                newPlayerScoreUI.transform.localScale = new Vector3(rescaleMultLow, rescaleMultLow, 1f);

            UIs.Add(newPlayerScoreUI);
        }
    }

    public void UpdateScores()
    {
        for (int i = 0; i < UIs.Count; i++)
        {
            UIs[i].UpdatePlayerScoreUI();
        }
    }
}
