using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ExplanationUI : MonoBehaviour
{
    private const string JUMP = "Jump";
    private const string MOVE = "Move";
    private const string AIM = "Aim";
    private const string SHOOT = "Shoot";
    private const string ABILITY = "Ability";

    private const string EJECT_FRIEND_TEXT = "Eject your \"friends\"";
    private const string BE_LAST_TEXT = "Be the last one standing";
    private const string ROUNDS_TEXT_START_PART = "Survive ";
    private const string ROUNDS_TEXT_LAST_PART = " rounds to win";

    [SerializeField] private GameObject controlsSchema;
    [SerializeField] private TextMeshProUGUI controlTitle;
    [SerializeField] private Image skipIcon;

    private List<GameObject> moveControls = new();
    private List<GameObject> aimControls = new();
    private List<GameObject> jumpControls = new();
    private List<GameObject> shootControls = new();
    private List<GameObject> abilityControls = new();

    [SerializeField] private float timeBetween = 1f;

    [SerializeField] private float maxFontSize = 64;
    [SerializeField] private float minFontSize = 56;

    [SerializeField] Color hideColor;

    [SerializeField] private float timeToSkip = 1f;
    private float currentTimeHolding = 0f;
    private bool loadingGameScene = false;

    [SerializeField] private AudioClip textSound;

    private void Start()
    {
        GetControls();
        StartCoroutine(PlayAnimation());
    }

    private void Update()
    {
        ChangeFontSwitchCooldown();
        CheckForSkip();
    }

    private IEnumerator PlayAnimation()
    {
        yield return new WaitForSeconds(timeBetween);
        EnableFirstTimeSchema();
        ShowControls(moveControls, MOVE);

        yield return new WaitForSeconds(timeBetween);
        ShowControls(aimControls, AIM);

        yield return new WaitForSeconds(timeBetween);
        ShowControls(jumpControls, JUMP);

        yield return new WaitForSeconds(timeBetween);
        ShowControls(shootControls, SHOOT);

        yield return new WaitForSeconds(timeBetween);
        ShowControls(abilityControls, ABILITY);

        yield return new WaitForSeconds(timeBetween);
        GreyLastControls();
        ChangeTitle(EJECT_FRIEND_TEXT);

        yield return new WaitForSeconds(timeBetween);
        ChangeTitle(BE_LAST_TEXT);

        yield return new WaitForSeconds(timeBetween);
        ChangeTitle(ROUNDS_TEXT_START_PART + GameManager.nbOfPointsToWinGame + ROUNDS_TEXT_LAST_PART);

        yield return new WaitForSeconds(timeBetween);

        if (!loadingGameScene)
        {
            loadingGameScene = true;
            MultiplayerManager.Instance.GetAllPlayers().ForEach(p => p.SetPreventAllInputs(false));
            SceneManager.LoadScene(Harmony.Scenes.GameScene);
        }

        yield break;
    }

    private void CheckForSkip()
    {
        if (IsGamepadSkipPressed() || Keyboard.current.spaceKey.isPressed)
        {
            currentTimeHolding += Time.deltaTime;
            if (currentTimeHolding >= timeToSkip)
            {
                if (!loadingGameScene)
                {
                    loadingGameScene = true;
                    MultiplayerManager.Instance.GetAllPlayers().ForEach(p => p.SetPreventAllInputs(false));
                    SceneManager.LoadScene(Harmony.Scenes.GameScene);
                }
            }
        }
        else
        {
            currentTimeHolding -= Time.deltaTime;
            if (currentTimeHolding <= 0) currentTimeHolding = 0;
        }
        FillSkipIcon(currentTimeHolding / timeToSkip);
    }

    private bool IsGamepadSkipPressed()
    {
        foreach (var gamepad in Gamepad.all)
        {
            if (gamepad != null && gamepad.aButton.isPressed)
            {
                return true;
            }
        }
        return false;
    }

    private void FillSkipIcon(float amount)
    {
        skipIcon.fillAmount = amount;
    }


    private void EnableFirstTimeSchema()
    {
        controlsSchema.SetActive(true);
        skipIcon.transform.parent.gameObject.SetActive(true);
        controlsSchema.GetComponent<Image>().color = hideColor;
        controlTitle.gameObject.SetActive(true);
    }

    private void ShowControls(List<GameObject> controls, string text)
    {
        ResetFontSwitchCooldown();
        GreyLastControls();
        foreach (GameObject child in controls) child.SetActive(true);
        controlTitle.SetText(text);
        SoundManager.Instance.PlaySoundEffect(textSound);
    }

    private void GreyLastControls()
    {
        Transform[] children = controlsSchema.GetComponentsInChildren<Transform>(); // Include inactive children
        foreach (Transform child in children)
        {
            Transform[] grandChildren = child.GetComponentsInChildren<Transform>();
            foreach (Transform greatChild in grandChildren)
            {
                Image image = greatChild.GetComponent<Image>();
                TextMeshProUGUI text = greatChild.GetComponent<TextMeshProUGUI>();

                if (image) image.color = hideColor;
                else if (text) text.color = hideColor;
            }
        }
    }

    private void ChangeTitle(string text)
    {
        ResetFontSwitchCooldown();
        controlTitle.gameObject.SetActive(true);
        controlTitle.SetText(text);
        SoundManager.Instance.PlaySoundEffect(textSound);
    }


    private void GetControls()
    {
        controlsSchema.SetActive(true);
        Transform[] children = controlsSchema.GetComponentsInChildren<Transform>(true);
        foreach (Transform child in children)
        {
            switch (child.gameObject.name)
            {
                case MOVE:
                    moveControls.Add(child.gameObject);
                    break;
                case AIM:
                    aimControls.Add(child.gameObject);
                    break;
                case SHOOT:
                    shootControls.Add(child.gameObject);
                    break;
                case ABILITY:
                    abilityControls.Add(child.gameObject);
                    break;
                case JUMP:
                    jumpControls.Add(child.gameObject);
                    break;
                default:
                    break;
            }
        }
        controlsSchema.SetActive(false);
    }

    public void ChangeFontSwitchCooldown()
    {
        controlTitle.fontSize = Mathf.MoveTowards(controlTitle.fontSize, minFontSize, Time.deltaTime * 40f);
    }

    public void ResetFontSwitchCooldown()
    {
        controlTitle.fontSize = maxFontSize;
    }
}
