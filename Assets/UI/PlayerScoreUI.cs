using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PlayerScoreUI : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI pointsText;
    [SerializeField] private Image playerImageUI;
    [SerializeField] private Image backgroundTeamUI;
    [SerializeField] private RawImage winCrown;
    private Player player;
    private int score = 0;

    public Player GetPlayer()
    {
        return this.player;
    }

    public void SetPlayer(Player player)
    {
        this.player = player;
    }

    public void UpdatePlayerScoreUI()
    {
        if (player)
        {
            // Image du joueur
            Sprite sprite = player.GetSkin();
            playerImageUI.sprite = sprite;

            // Background
            Color team = player.GetTeam();
            backgroundTeamUI.color = new Color(team.r, team.g, team.b, backgroundTeamUI.color.a);

            // Points
            if (player)
            {
                pointsText.text = player.GetPoints().ToString() + "pts";

                if (player.GetPoints() > score)
                {
                    winCrown.gameObject.SetActive(true);
                    player.GetComponent<Player>().SetCrown(true);
                }
                else 
                {
                    winCrown.gameObject.SetActive(false);
                    player.GetComponent<Player>().SetCrown(false);
                }

                score = player.GetPoints();
            }
        }
    }
}
