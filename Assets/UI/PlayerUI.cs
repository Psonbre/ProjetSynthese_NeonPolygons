using Harmony;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PlayerUI : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI multText;
    [SerializeField] private CooldownUI cooldownUI;
    [SerializeField] private Image playerImageUI;
    [SerializeField] private Image backgroundTeamUI;
    [SerializeField] private float normalTextSize = 80f;
    [SerializeField] private float damagedTextSize = 100f;
    [SerializeField] private Color damagedTextColor = Color.red;
    [SerializeField] private Color normalTextColor = Color.white;
    [SerializeField] private Material reverseColorMaterial;
    [SerializeField] private Material normalColorMaterial;
    [SerializeField] private float normalImageScale = 0.3f;
    [SerializeField] private float damagedImageScale = 0.5f;


    private float currentDamage;
    private Player player;
    private Ability activeAbility;

    public Player GetPlayer() {
        return this.player;
    }

    public void SetPlayer(Player player)
    {
        this.player = player;
    }

    public void SetActiveAbility(Ability ability)
    {
        activeAbility = ability;
        cooldownUI.SetAbilityImage(CharacterSelectionManager.Instance.GetAbilities().Find(abilityGroup => abilityGroup.abilityName == ability.GetType().ToString()).abilitySprite);
    }

    public void SetActiveAbility(Ability ability, Sprite sprite)
    {
        activeAbility = ability;
        cooldownUI.SetAbilityImage(sprite);
    }

    public void UpdatePlayerUI()
    {
        if (player) {
            // Image du joueur
            Sprite sprite = player.GetSkin();
            playerImageUI.sprite = sprite;

            // Background
            Color team = player.GetTeam();
            backgroundTeamUI.color = new Color(team.r, team.g, team.b, backgroundTeamUI.color.a);

            // Multiplicateur
            if (player) {
                if (currentDamage < player.GetDamage()) currentDamage = Mathf.MoveTowards(currentDamage, player.GetDamage(), Time.deltaTime * 4f);
                else currentDamage = player.GetDamage();
				multText.text = "x" + currentDamage.ToString("0.0");
            }

            // Cooldown
            if (activeAbility)
            {
                cooldownUI.UpdateCooldown(activeAbility.GetCooldownRatio());
            }
                
        }
    }

    public void PlayHurtAnimation()
    {
        StartCoroutine(HurtAnimation());
    }

    private IEnumerator HurtAnimation()
    {
        playerImageUI.material = reverseColorMaterial;
        playerImageUI.transform.localScale = damagedImageScale * Vector2.one;
        multText.fontSize = damagedTextSize;
        multText.color = damagedTextColor;
        multText.transform.localEulerAngles = new(0, 0, 40 * (Random.value - 0.5f));
		playerImageUI.transform.localEulerAngles = new(0, 0, 40 * (Random.value - 0.5f));
		transform.localEulerAngles = new(0, 0, 20 * (Random.value - 0.5f));
		yield return new WaitForSeconds(0.05f);
		playerImageUI.transform.localEulerAngles = new(0, 0, 40 * (Random.value - 0.5f));
		multText.transform.localEulerAngles = new(0, 0, 40 * (Random.value - 0.5f));
		transform.localEulerAngles = new(0, 0, 20 * (Random.value - 0.5f));
		yield return new WaitForSeconds(0.1f);
        Reset();
    }

	public void Reset()
	{
		playerImageUI.transform.localScale = normalImageScale * Vector2.one;
		multText.color = normalTextColor;
		multText.fontSize = normalTextSize;
		playerImageUI.transform.localEulerAngles = new(0, 0, 0);
		multText.transform.localEulerAngles = new(0, 0, 0);
		transform.localEulerAngles = new(0, 0, 0);
		playerImageUI.material = normalColorMaterial;
	}

	public void Hide()
    {
        StopAllCoroutines();
        StartCoroutine(HideCoroutine());
    }

    public void Show()
    {
        StopAllCoroutines();
        StartCoroutine(ShowCoroutine());
    }

    private IEnumerator HideCoroutine()
    {
        RectTransform rectTransform = GetComponent<RectTransform>();
        while (rectTransform.anchoredPosition.y > -268)
        {
            rectTransform.anchoredPosition = new(rectTransform.anchoredPosition.x, rectTransform.anchoredPosition.y - Time.deltaTime * 300);
            yield return null;
        }
    }

    private IEnumerator ShowCoroutine()
    {
        RectTransform rectTransform = GetComponent<RectTransform>();
        while (rectTransform.anchoredPosition.y < -184)
        {
            rectTransform.anchoredPosition = new(rectTransform.anchoredPosition.x, rectTransform.anchoredPosition.y + Time.deltaTime * 300);
            yield return null;
        }
    }
}
