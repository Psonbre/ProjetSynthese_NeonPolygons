using UnityEngine;
using UnityEngine.UI;

public class CooldownUI : MonoBehaviour
{
    [SerializeField] private Image backgroundImageGameObject;
    [SerializeField] private Image overlayImageGameObject;

    [SerializeField] private Color backgroundColor = new Color(1f, 0f, 0f, 1f);
    [SerializeField] private Color overlayColor = new Color(0.5f, 0, 0, 1f);


    private void Start()
    {
        overlayImageGameObject.type = Image.Type.Filled;

        backgroundImageGameObject.color = backgroundColor;
        overlayImageGameObject.color = overlayColor;
    }

    public void SetAbilityImage(Sprite abilityIcon)
    {
        backgroundImageGameObject.sprite = abilityIcon;
    }

    public void UpdateCooldown(float value)
    {
        overlayImageGameObject.fillAmount = value;
    }

}