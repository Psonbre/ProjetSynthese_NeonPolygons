using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dash : Ability
{
    [SerializeField] private float dashForce;
    [SerializeField] private float dashDuration;
    private Vector2 lookInput;
    private Vector2 lookLeftStickInput;
    private Vector2 lookRightStickInput;
    private Vector2 mousePosition;
    private Camera cam;

    private void Start()
    {
        controls.actions[Harmony.InputActions.LookLeftStick].performed += ctx => lookLeftStickInput = ctx.ReadValue<Vector2>();
        controls.actions[Harmony.InputActions.LookRightStick].performed += ctx => lookRightStickInput = ctx.ReadValue<Vector2>();
        controls.actions[Harmony.InputActions.LookRightStick].canceled += ctx => lookRightStickInput = Vector2.zero;
        controls.actions[Harmony.InputActions.LookMouse].performed += ctx => mousePosition = ctx.ReadValue<Vector2>();
    }

    protected override void Activate()
    {
        if (multiplayerManager)
        {
            Statistics.IncrementStat(Harmony.Stats.dash, 1);
        }
        base.Activate();
        cam = Camera.main;
        StartCoroutine(Dashing());
    }

    IEnumerator Dashing()
    {
        lookInput = lookInput = lookRightStickInput != Vector2.zero ? lookRightStickInput : lookLeftStickInput;
        if (mousePosition != Vector2.zero && cam) lookInput = (cam.ScreenToWorldPoint(mousePosition) - transform.position).normalized;

        rb.velocity = lookInput * dashForce;

        yield return new WaitForSeconds(dashDuration);

        rb.velocity = Vector2.zero;
    }
}
