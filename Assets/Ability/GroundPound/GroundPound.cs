using UnityEngine;

public class GroundPound : Ability
{
    [SerializeField] private float downwardsVelocity = 500f;
    [SerializeField] private float groundPoundDuration = 1f;
    [SerializeField] private float groundPoundMass = 200f;
    [SerializeField] private float explosionSize = 40f;
    [SerializeField] private float explosionPower = 600f;
    [SerializeField] private float explosionDamage = 2f;
    private bool groundPounding = false;
    private bool speedingDown = false;
    private float defaultMass;

    protected override void Awake()
    {
        base.Awake();
        defaultMass = rb.mass;
    }

    protected override void Activate()
    {
        if (multiplayerManager)Statistics.IncrementStat(Harmony.Stats.groundPound, 1);

        player.RemoveJumps();

        base.Activate();
        SpeedDown();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if ((collision.gameObject.GetComponent<DestroyablePlatform>() != null || collision.gameObject.CompareTag(Harmony.Tags.Player)) && !groundPounding && speedingDown)
        {
            Explosion explosion = PoolManager.Instance.Get(Harmony.PoolTags.explosion).GetComponent<Explosion>();
            explosion.gameObject.SetActive(true);
            explosion.SetExplosionPower(explosionPower);
            explosion.SetExplosionDamage(explosionDamage);
            explosion.SetTeam(player.GetTeam());
            explosion.transform.position = transform.position;
            explosion.Explode(explosionSize, Vector2.up * explosionPower);
            speedingDown = false;
            groundPounding = true;
            player.Jump();
            Invoke("StopGroundPound", groundPoundDuration);
        }
    }

    public override void ResetAbility()
    {
        StopGroundPound();
        speedingDown = false;
    }

    protected override bool CanActivate()
    {
        return base.CanActivate() && !player.IsGrounded();
    }

    private void SpeedDown()
    {
        speedingDown = true;
        rb.velocityY = -downwardsVelocity;
        rb.mass = groundPoundMass;
    }

    private void StopGroundPound()
    {
        groundPounding = false;
        rb.mass = defaultMass;
    }
}
