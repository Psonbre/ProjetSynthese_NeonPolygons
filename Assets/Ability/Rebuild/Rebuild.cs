using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Rebuild : Ability
{
    [SerializeField] private float defaultPlatformGravity = 0f;
    [SerializeField] private float movingOpacity = 0.5f;
    private GameObject selectedPlatform;
    private DestroyablePlatform selectedDestroyablePlatform;
    private float defaultOpacity = 1f;

    protected override void Activate()
    {
        base.Activate();
        List<GameObject> allPlatforms = GameObject.FindGameObjectsWithTag(Harmony.Tags.Ground).ToList();

        foreach (GameObject platform in allPlatforms)
        {
            DestroyablePlatform destroyablePlatform = platform.GetComponent<DestroyablePlatform>();
			if (destroyablePlatform.GetOriginalPosition().Equals(selectedDestroyablePlatform.GetOriginalPosition()) && !destroyablePlatform.GetIsLocked())
            {
                StartCoroutine(MovePlatformsCoroutine(platform));
            }
        }
    }

    private IEnumerator MovePlatformsCoroutine(GameObject platform)
    {
        Rigidbody2D rb = platform.GetComponent<Rigidbody2D>();
        SpriteRenderer spriteRenderer = platform.GetComponent<SpriteRenderer>();
        DestroyablePlatform destroyablePlatform = platform.GetComponent<DestroyablePlatform>();
        Vector2 originalPosition = destroyablePlatform.GetOriginalPosition();
        float originalAngle = destroyablePlatform.GetOriginalAngle();

        Color currentColor = spriteRenderer.color;
        rb.gravityScale = 0;
        currentColor.a = movingOpacity;
        spriteRenderer.color = currentColor;
        float elapsedTime = 0f;
        Vector2 startingPosition = platform.transform.position;
        float startingAngle = platform.transform.eulerAngles.z;

        while (elapsedTime < Mathf.Min(3, maxCooldown / 2f))
        {
            elapsedTime += Time.deltaTime;
            platform.transform.position = Vector2.Lerp(startingPosition, originalPosition, elapsedTime / (Mathf.Min(3, maxCooldown / 2f) / 2f));
            platform.transform.eulerAngles = new(0, 0, Mathf.LerpAngle(startingAngle, originalAngle, elapsedTime / (Mathf.Min(3, maxCooldown / 2f) / 2f)));
            yield return null;
        }

        rb.gravityScale = defaultPlatformGravity;
        currentColor.a = defaultOpacity;
        spriteRenderer.color = currentColor;

        if(!platform.GetInstanceID().Equals(selectedPlatform.gameObject.GetInstanceID()))
        {
            Destroy(platform);
        }
        else
        {
            selectedDestroyablePlatform.SetOriginalTexture();
        }
    }

    protected override bool CanActivate()
    {
        if (base.CanActivate())
        {
            selectedPlatform = player.GetSelectedPlatform();
            if(selectedPlatform != null)
            {
                selectedDestroyablePlatform = selectedPlatform.GetComponent<DestroyablePlatform>();
                return !selectedPlatform.GetComponent<DestroyablePlatform>().GetIsLocked();
            }
        }
        return false;
    }
}