
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Munch : Ability
{
    [SerializeField] private float healAmount;
    [SerializeField] private GameObject munchPrefab;
    [SerializeField] private float collideCircleRadius;
    [SerializeField] private LayerMask platformLayer;
    private Collider2D[] nearPlatforms;

    protected override void Activate()
    {
        base.Activate();
        Vector2 closestPoint = Vector2.zero;
        bool firstPoint = true;

        foreach (Collider2D collider in nearPlatforms)
        {
            Vector2 currentClosestPoint = collider.ClosestPoint(gameObject.transform.position);

            if (firstPoint)
            {
                closestPoint = currentClosestPoint;
                firstPoint = false;
            }
            else if (Vector2.Distance(transform.position, currentClosestPoint) < Vector2.Distance(transform.position, closestPoint))
            {
                closestPoint = currentClosestPoint;
            }
        }

        if (!firstPoint)
        {
            StartCoroutine(MunchPlatform(closestPoint));
        }

        player.Heal(healAmount);
    }


    IEnumerator MunchPlatform(Vector2 closestPoint)
    {
        GameObject munch = Instantiate(munchPrefab);

        munch.transform.position = closestPoint;
        float angle = Mathf.Atan2(closestPoint.y - gameObject.transform.position.y, closestPoint.x - gameObject.transform.position.x);

        munch.transform.rotation = Quaternion.Euler(0, 0, angle * Mathf.Rad2Deg);

        munch.SetActive(true);

        yield return new WaitForSeconds(0.1f);

        munch.SetActive(false);
        Destroy(munch);
    }

    protected override bool CanActivate()
    {
        if (base.CanActivate())
        {
            nearPlatforms = Physics2D.OverlapCircleAll(transform.position, collideCircleRadius, platformLayer);

            if (nearPlatforms.Length != 0)
            {
                return true;
            }
            return false;
        }
        return false;
    }
}