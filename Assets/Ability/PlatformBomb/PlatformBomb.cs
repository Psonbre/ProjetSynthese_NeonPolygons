using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class PlatformBomb : Ability
{
    [SerializeField] private float normalCountdownDuration;
    [SerializeField] private float maxCountdownDuration;
    [SerializeField] private int averageNbPixel;
    [SerializeField] private int maxNbPixel;
    [SerializeField] private float explosionPower;
    [SerializeField] private float explosionDamage;
    [SerializeField] private float flashAcceleration = 0.5f;
    [SerializeField] private Color flashColor = new Color(1f, 0f, 0f);
    [SerializeField] private float explosionSizeMultiplier = .7f;
    [SerializeField] private float crackSizeMultiplier = .02f;
    [SerializeField] private List<AudioClip> beepSounds;
    [SerializeField] private float volumeMultiplicator = 1f;
    [SerializeField] private float minimumBeepDelay = .05f;
    private GameObject selectedPlatform;
    private float flashBuffer = 0.005f;

    protected override void Activate()
    {
        base.Activate();
        StartCoroutine(PlatformBombCoroutine(selectedPlatform));
    }

    private IEnumerator PlatformBombCoroutine(GameObject platform)
    {
        Collider2D collider = platform.GetComponent<Collider2D>();
        SpriteRenderer spriteRenderer = platform.GetComponent<SpriteRenderer>();
        DestroyablePlatform destroyablePlatform = platform.GetComponent<DestroyablePlatform>();


        float flashDuration = Mathf.Min(maxCountdownDuration, normalCountdownDuration * (float)Mathf.Sqrt(destroyablePlatform.GetPixelCount()) / (float)Mathf.Sqrt(averageNbPixel));
        float elapsedTime = flashDuration;
        bool isFlash = true;

        float beepDelay = minimumBeepDelay;

        while (elapsedTime > flashBuffer)
        {
            beepDelay -= flashAcceleration * (elapsedTime / flashDuration);
            elapsedTime -= flashAcceleration * (elapsedTime / flashDuration);

            if(spriteRenderer != null)
            {
                if (isFlash) {

                    if (beepDelay <= 0) {
                        beepDelay = minimumBeepDelay;
                        SoundManager.Instance.PlayRandomSoundEffect(beepSounds, volumeMultiplicator);
                    }
                    spriteRenderer.color = flashColor;
                }
                else
                    spriteRenderer.color = Color.white;
            }
            else
            {
                yield break;
            }

            isFlash = !isFlash;

            yield return new WaitForSeconds(flashAcceleration * (elapsedTime / flashDuration));
        }

        spriteRenderer.color = Color.white;


        Explosion explosion = PoolManager.Instance.Get(Harmony.PoolTags.explosion).GetComponent<Explosion>();
        explosion.gameObject.SetActive(true);
        explosion.transform.position = collider.bounds.center;
        explosion.SetExplosionPower(explosionPower);
        explosion.SetExplosionDamage(explosionDamage);
        explosion.SetTeam(player.GetTeam());

        Explosion crack = PoolManager.Instance.Get(Harmony.PoolTags.crack).GetComponent<Explosion>();
        crack.gameObject.SetActive(true);
        crack.transform.position = collider.bounds.center;
        crack.SetExplosionPower(0);
        crack.SetExplosionDamage(0);
        crack.SetTeam(new Color(0, 0, 0));

        float explosionRadius = Mathf.Min(collider.bounds.size.x, collider.bounds.size.y) * explosionSizeMultiplier;
        Vector2 crackSize = new Vector2(collider.bounds.size.x, collider.bounds.size.y) * crackSizeMultiplier; 


        explosion.Explode(explosionRadius, new Vector2(0, 0));
        crack.Explode(crackSize, new Vector2(0, 0));
    }

    protected override bool CanActivate()
    {
        if (base.CanActivate())
        {
            selectedPlatform = player.GetSelectedPlatform();
            if(selectedPlatform != null && selectedPlatform.GetComponent<DestroyablePlatform>().GetPixelCount() <= maxNbPixel)
            {
                return true;
            }
            return false;
        }
        return false;
    }
}
