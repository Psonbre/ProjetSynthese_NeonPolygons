using Harmony;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.VisualScripting;
using UnityEngine;

public class Portal : Ability
{
    [SerializeField] private string prefabType;
    [SerializeField] private float firstPortalOpacity = 1f;
    [SerializeField] private float secondPortalOpacity = 0.5f;
    [SerializeField] private float extraLineOffset = .5f;
    [SerializeField] private float particleOpacity = .5f;
    private PortalController firstPortal;
    private PortalController secondPortal;
    private SpriteRenderer firstPortalSpriteRenderer;
    private ParticleSystem firstPortalParticleSystem;
    private ParticleSystem secondPortalParticleSystem;
    private SpriteRenderer secondPortalSpriteRenderer;
    private bool firstPortalToMove = true;
    private LineRenderer portalLink;

    private void Start()
    {
        ResetAbility();
    }

    public PortalController GetFirstPortal() {
        return firstPortal;
    }

    public PortalController GetSecondPortal() {
        return secondPortal;
    }

    public override void ResetAbility() {
        firstPortal = PoolManager.Instance.Get(prefabType).gameObject.GetComponent<PortalController>();
        secondPortal = PoolManager.Instance.Get(prefabType).gameObject.GetComponent<PortalController>();
        firstPortalSpriteRenderer = firstPortal.GetComponent<SpriteRenderer>();
        secondPortalSpriteRenderer = secondPortal.GetComponent<SpriteRenderer>();
        firstPortalParticleSystem = firstPortal.GetComponent<ParticleSystem>();
        secondPortalParticleSystem = secondPortal.GetComponent<ParticleSystem>();
        firstPortal.gameObject.SetActive(false);
        secondPortal.gameObject.SetActive(false);
        portalLink = firstPortal.gameObject.GetComponent<LineRenderer>();
        portalLink.positionCount = 2;
        portalLink.startWidth = 1f;
        portalLink.endWidth = 1f;
        portalLink.sortingOrder = 1;
        portalLink.enabled = false;
    }

    protected override void Activate()
    {
        base.Activate();
        Color teamColor = player.GetTeam();
        if (!firstPortal.gameObject.activeSelf)
        {
            firstPortal.gameObject.SetActive(true);
            firstPortal.transform.position = gameObject.transform.position;
            firstPortalSpriteRenderer.color = teamColor;
            firstPortalParticleSystem.startColor = new Color(teamColor.r, teamColor.g, teamColor.b, firstPortalOpacity * particleOpacity);
        }
        else if (!secondPortal.gameObject.activeSelf)
        {
            secondPortal.gameObject.SetActive(true);
            secondPortal.transform.position = gameObject.transform.position;
            secondPortalSpriteRenderer.color = teamColor;

            firstPortalParticleSystem.startColor = new Color(teamColor.r, teamColor.g, teamColor.b, secondPortalOpacity * particleOpacity);
            secondPortalParticleSystem.startColor = new Color(teamColor.r, teamColor.g, teamColor.b, firstPortalOpacity * particleOpacity);
            firstPortal.Link(secondPortal);
            secondPortal.Link(firstPortal);
            firstPortalSpriteRenderer.color = new Color(teamColor.r, teamColor.g, teamColor.b, secondPortalOpacity);

            
            portalLink.startColor = new Color(teamColor.r, teamColor.g, teamColor.b, secondPortalOpacity);
            portalLink.endColor = teamColor;
            UpdateLine();
            portalLink.enabled = true;

        }
        else
        {
            if (firstPortalToMove)
            {
                firstPortal.GetComponent<PortalController>().canTeleport = false;
                firstPortal.transform.position = gameObject.transform.position;
                firstPortalToMove = false;

                portalLink.startColor = teamColor;
                portalLink.endColor = new Color(teamColor.r, teamColor.g, teamColor.b, secondPortalOpacity);
                UpdateLine();

                firstPortalSpriteRenderer.color = new Color(teamColor.r, teamColor.g, teamColor.b, firstPortalOpacity);
                secondPortalSpriteRenderer.color = new Color(teamColor.r, teamColor.g, teamColor.b, secondPortalOpacity);
                firstPortalParticleSystem.startColor = new Color(teamColor.r, teamColor.g, teamColor.b, firstPortalOpacity * particleOpacity);
                secondPortalParticleSystem.startColor = new Color(teamColor.r, teamColor.g, teamColor.b, secondPortalOpacity * particleOpacity);
            }
            else
            {
                secondPortal.GetComponent<PortalController>().canTeleport = false;
                secondPortal.transform.position = gameObject.transform.position;
                firstPortalToMove = true;

                portalLink.startColor = new Color(teamColor.r, teamColor.g, teamColor.b, secondPortalOpacity);
                portalLink.endColor = teamColor;
                UpdateLine();

                firstPortalSpriteRenderer.color = new Color(teamColor.r, teamColor.g, teamColor.b, secondPortalOpacity);
                secondPortalSpriteRenderer.color = new Color(teamColor.r, teamColor.g, teamColor.b, firstPortalOpacity);
                firstPortalParticleSystem.startColor = new Color(teamColor.r, teamColor.g, teamColor.b, secondPortalOpacity * particleOpacity);
                secondPortalParticleSystem.startColor = new Color(teamColor.r, teamColor.g, teamColor.b, firstPortalOpacity * particleOpacity);
            }
        }
    }

    private void UpdateLine() {
        if (firstPortal.gameObject.activeSelf && secondPortal.gameObject.activeSelf)
        {
            Vector3 firstPortalPosition = new Vector3(firstPortal.transform.position.x, firstPortal.transform.position.y, 1);
            Vector3 secondPortalPosition = new Vector3(secondPortal.transform.position.x, secondPortal.transform.position.y, 1);

            float firstPortalOffset = firstPortal.GetComponent<SpriteRenderer>().bounds.extents.x - extraLineOffset;
            float secondPortalOffset = secondPortal.GetComponent<SpriteRenderer>().bounds.extents.x - extraLineOffset;

            float angle = Mathf.Atan2(secondPortal.transform.position.y - firstPortal.transform.position.y, secondPortal.transform.position.x - firstPortal.transform.position.x);

            firstPortalPosition += new Vector3(firstPortalOffset * Mathf.Cos(angle), firstPortalOffset * Mathf.Sin(angle), 0);
            secondPortalPosition -= new Vector3(secondPortalOffset * Mathf.Cos(angle), secondPortalOffset * Mathf.Sin(angle), 0);

            portalLink.SetPosition(0, firstPortalPosition);
            portalLink.SetPosition(1, secondPortalPosition);
        }
    }
}
