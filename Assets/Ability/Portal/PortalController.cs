using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;

public class PortalController : MonoBehaviour
{
    [SerializeField] private float teleportCooldown = 0.5f;
    [SerializeField] private List<AudioClip> teleportSounds;
    [SerializeField] private float volumeMultiplicator = 1f;
    public bool canTeleport = false;
    private PortalController linkedPortal;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (linkedPortal)
        {
            if (canTeleport && !collision.gameObject.CompareTag(Harmony.Tags.Laser) && !collision.gameObject.CompareTag(Harmony.Tags.Portal) && !collision.gameObject.CompareTag(Harmony.Tags.PlayerAimAssist) && collision.gameObject.layer != 14) 
            { 
                if(collision.gameObject.CompareTag(Harmony.Tags.Ground) && !collision.gameObject.GetComponent<SpringJoint2D>())
                {
                    TeleportPlatform(collision.gameObject);
                    StartCoroutine(TeleportCooldown());
                }
                else if(!collision.gameObject.CompareTag(Harmony.Tags.Ground))
                {
                    Teleport(collision.gameObject);
                    StartCoroutine(TeleportCooldown());
                }
            }
        }
    }

    private void OnDisable()
    {
        this.canTeleport = false;
        this.linkedPortal = null;
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        canTeleport = true;
    }

    IEnumerator TeleportCooldown()
    {
        canTeleport = false;
        linkedPortal.GetComponent<Collider2D>().enabled = false;
        yield return new WaitForSeconds(teleportCooldown);
        linkedPortal.GetComponent<Collider2D>().enabled = true;
    }

    void Teleport(GameObject player)
    {
        SoundManager.Instance.PlayRandomSoundEffect(teleportSounds, volumeMultiplicator);
        player.transform.position = linkedPortal.transform.position;
    }

    void TeleportPlatform(GameObject platform)
    {
        SoundManager.Instance.PlayRandomSoundEffect(teleportSounds, volumeMultiplicator);
        platform.transform.position += (Vector3)(linkedPortal.transform.position - platform.GetComponent<PolygonCollider2D>().bounds.center);
    }

    public void Link(PortalController portalToLink)
    {
        linkedPortal = portalToLink;
    }

}
