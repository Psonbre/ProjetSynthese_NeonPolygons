using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mine : Ability
{
    [SerializeField] private float activationTime;
    [SerializeField] private string bulletType;

    protected override void Activate()
    {
        base.Activate();
        GameObject mine = PoolManager.Instance.Get(bulletType);

        mine.SetActive(true);
        mine.transform.position = player.transform.position;
        mine.GetComponent<SpriteRenderer>().color = player.GetTeam();
        mine.GetComponent<MineController>().SetTeam(player.GetTeam());
    }
}
