using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MineController : MonoBehaviour
{
    [SerializeField] private float explosionPower;
    [SerializeField] private float explosionDamage;
    [SerializeField] private float explosionRadius;
    private Color team; 

    public void SetTeam(Color team)
    { 
        this.team = team; 
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag(Harmony.Tags.Player) && collision.transform.GetComponent<Player>().GetTeam() != team)
        {
            Explode();
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag(Harmony.Tags.DestroyArea))
        {
            Explode();
        }
    }

    private void Explode()
    {
        Explosion explosion = PoolManager.Instance.Get(Harmony.PoolTags.explosion).GetComponent<Explosion>();

        explosion.gameObject.SetActive(true);
        explosion.transform.position = transform.position;
        explosion.SetExplosionPower(explosionPower);
        explosion.SetExplosionDamage(explosionDamage);
        explosion.SetTeam(team);

        explosion.Explode(explosionRadius, new Vector2(0, 0));
        PoolManager.Instance.ReturnToPool(Harmony.PoolTags.mine, gameObject);
    }
}
