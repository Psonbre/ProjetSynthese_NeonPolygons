using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lock : Ability
{
    [SerializeField] private float lockedOpacity = 0.25f;
    [SerializeField] private float normalLockDuration;
    [SerializeField] private float maxLockDuration;
    [SerializeField] private int averageNbPixel;
    [SerializeField] private float normalLockSpriteScale;
    [SerializeField] private GameObject lockSpritePrefab;
    private GameObject currentLockedPlatform;
    private GameObject currentLockObj;
    private GameObject lastLockedPlatform;
    private GameObject lastLockObj;

    protected override void Activate()
    {
        base.Activate();
        Collider2D collider = currentLockedPlatform.GetComponent<Collider2D>();
        Rigidbody2D rb = currentLockedPlatform.GetComponent<Rigidbody2D>();
        SpriteRenderer spriteRenderer = currentLockedPlatform.GetComponent<SpriteRenderer>();
        DestroyablePlatform destroyablePlatform = currentLockedPlatform.GetComponent<DestroyablePlatform>();
        SpringJoint2D joint = currentLockedPlatform.GetComponent<SpringJoint2D>();
        Color currentColor = spriteRenderer.color;

        if (!currentLockedPlatform.GetComponent<DestroyablePlatform>().GetIsLocked())
        {
            rb.constraints = RigidbodyConstraints2D.FreezeAll;
            destroyablePlatform.SetLocked(true);
            currentColor.a = lockedOpacity;
            spriteRenderer.color = currentColor; 

            currentLockObj = Instantiate(lockSpritePrefab);
            float lockSpriteScale = normalLockSpriteScale / ((float)Mathf.Sqrt(averageNbPixel) / Mathf.Sqrt(destroyablePlatform.GetPixelCount()));
            currentLockObj.transform.localScale = new Vector2(lockSpriteScale, lockSpriteScale);
            currentLockObj.transform.SetParent(currentLockedPlatform.transform);
            currentLockObj.transform.position = collider.bounds.center;

            if (!lastLockedPlatform)
            {
                lastLockedPlatform = currentLockedPlatform;
                lastLockObj = currentLockObj;
            }
            else
            {
                Destroy(lastLockObj);
                lastLockedPlatform.GetComponent<SpriteRenderer>().color = Color.white;
                lastLockedPlatform.GetComponent<DestroyablePlatform>().SetLocked(false);
                if (lastLockedPlatform.GetComponent<SpringJoint2D>())
                    rb.constraints = RigidbodyConstraints2D.FreezeRotation;
                else
                    rb.constraints = RigidbodyConstraints2D.None;

                lastLockedPlatform = currentLockedPlatform;
                lastLockObj = currentLockObj;
            }
        }
        else
        {
            Destroy(currentLockObj);
            spriteRenderer.color = Color.white;
            destroyablePlatform.SetLocked(false);
            if (joint)
                rb.constraints = RigidbodyConstraints2D.FreezeRotation;
            else
                rb.constraints = RigidbodyConstraints2D.None;
        }
    }

    protected override bool CanActivate()
    {
        if (base.CanActivate())
        {
            currentLockedPlatform = player.GetSelectedPlatform();

            return currentLockedPlatform != null;
        }
        return false;
    }
}
