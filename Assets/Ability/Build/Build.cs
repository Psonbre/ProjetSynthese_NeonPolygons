using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Build : Ability
{
    [SerializeField] private List<GameObject> buildPlatforms;
    [SerializeField] private float timeBeforeCountdown = 5f;
    [SerializeField] private float countdownDuration = 5f;
    [SerializeField] private float explosionPower;
    [SerializeField] private float explosionDamage;
    [SerializeField] private float flashAcceleration = 0.5f;
    [SerializeField] private Color flashColor = new Color(1f, 0f, 0f);
    [SerializeField] private List<AudioClip> beepSounds;
    [SerializeField] private float volumeMultiplicator = 1f;
    [SerializeField] private float minimumBeepDelay = .05f;
    private float flashBuffer = 0.005f;
    protected override void Activate()
    {
        if (multiplayerManager)
        {
            Statistics.IncrementStat(Harmony.Stats.build, 1);
        }
        base.Activate();
        if (buildPlatforms.Count > 0)
        {
            Vector2 bottomPosition = new Vector2(transform.position.x, transform.position.y - 15.0f);

            string theme = FindAnyObjectByType<MusicThemeManager>().themeName;
            GameObject plaform = Instantiate(buildPlatforms.Find(p => p.name == theme), bottomPosition, Quaternion.identity);

            plaform.transform.parent = GameObject.Find(Harmony.GameObjects.Platforms).transform;
            StartCoroutine(PlatformBombCoroutine(plaform));
        }
    }

        private IEnumerator PlatformBombCoroutine(GameObject platform)
        {
            yield return new WaitForSeconds(timeBeforeCountdown);
            
            if (platform != null)
            {
                Collider2D collider = platform.GetComponent<Collider2D>();
                SpriteRenderer spriteRenderer = platform.GetComponent<SpriteRenderer>();

                float elapsedTime = countdownDuration;
                bool isFlash = true;
                float beepDelay = minimumBeepDelay;
                while (elapsedTime > flashBuffer)
                {
                    beepDelay -= flashAcceleration * (elapsedTime / countdownDuration);
                    elapsedTime -= flashAcceleration * (elapsedTime / countdownDuration);

                    if (spriteRenderer != null)
                    {
                        if (isFlash)
                        {
                            if (beepDelay <= 0)
                            {
                                beepDelay = minimumBeepDelay;
                                SoundManager.Instance.PlayRandomSoundEffect(beepSounds, volumeMultiplicator);
                            }
                            spriteRenderer.color = flashColor;
                        }
                        else
                        {
                            spriteRenderer.color = Color.white;
                        }
                    }
                    else
                    {
                        yield break;
                    }

                    isFlash = !isFlash;

                    yield return new WaitForSeconds(flashAcceleration * (elapsedTime / countdownDuration));
                }

                spriteRenderer.color = Color.white;

                Explosion explosion = PoolManager.Instance.Get(Harmony.PoolTags.explosion).GetComponent<Explosion>();
                explosion.gameObject.SetActive(true);
                explosion.transform.position = collider.bounds.center;
                explosion.SetExplosionPower(explosionPower);
                explosion.SetExplosionDamage(explosionDamage);
                explosion.SetTeam(player.GetTeam());
                float explosionRadius = Mathf.Max(collider.bounds.size.x, collider.bounds.size.y);
                explosion.Explode(explosionRadius, new Vector2(0, 0));

            }
        }

    protected override bool CanActivate()
    {
        return base.CanActivate() && !player.IsGrounded();
    }
}
