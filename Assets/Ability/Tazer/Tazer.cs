using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tazer : Ability
{
    [SerializeField] private string bulletType;
    [SerializeField] private float activationTime;

    protected override void Activate()
    {
        base.Activate();
        StartCoroutine(ActivateTazer());
    }

    IEnumerator ActivateTazer()
    {
        GameObject electricity = PoolManager.Instance.Get(bulletType);

        electricity.SetActive(true);
        electricity.transform.position = player.transform.position;
        electricity.GetComponent<ElectricityController>().SetTeam(player.GetTeam());

        yield return new WaitForSeconds(activationTime);

        PoolManager.Instance.ReturnToPool(bulletType, electricity);
    }
}