using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElectricityController : MonoBehaviour
{
    [SerializeField] private float tazeTime;
    private Color team;

    public void SetTeam(Color team)
    {
        this.team = team;
        GetComponent<ParticleSystem>().startColor = team;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag(Harmony.Tags.Player) && !collision.gameObject.GetComponent<Player>().GetPlayerInputsFrozen() && collision.gameObject.GetComponent<Player>().GetTeam() != team)
        {
            collision.gameObject.GetComponent<Player>().TazeFor(tazeTime);
        }
    }
}