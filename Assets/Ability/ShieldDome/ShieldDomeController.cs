using System.Collections.Generic;
using UnityEngine;

public class ShieldDomeController : MonoBehaviour
{
    [SerializeField] private int nbColliderPoints = 100;
    [SerializeField] private float colliderRadius = 1.3f;
    [SerializeField] private float edgeRadius = 1.5f;
    [SerializeField] private float bounceFriction = .98f;
    [SerializeField] private float opacity = .7f;
    [SerializeField] private List<AudioClip> shieldHitSounds;
    [SerializeField] private float shieldHitSoundVolume = 1f;
    [SerializeField] private float bulletLayer = 10;
    private Color team;

    private void Awake()
    {
        EdgeCollider2D edgeCollider = gameObject.AddComponent<EdgeCollider2D>();

        Vector2[] points = new Vector2[nbColliderPoints];

        for (int i = 0; i < nbColliderPoints; i++)
        {
            float angle = i * Mathf.PI * 2f / nbColliderPoints;
            float x = Mathf.Cos(angle) * colliderRadius;
            float y = Mathf.Sin(angle) * colliderRadius;
            points[i] = new Vector2(x, y);
        }

        edgeCollider.points = points;
        edgeCollider.isTrigger = true;
        edgeCollider.edgeRadius = edgeRadius;
    }

    public Color GetTeam() {
        return team; 
    }
    public void SetTeam(Color team)
    {
        this.team = team;
        gameObject.GetComponent<SpriteRenderer>().color = new Color(team.r, team.g, team.b, opacity);
        GetComponent<ParticleSystem>().startColor = new Color(team.r, team.g, team.b, opacity);

	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == bulletLayer)
        {
            Bullet bullet = collision.gameObject.GetComponent<Bullet>();
            if (bullet && bullet.GetTeam() != team)
            {
                SoundManager.Instance.PlayRandomSoundEffect(shieldHitSounds, shieldHitSoundVolume);
                PoolManager.Instance.ReturnToPool(bullet.GetPoolTag(), bullet.gameObject);
            }
            
            GrenadeProjectile grenade = collision.gameObject.GetComponent<GrenadeProjectile>();
            C4Projectile c4 = collision.gameObject.GetComponent<C4Projectile>();
            BounceBullet bounceBall = collision.gameObject.GetComponent<BounceBullet>();

            if ((grenade && grenade.GetTeam() != team) || (c4 && c4.GetTeam() != team) || (bounceBall && bounceBall.GetTeam() != team))
            {
                SoundManager.Instance.PlayRandomSoundEffect(shieldHitSounds, shieldHitSoundVolume);
                Vector2 reflectionAxis = (collision.gameObject.transform.position - transform.position).normalized;
                Vector2 result = Vector2.Reflect(collision.GetComponent<Rigidbody2D>().velocity, reflectionAxis);
                collision.GetComponent<Rigidbody2D>().velocity = result * bounceFriction;
            }
        }
    }
}