using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public abstract class Ability : MonoBehaviour
{
    [SerializeField] protected float maxCooldown;
    [SerializeField] protected List<AudioClip> activationSounds;
    [SerializeField] protected float activationVolume = 1f;
    protected float currentCooldown;
    protected PlayerInput controls;
    protected Player player;
    protected GameObject platform;
    protected Rigidbody2D rb;
    protected MultiplayerManager multiplayerManager;

    protected virtual void Awake()
    {
        multiplayerManager = FindFirstObjectByType<MultiplayerManager>();
        currentCooldown = 0f;
        player = GetComponent<Player>();
        controls = GetComponentInParent<PlayerInput>();
        rb = GetComponent<Rigidbody2D>();   
    }

    protected virtual void Update()
    {
        currentCooldown = Mathf.MoveTowards(currentCooldown, 0f, Time.deltaTime);

        if(CanActivate())
        {
            currentCooldown = maxCooldown;
            Activate();
        }
    }

    public float GetCooldownRatio() {
        return currentCooldown / maxCooldown;
    }

    protected virtual void Activate()
    {
        SoundManager.Instance.PlayRandomSoundEffect(activationSounds, activationVolume);
    }

    public virtual void ResetAbility() 
    { 
    }

    public void ResetCooldown() {
        currentCooldown = 0;
    }

    protected virtual bool CanActivate()
    { 
        return currentCooldown <= 0f && 
            controls.actions[Harmony.InputActions.Ability].triggered && 
            player.CanUseAbility() && 
            !player.IsPlayerFrozen() && 
            !player.GetPlayerInputsFrozen() && 
            !player.GetIsDead() && 
            !player.GetAllInputsFrozen();
    }
}